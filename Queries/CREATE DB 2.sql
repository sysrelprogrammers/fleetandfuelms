USE [master]
GO
/****** Object:  Database [FFMS]    Script Date: 2/12/2016 2:19:06 PM ******/
CREATE DATABASE [FFMS]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'FFMS', FILENAME = N'C:\DATA\FFMS.mdf' , SIZE = 7168KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'FFMS_log', FILENAME = N'C:\DATA\FFMS_log.ldf' , SIZE = 1536KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

ALTER DATABASE [FFMS] SET COMPATIBILITY_LEVEL = 120
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
	begin
	EXEC [FFMS].[dbo].[sp_fulltext_database] @action = 'enable'
	end
GO

ALTER DATABASE [FFMS] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [FFMS] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [FFMS] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [FFMS] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [FFMS] SET ARITHABORT OFF 
GO
ALTER DATABASE [FFMS] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [FFMS] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [FFMS] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [FFMS] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [FFMS] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [FFMS] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [FFMS] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [FFMS] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [FFMS] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [FFMS] SET DISABLE_BROKER 
GO
ALTER DATABASE [FFMS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [FFMS] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [FFMS] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [FFMS] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [FFMS] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [FFMS] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [FFMS] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [FFMS] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [FFMS] SET MULTI_USER 
GO
ALTER DATABASE [FFMS] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [FFMS] SET DB_CHAINING OFF 
GO
ALTER DATABASE [FFMS] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [FFMS] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [FFMS] SET DELAYED_DURABILITY = DISABLED 
GO
USE [FFMS]
GO
/****** Object:  Table [dbo].[CARMAKE]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CARMAKE](
	[MakeID] [varchar](50) NOT NULL,
	[Make] [varchar](50) NULL,
	[Model] [varchar](50) NULL,
	[Description] [varchar](100) NULL DEFAULT (''),
PRIMARY KEY CLUSTERED 
(
	[MakeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CODELOG]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CODELOG](
	[TableName] [varchar](50) NOT NULL,
	[Prefix] [varchar](2) NOT NULL,
	[LargeCode] [numeric](18, 0) NOT NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[TableName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Prefix] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[COUNTRY]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[COUNTRY](
	[CountryCode] [varchar](4) NOT NULL,
	[Country] [nvarchar](50) NOT NULL,
	[CImage] [image] NULL,
PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[COUPON]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[COUPON](
	[CouponID] [varchar](50) NOT NULL,
	[Number] [varchar](50) NOT NULL,
	[Station] [nvarchar](50) NOT NULL,
	[Amount] [float] NOT NULL,
	[SNRange] [varchar](50) NOT NULL,
	[IssuedDate] [date] NULL,
	[ExpireDate] [date] NULL,
	[Quantity] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CouponID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DRIVER]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DRIVER](
	[DriverID] [varchar](50) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[MiddleName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NULL,
	[Gender] [nvarchar](6) NOT NULL,
	[BirthDate] [datetime] NULL,
	[Phone] [varchar](14) NULL,
	[License] [varchar](50) NOT NULL,
	[Vehicle] [varchar](50) NULL,
	[DImage] [image] NULL,
	[RegDate] [datetime] NOT NULL,
	[RUser] [varchar](50) NOT NULL,
 CONSTRAINT [PK__DRIVER__F1B1CD24C68DCD3B] PRIMARY KEY CLUSTERED 
(
	[DriverID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DRIVERGUARANTORS]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DRIVERGUARANTORS](
	[DGID] [varchar](50) NOT NULL,
	[Driver] [varchar](50) NOT NULL,
	[Guarantor] [varchar](50) NOT NULL,
	[Organization] [nvarchar](50) NULL,
	[LetterReference] [nvarchar](20) NULL,
	[RegDate] [datetime] NOT NULL,
	[RUser] [varchar](50) NOT NULL,
 CONSTRAINT [PK__DRIVERGU__28D33E76F017B02B] PRIMARY KEY CLUSTERED 
(
	[DGID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FUEL]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FUEL](
	[FuelID] [varchar](50) NOT NULL,
	[FName] [nvarchar](50) NOT NULL,
	[CurrentPrice] [money] NOT NULL,
	[CPriceStartDate] [date] NOT NULL,
	[FDescription] [varchar](100) NULL,
	[RegDate] [datetime] NOT NULL,
	[RUser] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FuelID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FUELCONSUMED]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FUELCONSUMED](
	[FCID] [varchar](50) NOT NULL,
	[IssuedNo] [varchar](50) NOT NULL,
	[Gauge] [int] NOT NULL,
	[ReceiptNo] [varchar](50) NOT NULL,
	[ReturnDate] [date] NOT NULL,
	[RegDate] [datetime] NOT NULL,
	[RUser] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FCID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FUELISSUED]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FUELISSUED](
	[IssueID] [varchar](50) NOT NULL,
	[Vehicle] [varchar](50) NOT NULL,
	[Driver] [varchar](50) NOT NULL,
	[Gauge] [int] NOT NULL,
	[Amount] [int] NOT NULL,
	[issueDate] [date] NOT NULL,
	[Destination] [varchar](50) NOT NULL,
	[Purpose] [varchar](100) NOT NULL,
	[Duration] [int] NOT NULL,
	[PaymentType] [int] NOT NULL,
	[PaymentReference] [nvarchar](50) NULL,
	[Quantity] [int] NULL,
	[Status] [varchar](1) NOT NULL,
	[RegDate] [date] NOT NULL,
	[RUser] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IssueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Vehicle] ASC,
	[Driver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GUARANTOR]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GUARANTOR](
	[GuarantorID] [varchar](50) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[MiddleName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NULL,
	[Gender] [nvarchar](6) NOT NULL,
	[BirthDate] [datetime] NULL,
	[Phone] [varchar](14) NULL,
	[GImage] [image] NULL,
	[RegDate] [datetime] NOT NULL,
	[RUser] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[GuarantorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LANG]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LANG](
	[LangID] [varchar](50) NOT NULL,
	[Lan] [varchar](2) NOT NULL,
	[CommandID] [varchar](50) NOT NULL,
	[CommandText] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[LangID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LICENSE]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LICENSE](
	[LicenseID] [varchar](50) NOT NULL,
	[LName] [nvarchar](50) NOT NULL,
	[LLevel] [nvarchar](50) NOT NULL,
	[LDescription] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[LicenseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOG]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOG](
	[LogNo] [int] NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[Login] [datetime] NOT NULL,
	[Logout] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[LogNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LUBRICANT]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LUBRICANT](
	[LubricantID] [varchar](50) NOT NULL,
	[LName] [nvarchar](50) NOT NULL,
	[LType] [nvarchar](50) NOT NULL,
	[CurrentPrice] [money] NOT NULL,
	[CPriceStartDate] [date] NOT NULL,
	[LDescription] [varchar](100) NULL,
	[RegDate] [datetime] NOT NULL,
	[RUser] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[LubricantID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PRICEHISTORY]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PRICEHISTORY](
	[PHID] [varchar](50) NOT NULL,
	[PHType] [varchar](50) NOT NULL,
	[ItemID] [varchar](50) NOT NULL,
	[StartDate] [date] NOT NULL,
	[EndDate] [date] NOT NULL,
	[RegDate] [date] NOT NULL,
	[RUser] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PHID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RECEIPT]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RECEIPT](
	[ReceiptID] [varchar](50) NOT NULL,
	[Driver] [varchar](50) NOT NULL,
	[PromptDate] [date] NOT NULL,
	[RegDate] [datetime] NOT NULL,
	[RUser] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ReceiptID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RECEIPTDETAIL]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RECEIPTDETAIL](
	[RDID] [varchar](50) NOT NULL,
	[Reciept] [varchar](50) NULL,
	[SerialNo] [varchar](20) NOT NULL,
	[Date] [date] NOT NULL,
	[Station] [nvarchar](50) NULL,
	[Amount] [float] NOT NULL,
	[Cashier] [nvarchar](50) NULL,
	[VATInclusive] [bit] NOT NULL,
	[Attachment] [image] NULL,
PRIMARY KEY CLUSTERED 
(
	[RDID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TIRE]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TIRE](
	[TireID] [varchar](50) NOT NULL,
	[DOTidentificationNo] [varchar](50) NULL,
	[TName] [nvarchar](50) NOT NULL,
	[Brand] [nvarchar](50) NOT NULL,
	[MaxLoad] [int] NULL,
	[Width] [int] NULL,
	[CurrentPrice] [money] NOT NULL,
	[CPriceStartDate] [date] NOT NULL,
	[TDescription] [nvarchar](100) NULL,
	[RegDate] [datetime] NOT NULL,
	[RUser] [varchar](50) NOT NULL,
 CONSTRAINT [PK__TIRE__6FA3AEC9E3236FF5] PRIMARY KEY CLUSTERED 
(
	[TireID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[USERS]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[USERS](
	[UserID] [varchar](50) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[Password] [nvarchar](100) NOT NULL,
	[RegDate] [datetime] NOT NULL,
	[RUser] [varchar](50) NOT NULL,
 CONSTRAINT [PK__USERS__1788CCAC2B615F60] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VEHICLE]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VEHICLE](
	[VehicleID] [varchar](50) NOT NULL,
	[VType] [varchar](50) NOT NULL,
	[PlateNo] [nvarchar](50) NOT NULL,
	[Make] [varchar](50) NOT NULL,
	[MotorNo] [nvarchar](50) NOT NULL,
	[ChassisNo] [nvarchar](50) NOT NULL,
	[ManufacturedYear] [int] NULL,
	[ManufacturedCountry] [varchar](4) NULL,
	[PurchaseDate] [date] NULL,
	[FuelType] [varchar](50) NOT NULL,
	[Mileage] [float] NOT NULL,
	[StartGageReading] [int] NULL,
	[FuelReserve] [numeric](18, 0) NULL,
	[TankerCapacity] [numeric](18, 0) NULL,
	[VStatus] [varchar](50) NOT NULL,
	[VImage] [image] NULL,
	[RegDate] [datetime] NOT NULL,
	[RUser] [varchar](50) NOT NULL,
 CONSTRAINT [PK__VEHICLE__476B54B29FE4B50D] PRIMARY KEY CLUSTERED 
(
	[VehicleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VEHICLELUBRICANT]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VEHICLELUBRICANT](
	[VLID] [varchar](50) NOT NULL,
	[Vehicle] [varchar](50) NOT NULL,
	[Lubricant] [varchar](50) NOT NULL,
	[RegDate] [datetime] NOT NULL,
	[RUser] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[VLID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VEHICLETIRE]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VEHICLETIRE](
	[VTID] [varchar](50) NOT NULL,
	[Vehicle] [varchar](50) NOT NULL,
	[Tire] [varchar](50) NOT NULL,
	[RegDate] [datetime] NOT NULL,
	[RUser] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[VTID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VEHICLETYPE]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VEHICLETYPE](
	[TypeID] [varchar](50) NOT NULL,
	[TName] [nvarchar](50) NOT NULL,
	[TDescription] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[TypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[DRIVER]  WITH CHECK ADD  CONSTRAINT [_DriverLicense] FOREIGN KEY([License])
REFERENCES [dbo].[LICENSE] ([LicenseID])
GO
ALTER TABLE [dbo].[DRIVER] CHECK CONSTRAINT [_DriverLicense]
GO
ALTER TABLE [dbo].[DRIVER]  WITH CHECK ADD  CONSTRAINT [_DRUser] FOREIGN KEY([RUser])
REFERENCES [dbo].[USERS] ([UserID])
GO
ALTER TABLE [dbo].[DRIVER] CHECK CONSTRAINT [_DRUser]
GO
ALTER TABLE [dbo].[DRIVER]  WITH CHECK ADD  CONSTRAINT [_VehicleDriver] FOREIGN KEY([Vehicle])
REFERENCES [dbo].[VEHICLE] ([VehicleID])
GO
ALTER TABLE [dbo].[DRIVER] CHECK CONSTRAINT [_VehicleDriver]
GO
ALTER TABLE [dbo].[DRIVERGUARANTORS]  WITH CHECK ADD  CONSTRAINT [_DriverGuars] FOREIGN KEY([Driver])
REFERENCES [dbo].[DRIVER] ([DriverID])
GO
ALTER TABLE [dbo].[DRIVERGUARANTORS] CHECK CONSTRAINT [_DriverGuars]
GO
ALTER TABLE [dbo].[DRIVERGUARANTORS]  WITH CHECK ADD  CONSTRAINT [_GuarantorDr] FOREIGN KEY([Guarantor])
REFERENCES [dbo].[GUARANTOR] ([GuarantorID])
GO
ALTER TABLE [dbo].[DRIVERGUARANTORS] CHECK CONSTRAINT [_GuarantorDr]
GO
ALTER TABLE [dbo].[FUEL]  WITH CHECK ADD  CONSTRAINT [_FRUser] FOREIGN KEY([RUser])
REFERENCES [dbo].[USERS] ([UserID])
GO
ALTER TABLE [dbo].[FUEL] CHECK CONSTRAINT [_FRUser]
GO
ALTER TABLE [dbo].[FUELCONSUMED]  WITH CHECK ADD  CONSTRAINT [_FCRUser] FOREIGN KEY([RUser])
REFERENCES [dbo].[USERS] ([UserID])
GO
ALTER TABLE [dbo].[FUELCONSUMED] CHECK CONSTRAINT [_FCRUser]
GO
ALTER TABLE [dbo].[FUELCONSUMED]  WITH CHECK ADD  CONSTRAINT [_FuelIssedCons] FOREIGN KEY([IssuedNo])
REFERENCES [dbo].[FUELISSUED] ([IssueID])
GO
ALTER TABLE [dbo].[FUELCONSUMED] CHECK CONSTRAINT [_FuelIssedCons]
GO
ALTER TABLE [dbo].[FUELCONSUMED]  WITH CHECK ADD  CONSTRAINT [_FuelReceipt] FOREIGN KEY([ReceiptNo])
REFERENCES [dbo].[RECEIPT] ([ReceiptID])
GO
ALTER TABLE [dbo].[FUELCONSUMED] CHECK CONSTRAINT [_FuelReceipt]
GO
ALTER TABLE [dbo].[FUELISSUED]  WITH CHECK ADD  CONSTRAINT [_DriverFuelConsumption] FOREIGN KEY([Driver])
REFERENCES [dbo].[DRIVER] ([DriverID])
GO
ALTER TABLE [dbo].[FUELISSUED] CHECK CONSTRAINT [_DriverFuelConsumption]
GO
ALTER TABLE [dbo].[FUELISSUED]  WITH CHECK ADD  CONSTRAINT [_FIUser] FOREIGN KEY([RUser])
REFERENCES [dbo].[USERS] ([UserID])
GO
ALTER TABLE [dbo].[FUELISSUED] CHECK CONSTRAINT [_FIUser]
GO
ALTER TABLE [dbo].[FUELISSUED]  WITH CHECK ADD  CONSTRAINT [_VehicleFuelConsumption] FOREIGN KEY([Vehicle])
REFERENCES [dbo].[VEHICLE] ([VehicleID])
GO
ALTER TABLE [dbo].[FUELISSUED] CHECK CONSTRAINT [_VehicleFuelConsumption]
GO
ALTER TABLE [dbo].[LOG]  WITH CHECK ADD  CONSTRAINT [_LoggedUser] FOREIGN KEY([UserName])
REFERENCES [dbo].[USERS] ([UserID])
GO
ALTER TABLE [dbo].[LOG] CHECK CONSTRAINT [_LoggedUser]
GO
ALTER TABLE [dbo].[LUBRICANT]  WITH CHECK ADD  CONSTRAINT [_LRUser] FOREIGN KEY([RUser])
REFERENCES [dbo].[USERS] ([UserID])
GO
ALTER TABLE [dbo].[LUBRICANT] CHECK CONSTRAINT [_LRUser]
GO
ALTER TABLE [dbo].[PRICEHISTORY]  WITH CHECK ADD  CONSTRAINT [_PHRUser] FOREIGN KEY([RUser])
REFERENCES [dbo].[USERS] ([UserID])
GO
ALTER TABLE [dbo].[PRICEHISTORY] CHECK CONSTRAINT [_PHRUser]
GO
ALTER TABLE [dbo].[RECEIPT]  WITH CHECK ADD  CONSTRAINT [_DriversReceipt] FOREIGN KEY([Driver])
REFERENCES [dbo].[DRIVER] ([DriverID])
GO
ALTER TABLE [dbo].[RECEIPT] CHECK CONSTRAINT [_DriversReceipt]
GO
ALTER TABLE [dbo].[RECEIPT]  WITH CHECK ADD  CONSTRAINT [_ReceiptRUser] FOREIGN KEY([RUser])
REFERENCES [dbo].[USERS] ([UserID])
GO
ALTER TABLE [dbo].[RECEIPT] CHECK CONSTRAINT [_ReceiptRUser]
GO
ALTER TABLE [dbo].[TIRE]  WITH CHECK ADD  CONSTRAINT [_TRUser] FOREIGN KEY([RUser])
REFERENCES [dbo].[USERS] ([UserID])
GO
ALTER TABLE [dbo].[TIRE] CHECK CONSTRAINT [_TRUser]
GO
ALTER TABLE [dbo].[VEHICLE]  WITH CHECK ADD  CONSTRAINT [_CarMake] FOREIGN KEY([Make])
REFERENCES [dbo].[CARMAKE] ([MakeID])
GO
ALTER TABLE [dbo].[VEHICLE] CHECK CONSTRAINT [_CarMake]
GO
ALTER TABLE [dbo].[VEHICLE]  WITH CHECK ADD  CONSTRAINT [_VehicleCountry] FOREIGN KEY([ManufacturedCountry])
REFERENCES [dbo].[COUNTRY] ([CountryCode])
GO
ALTER TABLE [dbo].[VEHICLE] CHECK CONSTRAINT [_VehicleCountry]
GO
ALTER TABLE [dbo].[VEHICLE]  WITH CHECK ADD  CONSTRAINT [_VehicleFuel] FOREIGN KEY([FuelType])
REFERENCES [dbo].[FUEL] ([FuelID])
GO
ALTER TABLE [dbo].[VEHICLE] CHECK CONSTRAINT [_VehicleFuel]
GO
ALTER TABLE [dbo].[VEHICLE]  WITH CHECK ADD  CONSTRAINT [_VehicleType] FOREIGN KEY([VType])
REFERENCES [dbo].[VEHICLETYPE] ([TypeID])
GO
ALTER TABLE [dbo].[VEHICLE] CHECK CONSTRAINT [_VehicleType]
GO
ALTER TABLE [dbo].[VEHICLE]  WITH CHECK ADD  CONSTRAINT [_VRuser] FOREIGN KEY([RUser])
REFERENCES [dbo].[USERS] ([UserID])
GO
ALTER TABLE [dbo].[VEHICLE] CHECK CONSTRAINT [_VRuser]
GO
ALTER TABLE [dbo].[VEHICLELUBRICANT]  WITH CHECK ADD  CONSTRAINT [_Lubrication] FOREIGN KEY([Lubricant])
REFERENCES [dbo].[LUBRICANT] ([LubricantID])
GO
ALTER TABLE [dbo].[VEHICLELUBRICANT] CHECK CONSTRAINT [_Lubrication]
GO
ALTER TABLE [dbo].[VEHICLELUBRICANT]  WITH CHECK ADD  CONSTRAINT [_VehicleLub] FOREIGN KEY([Vehicle])
REFERENCES [dbo].[VEHICLE] ([VehicleID])
GO
ALTER TABLE [dbo].[VEHICLELUBRICANT] CHECK CONSTRAINT [_VehicleLub]
GO
ALTER TABLE [dbo].[VEHICLELUBRICANT]  WITH CHECK ADD  CONSTRAINT [_VLRUser] FOREIGN KEY([RUser])
REFERENCES [dbo].[USERS] ([UserID])
GO
ALTER TABLE [dbo].[VEHICLELUBRICANT] CHECK CONSTRAINT [_VLRUser]
GO
ALTER TABLE [dbo].[VEHICLETIRE]  WITH CHECK ADD  CONSTRAINT [_VehicleTr1] FOREIGN KEY([Vehicle])
REFERENCES [dbo].[VEHICLE] ([VehicleID])
GO
ALTER TABLE [dbo].[VEHICLETIRE] CHECK CONSTRAINT [_VehicleTr1]
GO
ALTER TABLE [dbo].[VEHICLETIRE]  WITH CHECK ADD  CONSTRAINT [_VehicleTr2] FOREIGN KEY([Tire])
REFERENCES [dbo].[TIRE] ([TireID])
GO
ALTER TABLE [dbo].[VEHICLETIRE] CHECK CONSTRAINT [_VehicleTr2]
GO
ALTER TABLE [dbo].[VEHICLETIRE]  WITH CHECK ADD  CONSTRAINT [_VTRUser] FOREIGN KEY([RUser])
REFERENCES [dbo].[USERS] ([UserID])
GO
ALTER TABLE [dbo].[VEHICLETIRE] CHECK CONSTRAINT [_VTRUser]
GO
/****** Object:  StoredProcedure [dbo].[getCountries]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getCountries]
AS 
	SELECT * FROM [COUNTRY]
	ORDER BY [COUNTRY] ASC

GO
/****** Object:  StoredProcedure [dbo].[getCountryNameByID]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getCountryNameByID]
	@CountryName varchar(50)
AS
	SELECT	TOP 1 CountryCode
	FROM	[COUNTRY]
	WHERE	Country = @CountryName

GO
/****** Object:  StoredProcedure [dbo].[getCurrentTime]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getCurrentTime]
AS
	SELECT CURRENT_TIMESTAMP AS [CurrentTime]

GO
/****** Object:  StoredProcedure [dbo].[getDriverInfo]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getDriverInfo]

AS
	-- With LICENSE
	SELECT	ROW_NUMBER() OVER (ORDER BY DriverID) AS [SNo],
			FirstName as [First Name], 
			MiddleName as [Middle Name],
			LastName as [Last Name],
			Gender, BirthDate as [Birth Date],
			Phone, [LICENSE].[LName] AS License,
			Vehicle
	INTO	#TempDriverLicense
	FROM	[DRIVER], [LICENSE]
	WHERE	[DRIVER].License = [LICENSE].[LicenseID]

	-- With VEHICLE
	SELECT  SNo, [First Name], [Middle Name], [Last Name],
			Gender, [Birth Date], Phone, License, 
			([VEHICLE].VehicleID), [VEHICLE].Make, [VEHICLE].PlateNo
	INTO	#TempDriverVehicle
	FROM	#TempDriverLicense, [VEHICLE]
	WHERE	#TempDriverLicense.Vehicle = [VEHICLE].VehicleID

	-- With CARMAKE
	SELECT	SNo, [First Name], [Middle Name], [Last Name],
			Gender, [Birth Date], Phone, License, 
			([CARMAKE].Make + ' ' + [CARMAKE].Model + ' ' + PlateNo) as Vehicle 
	INTO	#TempFinal
	FROM	#TempDriverVehicle, [CARMAKE]
	WHERE	#TempDriverVehicle.Make = [CARMAKE].MakeID

	SELECT	*
	FROM	#TempFinal

	DROP	TABLE #TempDriverLicense
	DROP	TABLE #TempDriverVehicle
	DROP	TABLE #TempFinal


GO
/****** Object:  StoredProcedure [dbo].[getFuelIDbyName]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getFuelIDbyName]
	@FName varchar(50)
AS
	SELECT	TOP 1 FuelID
	FROM	[FUEL]
	WHERE	FName = @FName

GO
/****** Object:  StoredProcedure [dbo].[getFuelTypes]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getFuelTypes]

AS 
	SELECT FName FROM [FUEL]

GO
/****** Object:  StoredProcedure [dbo].[getGuarantor]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[getGuarantor]

AS
	SELECT	GuarantorID, FirstName, MiddleName, LastName
	FROM	[GUARANTOR]

GO
/****** Object:  StoredProcedure [dbo].[getLicenseInfo]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getLicenseInfo]
	@LID as VARCHAR(50)
AS 
	SELECT	LicenseID, LName, LLevel, LDescription FROM [LICENSE]
	WHERE	LicenseID LIKE @LID

GO
/****** Object:  StoredProcedure [dbo].[getLubricantInfo]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getLubricantInfo]
	@LID as VARCHAR(50)
AS 
	SELECT	* FROM [LUBRICANT]
	WHERE	LubricantID LIKE @LID

GO
/****** Object:  StoredProcedure [dbo].[getNewCode]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getNewCode]
	@TName VARCHAR(50)
AS
	SELECT	Prefix, LargeCode
	FROM	CODELOG
	WHERE	TableName = @TName

GO
/****** Object:  StoredProcedure [dbo].[getTireInfo]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getTireInfo]
	@TID as VARCHAR(50)
AS 
	SELECT	* FROM [TIRE]
	WHERE	TireID LIKE @TID

GO
/****** Object:  StoredProcedure [dbo].[getUserByUserName]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getUserByUserName]
	@UName varchar(50)
AS
	SELECT	*
	FROM	[USERS]
	WHERE	UserName = @UName

GO
/****** Object:  StoredProcedure [dbo].[getVehicleDriver]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getVehicleDriver]
	@VID varchar(50)
AS
	SELECT	[DriverID]
      ,[FirstName]
      ,[MiddleName]
      ,[LastName]
      ,[Gender]
      ,[BirthDate]
      ,[Phone]
      ,[License]
      ,[Vehicle]
      ,[DImage]
      ,[RegDate]
      ,[RUser]
	FROM	[DRIVER]
	WHERE	Vehicle LIKE @VID

GO
/****** Object:  StoredProcedure [dbo].[getVehicleInfo]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getVehicleInfo]
	@VID as VARCHAR(50)
AS
	-- With CarMake 
	SELECT	ROW_NUMBER() OVER (ORDER BY VehicleID) AS [SNo],
			VehicleID, PlateNo, MotorNo, ChassisNo, VType, FuelType, Mileage, 
			[CARMAKE].Make, [CARMAKE].Model, ManufacturedCountry, ManufacturedYear, 
			PurchaseDate, StartGageReading, FuelReserve, TankerCapacity, VStatus, VImage
	INTO	#TempVehicleCarMake
	FROM	[VEHICLE], [CARMAKE]
	WHERE	[VEHICLE].[Make] = [CARMAKE].[MakeID]

	-- With Fuel
	SELECT  SNo, VehicleID, PlateNo, MotorNo, ChassisNo, VType, FUEL.FName, 
			Mileage, Make, Model, ManufacturedCountry, ManufacturedYear, 
			PurchaseDate, StartGageReading, FuelReserve, TankerCapacity, VStatus, VImage
	INTO	#TempVehicleFuel
	FROM	#TempVehicleCarMake, FUEL
	WHERE	#TempVehicleCarMake.FuelType = FUEL.FuelID

	-- With Type
	SELECT	SNo, VehicleID, PlateNo, MotorNo, ChassisNo, [VEHICLETYPE].TName AS [VType], 
			FName, Mileage, Make, Model, ManufacturedCountry, ManufacturedYear, 
			PurchaseDate, StartGageReading, FuelReserve, TankerCapacity, VStatus, VImage
	INTO	#TempVehicleCountry
	FROM	#TempVehicleFuel, VEHICLETYPE
	WHERE	#TempVehicleFuel.VType = VEHICLETYPE.TypeID

	SELECT	SNo, VehicleID, PlateNo, MotorNo, ChassisNo, VType, 
			FName, Mileage, Make, Model, [COUNTRY].Country, 
			ManufacturedYear, PurchaseDate, StartGageReading, 
			FuelReserve, TankerCapacity, VStatus, VImage
	INTO	#TempVehicleInfo
	FROM	#TempVehicleCountry, [COUNTRY]
	WHERE	#TempVehicleCountry.ManufacturedCountry = COUNTRY.CountryCode
	
	SELECT	*
	FROM	#TempVehicleInfo
	WHERE	VehicleID LIKE @VID

	DROP	TABLE #TempVehicleInfo
	DROP	TABLE #TempVehicleCountry 
	DROP	TABLE #TempVehicleFuel
	DROP	TABLE #TempVehicleCarMake


GO
/****** Object:  StoredProcedure [dbo].[getVehicleLubricant]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getVehicleLubricant]
	@VID as varchar(50)
AS
	SELECT	DISTINCT LubricantID, LName, LType, CurrentPrice, CPriceStartDate, LDescription
	FROM	[VEHICLELUBRICANT] LEFT JOIN [LUBRICANT] 
			ON [VEHICLELUBRICANT].Lubricant = [LUBRICANT].LubricantID
	WHERE	Vehicle LIKE @VID


GO
/****** Object:  StoredProcedure [dbo].[getVehicleMakeID]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getVehicleMakeID]
	@Make varchar(50),
	@Model varchar(50)
AS
	SELECT	TOP 1 MakeID
	FROM	[CARMAKE]
	WHERE	[Make] = @Make AND [Model] = @Model

GO
/****** Object:  StoredProcedure [dbo].[getVehicleMakes]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getVehicleMakes]  
AS 
	SELECT DISTINCT Make FROM [CARMAKE]

GO
/****** Object:  StoredProcedure [dbo].[getVehicleModels]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getVehicleModels]
	@Make varchar(50)
AS 
	SELECT DISTINCT Model FROM [CARMAKE] WHERE [Make] = @Make

GO
/****** Object:  StoredProcedure [dbo].[getVehicleTire]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getVehicleTire]
	@VID varchar(50)
AS
	SELECT	DISTINCT TireID, DOTidentificationNo, TName, Brand, CurrentPrice, MaxLoad, Width, CPriceStartDate, TDescription
	FROM	[TIRE] LEFT JOIN [VEHICLETIRE]
			ON [TIRE].TireID = [VEHICLETIRE].Tire
	WHERE	Vehicle LIKE @VID

GO
/****** Object:  StoredProcedure [dbo].[getVehicleTypeIDbyType]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getVehicleTypeIDbyType]
	@TName varchar(50)
AS
	SELECT	TOP 1 TypeID
	FROM	[VEHICLETYPE]
	WHERE	TName = @TName

GO
/****** Object:  StoredProcedure [dbo].[getVehicleTypes]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getVehicleTypes]

AS 
	SELECT TName FROM [VEHICLETYPE]

GO
/****** Object:  StoredProcedure [dbo].[registerCoupon]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[registerCoupon]
	@CouponID varchar(50),
	@Number varchar(50),
	@Station nvarchar(50),
	@Amount float,
	@SNRange varchar(50),
	@IssuedDate date,
	@ExpireDate date,
	@Quantity int  
AS 
	SET NOCOUNT ON;
	INSERT INTO COUPON (
		CouponID,
		[Number],
		Station,
		Amount,
		SNRange,
		IssuedDate,
		ExpireDate,
		Quantity
	)
	VALUES (
		@CouponID,
		@Number,
		@Station,
		@Amount,
		@SNRange,
		@IssuedDate,
		@ExpireDate,
		@Quantity
	)

GO
/****** Object:  StoredProcedure [dbo].[registerDriver]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[registerDriver]
	@DriverID varchar(50),
	@FirstName nvarchar(50),
	@MiddleName nvarchar(50),
	@LastName nvarchar(50),
	@Gender nvarchar(6),
	@BirthDate datetime,
	@Phone varchar(14),
	@License varchar(50),
	@Vehicle varchar(50),
	@DImage image,
	@RegDate datetime,
	@RUser varchar(50)
AS 
	SET NOCOUNT ON;
	INSERT INTO DRIVER (
		DriverID,
		FirstName,
		MiddleName,
		LastName,
		Gender,
		BirthDate,
		Phone,
		License,
		Vehicle,
		DImage,
		RegDate,
		RUser
	)
	VALUES (
		@DriverID,
		@FirstName,
		@MiddleName,
		@LastName,
		@Gender,
		@BirthDate,
		@Phone,
		@License, 
		@Vehicle,
		@DImage,
		@RegDate,
		@RUser
	)

GO
/****** Object:  StoredProcedure [dbo].[registerDriverGuarantor]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[registerDriverGuarantor]
	@DGID varchar(50),
	@Driver varchar(50),
	@Guarantor varchar(50),
	@Organization nvarchar(50),
	@LetterReference nvarchar(20),
	@RegDate datetime,
	@RUser varchar(50)
AS 
	SET NOCOUNT ON;
	INSERT INTO DRIVERGUARANTORS(
		DGID,
		Driver,
		Guarantor,
		Organization,
		LetterReference,
		RegDate,
		RUser
	)
	VALUES (
		@DGID,
		@Driver,
		@Guarantor,
		@Organization,
		@LetterReference,
		@RegDate,
		@RUser
	)

GO
/****** Object:  StoredProcedure [dbo].[registerGuarantor]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[registerGuarantor]
	@GuarantorID varchar(50),
	@FirstName nvarchar(50),
	@MiddleName nvarchar(50),
	@LastName nvarchar(50),
	@Gender nvarchar(6),
	@BirthDate datetime,
	@Phone varchar(14),
	@GImage image,
	@RegDate datetime,
	@RUser varchar(50)
AS 
	SET NOCOUNT ON;
	INSERT INTO GUARANTOR (
		GuarantorID,
		FirstName ,
		MiddleName,
		LastName,
		Gender,
		BirthDate,
		Phone,
		GImage,
		RegDate,
		RUser
	)
	VALUES (
		@GuarantorID,
		@FirstName ,
		@MiddleName,
		@LastName,
		@Gender,
		@BirthDate,
		@Phone,
		@GImage,
		@RegDate,
		@RUser
	)

GO
/****** Object:  StoredProcedure [dbo].[registerLicense]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[registerLicense]
	@LicenseID varchar(50),
	@LName nvarchar(50),
	@LLevel nvarchar(50),
	@LDescription nvarchar(100)
AS 
	SET NOCOUNT ON;
	INSERT INTO LICENSE (
		LicenseID,
		LName,
		LLevel,
		LDescription
	)
	VALUES (
		@LicenseID,
		@LName,
		@LLevel,
		@LDescription
	)

GO
/****** Object:  StoredProcedure [dbo].[registerLog]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[registerLog]
	@LogNo numeric,
	@UserName varchar(50),
	@Login datetime,
	@Logout datetime
AS 
	SET NOCOUNT ON;
	INSERT INTO [LOG] (
		LogNo,
		UserName,
		Login,
		Logout
	)
	VALUES (
		@LogNo,
		@UserName,
		@Login,
		@Logout
	)

GO
/****** Object:  StoredProcedure [dbo].[registerLubricant]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[registerLubricant]
	@LubricantID varchar(50),
	@LName nvarchar(50),
	@LType nvarchar(50),
	@CurrentPrice money,
	@CPriceStartDate date,
	@LDescription varchar(100),
	@RegDate datetime,
	@RUser varchar(50)
AS 
	SET NOCOUNT ON;
	INSERT INTO LUBRICANT (
		LubricantID,
		LName,
		LType,
		CurrentPrice,
		CPriceStartDate,
		LDescription,
		RegDate,
		RUser
	)
	VALUES (
		@LubricantID,
		@LName,
		@LType,
		@CurrentPrice,
		@CPriceStartDate,
		@LDescription,
		@RegDate,
		@RUser
	)

GO
/****** Object:  StoredProcedure [dbo].[registerPriceHistory]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[registerPriceHistory]
	@PHID varchar(50),
	@PHType varchar(50),
	@ItemID varchar(50),
	@StartDate date,
	@EndDate date,
	@RegDate date,
	@RUser varchar(50)
AS 
	SET NOCOUNT ON;
	INSERT INTO PRICEHISTORY (
		PHID,
		PHType,
		ItemID,
		StartDate,
		EndDate,
		RegDate,
		RUser
	)
	VALUES (
		@PHID,
		@PHType,
		@ItemID,
		@StartDate,
		@EndDate,
		@RegDate,
		@RUser
	)

GO
/****** Object:  StoredProcedure [dbo].[registerReceipt]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[registerReceipt]
	@ReceiptID varchar(50),
	@Driver varchar(50),
	@PromptDate date,
	@RegDate datetime,
	@RUser varchar(50)
AS 
	SET NOCOUNT ON;
	INSERT INTO RECEIPT (
		ReceiptID,
		Driver,
		PromptDate,
		RegDate,
		RUser
	)
	VALUES (
		@ReceiptID,
		@Driver,
		@PromptDate,
		@RegDate,
		@RUser
	)

GO
/****** Object:  StoredProcedure [dbo].[registerReceiptDetail]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[registerReceiptDetail]
	@RDID varchar(50) ,
	@Reciept varchar(50), 
	@SerialNo varchar(20),
	@Date date,
	@Station nvarchar(50),
	@Amount float,
	@Cashier nvarchar(50),
	@VATInclusive bit,
	@Attachment image
AS 
	SET NOCOUNT ON;
	INSERT INTO RECEIPTDETAIL (
		RDID,
		Reciept, 
		SerialNo,
		[Date],
		Station,
		Amount,
		Cashier,
		VATInclusive,
		Attachment
	)
	VALUES (
		@RDID,
		@Reciept, 
		@SerialNo,
		@Date,
		@Station,
		@Amount,
		@Cashier,
		@VATInclusive,
		@Attachment
	)

GO
/****** Object:  StoredProcedure [dbo].[registerTire]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[registerTire]
	@TireID varchar(50),
	@DOTidentificationNo varchar(50),
	@TName nvarchar(50),
	@Brand nvarchar(50),
	@MaxLoad int,
	@Width int, 
	@CurrentPrice money,
	@CPriceStartDate date,
	@TDescription varchar(100),
	@RegDate datetime,
	@RUser varchar(50)
AS 
	SET NOCOUNT ON;
	INSERT INTO [TIRE] (
		TireID,
		DOTidentificationNo,
		TName,
		Brand,
		MaxLoad,
		Width,
		CurrentPrice,
		CPriceStartDate,
		TDescription,
		RegDate,
		RUser
	)
	VALUES (
		@TireID,
		@DOTidentificationNo,
		@TName,
		@Brand,
		@MaxLoad,
		@Width,
		@CurrentPrice,
		@CPriceStartDate,
		@TDescription,
		@RegDate,
		@RUser
	)

GO
/****** Object:  StoredProcedure [dbo].[registerUser]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[registerUser]
	@UserID varchar(50),
	@UserName varchar(50),
	@Password varchar(2000),
	@RegDate datetime,
	@RUser varchar(50)
AS 
	SET NOCOUNT ON;
	INSERT INTO USERS (
		UserID,
		UserName,
		Password,
		RegDate,
		RUser
	)
	VALUES (
		@UserID,
		@UserName,
		@Password,
		@RegDate,
		@RUser
	)

GO
/****** Object:  StoredProcedure [dbo].[registerVehicle]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[registerVehicle]
    @VehicleID varchar(50), 
    @VType varchar(50),
	@PlateNo nvarchar(50),
	@Make nvarchar(50),
	@MotorNo nvarchar(50),
	@ChassisNo nvarchar(50),
	@ManufacturedYear int,
	@ManufacturedCountry nvarchar(50),
	@PurchaseDate date,
	@FuelType varchar(50),
	@Mileage float,
	@StartGageReading int,
	@VStatus varchar(50),
	@VImage image,
	@RUser varchar(50)
AS 
	DECLARE @TempDate as datetime
	SET @TempDate = (SELECT GETDATE())
	INSERT INTO VEHICLE (
		VehicleID, VType, PlateNo, Make, MotorNo, ChassisNo, 
		ManufacturedYear, ManufacturedCountry, PurchaseDate, FuelType, 
		Mileage, StartGageReading, VStatus, VImage, RegDate, RUser
	)
	VALUES (
		@VehicleID, @VType, @PlateNo, @Make, @MotorNo, @ChassisNo, 
		@ManufacturedYear, @ManufacturedCountry, @PurchaseDate, @FuelType,
		@Mileage, @StartGageReading, @VStatus, @VImage, @TempDate, @RUser
	)

GO
/****** Object:  StoredProcedure [dbo].[registerVehicleLubricant]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[registerVehicleLubricant]
	@VLID varchar(50),
	@Vehicle varchar(50),
	@Lubricant varchar(50),
	@RUser varchar(50)
AS
	DECLARE @TempDate as datetime
	SET @TempDate = (SELECT GETDATE())
	INSERT INTO [VEHICLELUBRICANT] (
		VLID,
		Vehicle,
		Lubricant,
		RegDate,
		RUser
	)
	VALUES (
		@VLID,
		@Vehicle,
		@Lubricant,
		@TempDate,
		@RUser
	)

GO
/****** Object:  StoredProcedure [dbo].[registerVehicleTire]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[registerVehicleTire]
	@VTID varchar(50),
	@Vehicle varchar(50),
	@Tire varchar(50),
	@RUser varchar(50)
AS 
	DECLARE @TempDate as datetime
	SET @TempDate = (SELECT GETDATE())
	INSERT INTO VEHICLETIRE (
		VTID,
		Vehicle,
		Tire,
		RegDate,
		RUser
	)
	VALUES (
		@VTID,
		@Vehicle,
		@Tire,
		@TempDate,
		@RUser
	)

GO
/****** Object:  StoredProcedure [dbo].[registerVehicleType]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[registerVehicleType]
	@TypeID varchar(50), 
	@TName nvarchar(50), 
	@TDescription nvarchar(100)
AS 
	SET NOCOUNT ON;
	INSERT INTO VEHICLETYPE (
		TypeID, 
		TName, 
		TDescription
	)
	VALUES (
		@TypeID, 
		@TName, 
		@TDescription
	)

GO
/****** Object:  StoredProcedure [dbo].[updateCodeLog]    Script Date: 2/12/2016 2:19:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[updateCodeLog]
	@LargeCode numeric,
	@TableName varchar(50)
AS 
	UPDATE	[CODELOG]
	SET		[LargeCode] = @LargeCode
	WHERE	[TableName] = @TableName

GO
USE [master]
GO
ALTER DATABASE [FFMS] SET  READ_WRITE 
GO
