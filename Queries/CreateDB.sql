--  CREATE DATABASE FFMS;
--  DROP DATABASE FFMS; 
-- AUTHOR 	 : [Company Name] 
-- YEAR		 : [2016 G.C.]
-- PROCEDURE : []
-- PROJECT	 : []
-- ----------------------------

USE FFMS;

--1. Vehicle
CREATE TABLE VEHICLE (
	VehicleID varchar(50) PRIMARY KEY,
	VType varchar(50) NOT NULL,
	PlateNo nvarchar(50) NOT NULL UNIQUE,
	Make varchar(50) NOT NULL, -- contains carmake info both
	MotorNo nvarchar(50) NOT NULL,
	ChassisNo nvarchar(50) NOT NULL,
	ManufacturedYear int,
	ManufacturedCountry varchar(4),
	PurchaseDate date,
	FuelType varchar(50) NOT NULL,
	Mileage float NOT NULL,
	StartGageReading int,
	FuelReserve	numeric NOT NULL,
	TankerCapacity numeric NOT NULL,
	VStatus varchar(50) NOT NULL,
	VImage image,
	RegDate datetime NOT NULL,
	RUser varchar(50) NOT NULL
)

--2. VehicleTire
CREATE TABLE VEHICLETYPE (
	TypeID varchar(50) PRIMARY KEY,
	TName nvarchar(50) NOT NULL,
	TDescription nvarchar(100)
)

--3. Driver
CREATE TABLE DRIVER (
	DriverID varchar(50) PRIMARY KEY,
	FirstName nvarchar(50) NOT NULL,
	MiddleName nvarchar(50) NOT NULL,
	LastName nvarchar(50),
	Gender nvarchar(6) NOT NULL,
	BirthDate datetime,
	Phone varchar(14),
	License varchar(50) NOT NULL,
	Vehicle varchar(50), -- Could be NULL in case the driver is not yet assigned
	DImge image,
	RegDate datetime NOT NULL,
	RUser varchar(50) NOT NULL
)

--4. Guarantor
CREATE TABLE GUARANTOR (
	GuarantorID varchar(50) PRIMARY KEY,
	FirstName nvarchar(50) NOT NULL,
	MiddleName nvarchar(50) NOT NULL,
	LastName nvarchar(50),
	Gender nvarchar(6) NOT NULL,
	BirthDate datetime,
	Phone varchar(14),
	Organization nvarchar(100),
	GImage image,
	RegDate datetime NOT NULL,
	RUser varchar(50) NOT NULL
)

--5 - DriverVsGuarantors
CREATE TABLE DRIVERGUARANTORS (
	DGID varchar(50) PRIMARY KEY,
	Driver varchar(50) NOT NULL,
	Guarantor varchar(50) NOT NULL,
	LetterReference nvarchar(20),
	RegDate datetime NOT NULL,
	RegUser varchar(50) NOT NULL
) 

--5. Fuel
CREATE TABLE FUEL (
	FuelID varchar(50) PRIMARY KEY,
	FName nvarchar(50) NOT NULL,
	CurrentPrice money NOT NULL,
	CPriceStartDate date NOT NULL,
	FDescription varchar(100),
	RegDate datetime NOT NULL,
	RUser varchar(50) NOT NULL
)

--6.Lubricant
CREATE TABLE LUBRICANT (
	LubricantID varchar(50) PRIMARY KEY,
	LName nvarchar(50) NOT NULL,
	LType nvarchar(50) NOT NULL,
	CurrentPrice money NOT NULL,
	CPriceStartDate date NOT NULL,
	LDescription varchar(100),
	RegDate datetime NOT NULL,
	RUser varchar(50) NOT NULL
)

--7. Tire
CREATE TABLE TIRE (
	TireID varchar(50) PRIMARY KEY,
	DOTidentificationNo varchar(50),
	TName nvarchar(50) NOT NULL,
	Brand nvarchar(50) NOT NULL,
	MaxLoad int,
	Width int, 
	CurrentPrice money NOT NULL,
	CPriceStartDate date NOT NULL,
	TDescription varchar(100),
	RegDate datetime NOT NULL,
	RUser varchar(50) NOT NULL
)

--8. License
CREATE TABLE LICENSE (
	LicenseID varchar(50) PRIMARY KEY,
	LName nvarchar(50) NOT NULL,
	LLevel nvarchar(50) NOT NULL,
	LDescription nvarchar(100)
)

--9. Receipt
CREATE TABLE RECEIPT (
	ReceiptID varchar(50) PRIMARY KEY,
	Driver varchar(50) NOT NULL,
	PromptDate date NOT NULL,
	RegDate datetime NOT NULL,
	RUser varchar(50) NOT NULL
) 

--10. ReceiptDetail
CREATE TABLE RECEIPTDETAIL (
	RDID varchar(50) PRIMARY KEY,
	Reciept varchar(50), -- ReceiptID
	SerialNo varchar(20) NOT NULL,
	[Date] date NOT NULL,
	Station nvarchar(50),
	Amount float NOT NULL,
	Cashier nvarchar(50),
	VATIncluse bit NOT NULL,
	Attachment image
)

--11. VehicleTire
CREATE TABLE VEHICLETIRE (
	VTID varchar(50) PRIMARY KEY,
	Vehicle varchar(50) NOT NULL,
	Tire varchar(50) NOT NULL,
	RegDate datetime NOT NULL,
	RUser varchar(50) NOT NULL
) 

--12. VehicleLubricant
CREATE TABLE VEHICLELUBRICANT (
	VLID varchar(50) PRIMARY KEY,
	Vehicle varchar(50) NOT NULL,
	Lubricant varchar(50) NOT NULL,
	RegDate datetime NOT NULL,
	RUser varchar(50) NOT NULL
)

--13. PriceHistory
CREATE TABLE PRICEHISTORY (
	PHID varchar(50) PRIMARY KEY,
	PHType varchar(50) NOT NULL,
	ItemID varchar(50) NOT NULL,
	StartDate date NOT NULL,
	EndDate date NOT NULL,
	RegDate date NOT NULL,
	RUser varchar(50) NOT NULL
)

--14. FuelIssued
CREATE TABLE FUELISSUED (
	IssueID varchar(50) PRIMARY KEY,
	Vehicle varchar(50) NOT NULL,
	Driver varchar(50) NOT NULL,
	Gauge int NOT NULL,
	Amount int NOT NULL,
	issueDate date NOT NULL,
	Destination varchar(50) NOT NULL,
	Purpose varchar(100) NOT NULL,
	Duration int NOT NULL,
	PaymentType int NOT NULL, -- 0 for check, 1- for cash, 2-for coupon 
	PaymentReference nvarchar(50),
	Quantity int,
	[Status] varchar NOT NULL,
	RegDate date NOT NULL,
	RUser varchar(50) NOT NULL
	UNIQUE (Vehicle, Driver)
)

--15. FuelConsumed
CREATE TABLE FUELCONSUMED (
	FCID varchar(50) PRIMARY KEY,
	IssuedNo varchar(50) NOT NULL,
	Gauge int NOT NULL,
	ReceiptNo varchar(50) NOT NULL,
	ReturnDate date NOT NULL,
	RegDate datetime NOT NULL,
	RUser varchar(50) NOT NULL,
)

--15. Coupon
CREATE TABLE COUPON (
	CouponID varchar(50) PRIMARY KEY,
	[Number] varchar(50) NOT NULL,
	Station nvarchar(50) NOT NULL,
	Amount float NOT NULL,
	SNRange varchar(50) NOT NULL,
	IssuedDate date,
	ExpireDate date,
	Quantity int NOT NULL
)
	
--16 CarMake
CREATE TABLE CARMAKE (
	MakeID  varchar(50),
	Make  varchar(50),
	Model  varchar(50),
	[Description] varchar(100) DEFAULT '',
	PRIMARY KEY(MakeID)
)

CREATE TABLE COUNTRY (
	CountryCode varchar(4) PRIMARY KEY,
	Country nvarchar(50) NOT NULL,
	CImage image
)

--17. Lang
CREATE TABLE CODELOG (
	TableName varchar(50) PRIMARY KEY,
	Prefix varchar(2) NOT NULL UNIQUE,	
	LargeCode numeric NOT NULL DEFAULT 0
)

--17. Lang
CREATE TABLE LANG (
	LangID varchar(50) PRIMARY KEY,
	Lan varchar(2) NOT NULL,
	CommandID varchar(50) NOT NULL,
	CommandText nvarchar(100) NOT NULL,
)

--18. Users
CREATE TABLE USERS (
	UserID varchar(50) PRIMARY KEY,
	UserName varchar(50) NOT NULL,
	Password varchar(2000) NOT NULL,
	RegDate datetime NOT NULL,
	RUser varchar(50) NOT NULL
)

--19. Log
CREATE TABLE [LOG] (
	LogNo int PRIMARY KEY,
	[UserName] varchar(50) NOT NULL,
	[Login] datetime NOT NULL,
	[Logout] datetime NOT NULL
) 

---------------------------------
---------------------------------
-- CREATE RELATION SHIPS
---------------------------------
---------------------------------
ALTER TABLE VEHICLE 		 ADD CONSTRAINT _VehicleType 			FOREIGN KEY (VType) 				REFERENCES VEHICLETYPE 	(TypeID)
ALTER TABLE VEHICLE 		 ADD CONSTRAINT _VehicleFuel 			FOREIGN KEY (FuelType) 				REFERENCES FUEL 		(FuelID)
ALTER TABLE VEHICLE 		 ADD CONSTRAINT _CarMake 				FOREIGN KEY (Make) 					REFERENCES CARMAKE 		(MakeID)
ALTER TABLE VEHICLE 		 ADD CONSTRAINT _VehicleCountry			FOREIGN KEY (ManufacturedCountry) 	REFERENCES COUNTRY 		(CountryCode)
ALTER TABLE VEHICLE 		 ADD CONSTRAINT _VRuser 				FOREIGN KEY (RUser) 				REFERENCES USERS 		(UserID)

ALTER TABLE FUEL 			 ADD CONSTRAINT _FRUser 				FOREIGN KEY (RUser) 				REFERENCES USERS 		(UserID)

ALTER TABLE DRIVERGUARANTORS ADD CONSTRAINT _DriverGuars 			FOREIGN KEY (Driver) 				REFERENCES DRIVER 		(DriverID)
ALTER TABLE DRIVERGUARANTORS ADD CONSTRAINT _GuarantorDr 			FOREIGN KEY (Guarantor) 			REFERENCES GUARANTOR	(GuarantorID)

ALTER TABLE DRIVER 			 ADD CONSTRAINT _DriverLicense 			FOREIGN KEY (License) 				REFERENCES LICENSE 		(LicenseID)
ALTER TABLE DRIVER 			 ADD CONSTRAINT _VehicleDriver 			FOREIGN KEY (Vehicle) 				REFERENCES VEHICLE 		(VehicleID)
ALTER TABLE DRIVER 			 ADD CONSTRAINT _DRUser 				FOREIGN KEY (RUser) 				REFERENCES USERS 		(UserID)

ALTER TABLE LUBRICANT 		 ADD CONSTRAINT _LRUser 				FOREIGN KEY (RUser) 				REFERENCES USERS 		(UserID)

ALTER TABLE TIRE 			 ADD CONSTRAINT _TRUser 				FOREIGN KEY (RUser) 				REFERENCES USERS 		(UserID)

ALTER TABLE FUELISSUED		 ADD CONSTRAINT _VehicleFuelConsumption FOREIGN KEY (Vehicle) 				REFERENCES VEHICLE 		(VehicleID)
ALTER TABLE FUELISSUED		 ADD CONSTRAINT _DriverFuelConsumption	FOREIGN KEY (Driver) 				REFERENCES DRIVER 		(DriverID)
ALTER TABLE FUELISSUED		 ADD CONSTRAINT _FIUser 				FOREIGN KEY (RUser) 				REFERENCES USERS 		(UserID)

ALTER TABLE FUELCONSUMED	 ADD CONSTRAINT _FuelIssedCons 			FOREIGN KEY (IssuedNo)				REFERENCES FUELISSUED	(IssueID)
ALTER TABLE FUELCONSUMED	 ADD CONSTRAINT _FuelReceipt 			FOREIGN KEY (ReceiptNo)				REFERENCES RECEIPT 		(ReceiptID)
ALTER TABLE FUELCONSUMED	 ADD CONSTRAINT _FCRUser	 			FOREIGN KEY (RUser)					REFERENCES USERS 		(UserID)

ALTER TABLE [LOG] 			 ADD CONSTRAINT _LoggedUser 			FOREIGN KEY (UserName) 				REFERENCES USERS 		(UserID)

ALTER TABLE PRICEHISTORY 	 ADD CONSTRAINT _PHRUser 				FOREIGN KEY (RUser) 				REFERENCES USERS 		(UserID)

ALTER TABLE RECEIPT 		 ADD CONSTRAINT _DriversReceipt 		FOREIGN KEY (Driver) 				REFERENCES DRIVER 		(DriverID)
ALTER TABLE RECEIPT 		 ADD CONSTRAINT _ReceiptRUser 			FOREIGN KEY (RUser) 				REFERENCES USERS 		(UserID)

ALTER TABLE VEHICLELUBRICANT ADD CONSTRAINT _VehicleLub 			FOREIGN KEY (Vehicle) 				REFERENCES VEHICLE 		(VehicleID)
ALTER TABLE VEHICLELUBRICANT ADD CONSTRAINT _Lubrication 			FOREIGN KEY (Lubricant)				REFERENCES LUBRICANT 	(LubricantID)
ALTER TABLE VEHICLELUBRICANT ADD CONSTRAINT _VLRUser 				FOREIGN KEY (RUser) 				REFERENCES USERS 		(UserID)

ALTER TABLE VEHICLETIRE 	 ADD CONSTRAINT _VehicleTr1 			FOREIGN KEY (Vehicle) 				REFERENCES VEHICLE 		(VehicleID)
ALTER TABLE VEHICLETIRE 	 ADD CONSTRAINT _VehicleTr2 			FOREIGN KEY (Tire) 					REFERENCES TIRE 		(TireID)
ALTER TABLE VEHICLETIRE 	 ADD CONSTRAINT _VTRUser 				FOREIGN KEY (RUser) 				REFERENCES USERS 		(UserID) 

---------------------------------
---------------------------------
-- POPULATE TABLES
---------------------------------
---------------------------------
-- VEHICLETYPE

