-- AUTHOR 	: [Company Name] 
-- YEAR		: [2008 e.c.]
-- PROCUDER : []
-- PROJECT	: []
-- ----------------------------

USE FFMS;
GO

CREATE PROCEDURE updateCodeLog
	@LargeCode numeric,
	@TableName varchar(50)
AS 
	UPDATE	[CODELOG]
	SET		[LargeCode] = @LargeCode
	WHERE	[TableName] = @TableName
GO