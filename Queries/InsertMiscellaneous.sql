USE FFMS
GO

INSERT INTO VEHICLETYPE (TypeID, TName, TDescription) 
VALUES
	('VT-01', 'PickUp', ''),
	('VT-02', 'Bus - Small', ''),
	('VT-03', 'Bus - Medium', ''),
	('VT-04', 'Bus - Large', ''),
	('VT-05', 'Min-Van', ''),
	('VT-06', 'Van', ''),
	('VT-07', 'MotorCycle', ''),
	('VT-08', 'PickUp-Truck', ''),
	('VT-09', 'Truck', '')
GO

-- USERS
INSERT INTO USERS		(UserID, UserName, [Password], RegDate, RUser) 
VALUES
	('UR-01', 'Admin', '$2a$12$RE.wxcVFhuWqADQ7OjNYjO8QD1QYNFyC3Kn4gnvg81K.2h7UIqmXy', '2/3/2005', '')
GO

-- FUEL
INSERT INTO FUEL		(FuelID, FName, CurrentPrice, CPriceStartDate, FDescription, RegDate, RUser) 
VALUES
	('FL-01', 'Benzene', 0, '2/3/2005', '', '2/3/2005', 'UR-01'),
	('FL-02', 'Diesel', 0, '2/3/2005', '', '2/3/2005', 'UR-01'),
	('FL-03', 'Ethanol', 0, '2/3/2005', '', '2/3/2005', 'UR-01'),
	('FL-04', 'Gasoline', 0, '2/3/2005', '', '2/3/2005', 'UR-01'),
	('FL-05', 'Kerosene', 0, '2/3/2005', '', '2/3/2005', 'UR-01'),
	('FL-06', 'Naphta', 0, '2/3/2005', '', '2/3/2005', 'UR-01')
GO

-- LICENSE
INSERT INTO LICENSE		(LicenseID, LName, LLevel, LDescription) 
VALUES
	('LC-01', 'Level - 1', 'One', 'Level one driving license'),
	('LC-02', 'Level - 2', 'Two', 'Level two driving license')
GO


INSERT INTO CODELOG		(TableName, Prefix, LargeCode) 
VALUES
	('CARMAKE', 'MK', 191),
	('COUPON', 'CP', 0),
	('DRIVER', 'DR', 0),
	('DRIVERGUARANTORS', 'DG', 0),
	('FUEL', 'FL', 6),
	('FUELCONSUMPTION',	'FC', 0),
	('GUARANTOR', 'GR',	0),
	('LICENSE',	'LC', 2),
	('LUBRICANT', 'LB', 0),
	('PRICEHISTORY', 'PH', 0),
	('RECEIPT',	'RC', 0),
	('RECEIPTDETAIL', 'RD', 0),
	('TIRE', 'TI', 0),
	('VEHICLE',	'VH', 0),
	('VEHICLELUBRICANT', 'VL', 0),
	('VEHICLETIRE',	'VI', 0),
	('VEHICLETYPE',	'VT', 0),
	('USERS', 'UR', 1)
GO