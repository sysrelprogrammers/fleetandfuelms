USE FFMS;
GO

CREATE PROCEDURE getVehicleLubricant
	@VID as varchar(50)
AS
	SELECT	DISTINCT LubricantID, LName, LType, CurrentPrice, CPriceStartDate, LDescription
	FROM	[VEHICLELUBRICANT] LEFT JOIN [LUBRICANT] 
			ON [VEHICLELUBRICANT].Lubricant = [LUBRICANT].LubricantID
	WHERE	Vehicle LIKE @VID

GO
