USE FFMS;
GO

CREATE PROCEDURE getDriverInfo

AS
	-- With LICENSE
	SELECT	ROW_NUMBER() OVER (ORDER BY DriverID) AS [SNo],
			FirstName as [First Name], 
			MiddleName as [Middle Name],
			LastName as [Last Name],
			Gender, BirthDate as [Birth Date],
			Phone, [LICENSE].[LName] AS License,
			Vehicle
	INTO	#TempDriverLicense
	FROM	[DRIVER], [LICENSE]
	WHERE	[DRIVER].License = [LICENSE].[LicenseID]

	-- With VEHICLE
	SELECT  SNo, [First Name], [Middle Name], [Last Name],
			Gender, [Birth Date], Phone, License, 
			([VEHICLE].VehicleID), [VEHICLE].Make, [VEHICLE].PlateNo
	INTO	#TempDriverVehicle
	FROM	#TempDriverLicense, [VEHICLE]
	WHERE	#TempDriverLicense.Vehicle = [VEHICLE].VehicleID

	-- With CARMAKE
	SELECT	SNo, [First Name], [Middle Name], [Last Name],
			Gender, [Birth Date], Phone, License, 
			([CARMAKE].Make + ' ' + [CARMAKE].Model + ' ' + PlateNo) as Vehicle 
	INTO	#TempFinal
	FROM	#TempDriverVehicle, [CARMAKE]
	WHERE	#TempDriverVehicle.Make = [CARMAKE].MakeID

	SELECT	*
	FROM	#TempFinal

	DROP	TABLE #TempDriverLicense
	DROP	TABLE #TempDriverVehicle
	DROP	TABLE #TempFinal

GO 