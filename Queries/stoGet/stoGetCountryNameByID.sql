USE FFMS;
GO

CREATE PROCEDURE getCountryNameByID
	@CountryName varchar(50)
AS
	SELECT	TOP 1 CountryCode
	FROM	[COUNTRY]
	WHERE	Country = @CountryName
