-- AUTHOR 	: [Company Name] 
-- YEAR		: [2008 e.c.]
-- PROCUDER : []
-- PROJECT	: []
-- ----------------------------

USE FFMS;
GO

CREATE PROCEDURE getCountries
AS 
	SELECT * FROM [COUNTRY]
	ORDER BY [COUNTRY] ASC
GO