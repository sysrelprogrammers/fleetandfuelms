-- AUTHOR 	: [Company Name] 
-- YEAR		: [2008 e.c.]
-- PROCUDER : []
-- PROJECT	: []
-- ----------------------------

USE FFMS;
GO

CREATE PROCEDURE getLicenseInfo
	@LID as VARCHAR(50)
AS 
	SELECT	LicenseID, LName, LLevel, LDescription FROM [LICENSE]
	WHERE	LicenseID LIKE @LID
GO