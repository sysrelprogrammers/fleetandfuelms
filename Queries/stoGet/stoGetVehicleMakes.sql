-- AUTHOR 	: [Company Name] 
-- YEAR		: [2008 e.c.]
-- PROCUDER : []
-- PROJECT	: []
-- ----------------------------

USE FFMS;
GO

CREATE PROCEDURE getVehicleModels
	@Make varchar(50)
AS 
	SELECT DISTINCT Model FROM [CARMAKE] WHERE [Make] = @Make
GO