USE FFMS;
GO

CREATE PROCEDURE getVehicleInfo
	@VID as VARCHAR(50)
AS
	-- With CarMake 
	SELECT	ROW_NUMBER() OVER (ORDER BY VehicleID) AS [SNo],
			VehicleID, PlateNo, MotorNo, ChassisNo, VType, FuelType, Mileage, 
			[CARMAKE].Make, [CARMAKE].Model, ManufacturedCountry, ManufacturedYear, 
			PurchaseDate, StartGageReading, FuelReserve, TankerCapacity, VStatus, VImage
	INTO	#TempVehicleCarMake
	FROM	[VEHICLE], [CARMAKE]
	WHERE	[VEHICLE].[Make] = [CARMAKE].[MakeID]

	-- With Fuel
	SELECT  SNo, VehicleID, PlateNo, MotorNo, ChassisNo, VType, FUEL.FName, 
			Mileage, Make, Model, ManufacturedCountry, ManufacturedYear, 
			PurchaseDate, StartGageReading, FuelReserve, TankerCapacity, VStatus, VImage
	INTO	#TempVehicleFuel
	FROM	#TempVehicleCarMake, FUEL
	WHERE	#TempVehicleCarMake.FuelType = FUEL.FuelID

	-- With Type
	SELECT	SNo, VehicleID, PlateNo, MotorNo, ChassisNo, [VEHICLETYPE].TName AS [VType], 
			FName, Mileage, Make, Model, ManufacturedCountry, ManufacturedYear, 
			PurchaseDate, StartGageReading, FuelReserve, TankerCapacity, VStatus, VImage
	INTO	#TempVehicleCountry
	FROM	#TempVehicleFuel, VEHICLETYPE
	WHERE	#TempVehicleFuel.VType = VEHICLETYPE.TypeID

	SELECT	SNo, VehicleID, PlateNo, MotorNo, ChassisNo, VType, 
			FName, Mileage, Make, Model, [COUNTRY].Country, 
			ManufacturedYear, PurchaseDate, StartGageReading, 
			FuelReserve, TankerCapacity, VStatus, VImage
	INTO	#TempVehicleInfo
	FROM	#TempVehicleCountry, [COUNTRY]
	WHERE	#TempVehicleCountry.ManufacturedCountry = COUNTRY.CountryCode
	
	SELECT	*
	FROM	#TempVehicleInfo
	WHERE	VehicleID LIKE @VID

	DROP	TABLE #TempVehicleInfo
	DROP	TABLE #TempVehicleCountry 
	DROP	TABLE #TempVehicleFuel
	DROP	TABLE #TempVehicleCarMake

GO 