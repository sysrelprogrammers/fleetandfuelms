-- AUTHOR 	: [Company Name] 
-- YEAR		: [2008 e.c.]
-- PROCUDER : []
-- PROJECT	: []
-- ----------------------------

USE FFMS;
GO

CREATE PROCEDURE getTireInfo
	@TID as VARCHAR(50)
AS 
	SELECT	* FROM [TIRE]
	WHERE	TireID LIKE @TID
GO