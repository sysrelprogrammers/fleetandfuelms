USE FFMS;
GO

CREATE PROCEDURE getVehicleMakeID
	@Make varchar(50),
	@Model varchar(50)
AS
	SELECT	TOP 1 MakeID
	FROM	[CARMAKE]
	WHERE	[Make] = @Make AND [Model] = @Model
