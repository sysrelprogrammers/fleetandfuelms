USE FFMS;
GO

CREATE PROCEDURE getUserByUserName
	@UName varchar(50)
AS
	SELECT	*
	FROM	[USERS]
	WHERE	UserName = @UName
