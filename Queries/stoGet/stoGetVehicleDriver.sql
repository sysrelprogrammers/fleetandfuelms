USE FFMS;
GO

CREATE PROCEDURE getVehicleDriver
	@VID varchar(50)
AS
	SELECT	[DriverID]
      ,[FirstName]
      ,[MiddleName]
      ,[LastName]
      ,[Gender]
      ,[BirthDate]
      ,[Phone]
      ,[License]
      ,[Vehicle]
      ,[DImage]
      ,[RegDate]
      ,[RUser]
	FROM	[DRIVER]
	WHERE	Vehicle LIKE @VID
GO