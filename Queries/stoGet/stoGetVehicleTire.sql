USE FFMS;
GO

CREATE PROCEDURE getVehicleTire
	@VID varchar(50)
AS
	SELECT	DISTINCT TireID, DOTidentificationNo, TName, Brand, CurrentPrice, MaxLoad, Width, CPriceStartDate, TDescription
	FROM	[TIRE] LEFT JOIN [VEHICLETIRE]
			ON [TIRE].TireID = [VEHICLETIRE].Tire
	WHERE	Vehicle LIKE @VID
GO