-- AUTHOR 	: [Company Name] 
-- YEAR		: [2008 e.c.]
-- PROCUDER : []
-- PROJECT	: []
-- ----------------------------

USE FFMS;
GO

CREATE PROCEDURE registerUser
	@UserID varchar(50),
	@UserName varchar(50),
	@Password varchar(2000),
	@RegDate datetime,
	@RUser varchar(50)
AS 
	SET NOCOUNT ON;
	INSERT INTO USERS (
		UserID,
		UserName,
		Password,
		RegDate,
		RUser
	)
	VALUES (
		@UserID,
		@UserName,
		@Password,
		@RegDate,
		@RUser
	)
GO