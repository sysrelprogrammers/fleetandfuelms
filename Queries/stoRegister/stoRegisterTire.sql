-- AUTHOR 	: [Company Name] 
-- YEAR		: [2008 e.c.]
-- PROCUDER : []
-- PROJECT	: []
-- ----------------------------

USE FFMS;
GO

CREATE PROCEDURE registerTire
	@TireID varchar(50),
	@DOTidentificationNo varchar(50),
	@TName nvarchar(50),
	@Brand nvarchar(50),
	@MaxLoad int,
	@Width int, 
	@CurrentPrice money,
	@CPriceStartDate date,
	@TDescription varchar(100),
	@RegDate datetime,
	@RUser varchar(50)
AS 
	SET NOCOUNT ON;
	INSERT INTO [TIRE] (
		TireID,
		DOTidentificationNo,
		TName,
		Brand,
		MaxLoad,
		Width,
		CurrentPrice,
		CPriceStartDate,
		TDescription,
		RegDate,
		RUser
	)
	VALUES (
		@TireID,
		@DOTidentificationNo,
		@TName,
		@Brand,
		@MaxLoad,
		@Width,
		@CurrentPrice,
		@CPriceStartDate,
		@TDescription,
		@RegDate,
		@RUser
	)
GO