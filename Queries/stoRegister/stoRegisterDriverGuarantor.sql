-- AUTHOR 	: [Company Name] 
-- YEAR		: [2008 e.c.]
-- PROCUDER : []
-- PROJECT	: []
-- ----------------------------

USE FFMS;
GO

CREATE PROCEDURE registerDriverGuarantor
	@DGID varchar(50),
	@Driver varchar(50),
	@Guarantor varchar(50),
	@Organization nvarchar(50),
	@LetterReference nvarchar(20),
	@RegDate datetime,
	@RUser varchar(50)
AS 
	SET NOCOUNT ON;
	INSERT INTO DRIVERGUARANTORS(
		DGID,
		Driver,
		Guarantor,
		Organization,
		LetterReference,
		RegDate,
		RUser
	)
	VALUES (
		@DGID,
		@Driver,
		@Guarantor,
		@Organization,
		@LetterReference,
		@RegDate,
		@RUser
	)
GO