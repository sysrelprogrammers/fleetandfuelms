-- AUTHOR 	: [Company Name] 
-- YEAR		: [2008 e.c.]
-- PROCUDER : []
-- PROJECT	: []
-- ----------------------------

USE FFMS;
GO

CREATE PROCEDURE registerCoupon
	@CouponID varchar(50),
	@Number varchar(50),
	@Station nvarchar(50),
	@Amount float,
	@SNRange varchar(50),
	@IssuedDate date,
	@ExpireDate date,
	@Quantity int  
AS 
	SET NOCOUNT ON;
	INSERT INTO COUPON (
		CouponID,
		[Number],
		Station,
		Amount,
		SNRange,
		IssuedDate,
		ExpireDate,
		Quantity
	)
	VALUES (
		@CouponID,
		@Number,
		@Station,
		@Amount,
		@SNRange,
		@IssuedDate,
		@ExpireDate,
		@Quantity
	)
GO