USE FFMS;
GO

ALTER PROCEDURE registerVehicleLubricant
	@VLID varchar(50),
	@Vehicle varchar(50),
	@Lubricant varchar(50),
	@RUser varchar(50)
AS
	DECLARE @TempDate as datetime
	SET @TempDate = (SELECT GETDATE())
	INSERT INTO [VEHICLELUBRICANT] (
		VLID,
		Vehicle,
		Lubricant,
		RegDate,
		RUser
	)
	VALUES (
		@VLID,
		@Vehicle,
		@Lubricant,
		@TempDate,
		@RUser
	)
GO