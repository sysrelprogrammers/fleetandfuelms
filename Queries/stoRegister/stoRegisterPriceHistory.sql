-- AUTHOR 	: [Company Name] 
-- YEAR		: [2008 e.c.]
-- PROCUDER : []
-- PROJECT	: []
-- ----------------------------

USE FFMS;
GO

CREATE PROCEDURE registerPriceHistory
	@PHID varchar(50),
	@PHType varchar(50),
	@ItemID varchar(50),
	@StartDate date,
	@EndDate date,
	@RegDate date,
	@RUser varchar(50)
AS 
	SET NOCOUNT ON;
	INSERT INTO PRICEHISTORY (
		PHID,
		PHType,
		ItemID,
		StartDate,
		EndDate,
		RegDate,
		RUser
	)
	VALUES (
		@PHID,
		@PHType,
		@ItemID,
		@StartDate,
		@EndDate,
		@RegDate,
		@RUser
	)
GO