-- AUTHOR 	: [Company Name] 
-- YEAR		: [2008 e.c.]
-- PROCUDER : []
-- PROJECT	: []
-- ----------------------------

USE FFMS;
GO

CREATE PROCEDURE registerVehicleType
	@TypeID varchar(50), 
	@TName nvarchar(50), 
	@TDescription nvarchar(100)
AS 
	SET NOCOUNT ON;
	INSERT INTO VEHICLETYPE (
		TypeID, 
		TName, 
		TDescription
	)
	VALUES (
		@TypeID, 
		@TName, 
		@TDescription
	)
GO