-- AUTHOR 	: [Company Name] 
-- YEAR		: [2008 e.c.]
-- PROCUDER : []
-- PROJECT	: []
-- ----------------------------

USE FFMS;
GO

CREATE PROCEDURE registerReceiptDetail
	@RDID varchar(50) ,
	@Reciept varchar(50), 
	@SerialNo varchar(20),
	@Date date,
	@Station nvarchar(50),
	@Amount float,
	@Cashier nvarchar(50),
	@VATInclusive bit,
	@Attachment image
AS 
	SET NOCOUNT ON;
	INSERT INTO RECEIPTDETAIL (
		RDID,
		Reciept, 
		SerialNo,
		[Date],
		Station,
		Amount,
		Cashier,
		VATInclusive,
		Attachment
	)
	VALUES (
		@RDID,
		@Reciept, 
		@SerialNo,
		@Date,
		@Station,
		@Amount,
		@Cashier,
		@VATInclusive,
		@Attachment
	)
GO