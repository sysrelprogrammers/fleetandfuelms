-- AUTHOR 	: [Company Name] 
-- YEAR		: [2008 e.c.]
-- PROCUDER : []
-- PROJECT	: []
-- ----------------------------

USE FFMS;
GO

CREATE PROCEDURE registerReceipt
	@ReceiptID varchar(50),
	@Driver varchar(50),
	@PromptDate date,
	@RegDate datetime,
	@RUser varchar(50)
AS 
	SET NOCOUNT ON;
	INSERT INTO RECEIPT (
		ReceiptID,
		Driver,
		PromptDate,
		RegDate,
		RUser
	)
	VALUES (
		@ReceiptID,
		@Driver,
		@PromptDate,
		@RegDate,
		@RUser
	)
GO