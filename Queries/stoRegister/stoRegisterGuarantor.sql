-- AUTHOR 	: [Company Name] 
-- YEAR		: [2008 e.c.]
-- PROCUDER : []
-- PROJECT	: []
-- ----------------------------

USE FFMS;
GO

CREATE PROCEDURE registerGuarantor
	@GuarantorID varchar(50),
	@FirstName nvarchar(50),
	@MiddleName nvarchar(50),
	@LastName nvarchar(50),
	@Gender nvarchar(6),
	@BirthDate datetime,
	@Phone varchar(14),
	@GImage image,
	@RegDate datetime,
	@RUser varchar(50)
AS 
	SET NOCOUNT ON;
	INSERT INTO GUARANTOR (
		GuarantorID,
		FirstName ,
		MiddleName,
		LastName,
		Gender,
		BirthDate,
		Phone,
		GImage,
		RegDate,
		RUser
	)
	VALUES (
		@GuarantorID,
		@FirstName ,
		@MiddleName,
		@LastName,
		@Gender,
		@BirthDate,
		@Phone,
		@GImage,
		@RegDate,
		@RUser
	)
GO