USE FFMS;
GO
CREATE PROCEDURE registerVehicle
    @VehicleID varchar(50), 
    @VType varchar(50),
	@PlateNo nvarchar(50),
	@Make nvarchar(50),
	@MotorNo nvarchar(50),
	@ChassisNo nvarchar(50),
	@ManufacturedYear int,
	@ManufacturedCountry nvarchar(50),
	@PurchaseDate date,
	@FuelType varchar(50),
	@Mileage float,
	@StartGageReading int,
	@VStatus varchar(50),
	@VImage image,
	@RUser varchar(50)
AS 
	DECLARE @TempDate as datetime
	SET @TempDate = (SELECT GETDATE())
	INSERT INTO VEHICLE (
		VehicleID, VType, PlateNo, Make, MotorNo, ChassisNo, 
		ManufacturedYear, ManufacturedCountry, PurchaseDate, FuelType, 
		Mileage, StartGageReading, VStatus, VImage, RegDate, RUser
	)
	VALUES (
		@VehicleID, @VType, @PlateNo, @Make, @MotorNo, @ChassisNo, 
		@ManufacturedYear, @ManufacturedCountry, @PurchaseDate, @FuelType,
		@Mileage, @StartGageReading, @VStatus, @VImage, @TempDate, @RUser
	)
GO
