-- AUTHOR 	: [Company Name] 
-- YEAR		: [2008 e.c.]
-- PROCUDER : []
-- PROJECT	: []
-- ----------------------------

USE FFMS;
GO

CREATE PROCEDURE registerLog
	@LogNo numeric,
	@UserName varchar(50),
	@Login datetime,
	@Logout datetime
AS 
	SET NOCOUNT ON;
	INSERT INTO [LOG] (
		LogNo,
		UserName,
		Login,
		Logout
	)
	VALUES (
		@LogNo,
		@UserName,
		@Login,
		@Logout
	)
GO