-- AUTHOR 	: [Company Name] 
-- YEAR		: [2008 e.c.]
-- PROCUDER : []
-- PROJECT	: []
-- ----------------------------

USE FFMS;
GO

CREATE PROCEDURE registerLubricant
	@LubricantID varchar(50),
	@LName nvarchar(50),
	@LType nvarchar(50),
	@CurrentPrice money,
	@CPriceStartDate date,
	@LDescription varchar(100),
	@RegDate datetime,
	@RUser varchar(50)
AS 
	SET NOCOUNT ON;
	INSERT INTO LUBRICANT (
		LubricantID,
		LName,
		LType,
		CurrentPrice,
		CPriceStartDate,
		LDescription,
		RegDate,
		RUser
	)
	VALUES (
		@LubricantID,
		@LName,
		@LType,
		@CurrentPrice,
		@CPriceStartDate,
		@LDescription,
		@RegDate,
		@RUser
	)
GO