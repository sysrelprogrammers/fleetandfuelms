-- AUTHOR 	: [Company Name] 
-- YEAR		: [2008 e.c.]
-- PROCUDER : []
-- PROJECT	: []
-- ----------------------------

USE FFMS;
GO

CREATE PROCEDURE registerDriver
	@DriverID varchar(50),
	@FirstName nvarchar(50),
	@MiddleName nvarchar(50),
	@LastName nvarchar(50),
	@Gender nvarchar(6),
	@BirthDate datetime,
	@Phone varchar(14),
	@License varchar(50),
	@Vehicle varchar(50),
	@DImage image,
	@RegDate datetime,
	@RUser varchar(50)
AS 
	SET NOCOUNT ON;
	INSERT INTO DRIVER (
		DriverID,
		FirstName,
		MiddleName,
		LastName,
		Gender,
		BirthDate,
		Phone,
		License,
		Vehicle,
		DImage,
		RegDate,
		RUser
	)
	VALUES (
		@DriverID,
		@FirstName,
		@MiddleName,
		@LastName,
		@Gender,
		@BirthDate,
		@Phone,
		@License, 
		@Vehicle,
		@DImage,
		@RegDate,
		@RUser
	)
GO