-- AUTHOR 	: [Company Name] 
-- YEAR		: [2008 e.c.]
-- PROCUDER : []
-- PROJECT	: []
-- ----------------------------

USE FFMS;
GO

CREATE PROCEDURE registerLicense
	@LicenseID varchar(50),
	@LName nvarchar(50),
	@LLevel nvarchar(50),
	@LDescription nvarchar(100)
AS 
	SET NOCOUNT ON;
	INSERT INTO LICENSE (
		LicenseID,
		LName,
		LLevel,
		LDescription
	)
	VALUES (
		@LicenseID,
		@LName,
		@LLevel,
		@LDescription
	)
GO