-- AUTHOR 	: [Company Name] 
-- YEAR		: [2008 e.c.]
-- PROCUDER : []
-- PROJECT	: []
-- ----------------------------

USE FFMS;
GO

CREATE PROCEDURE registerVehicleTire
	@VTID varchar(50),
	@Vehicle varchar(50),
	@Tire varchar(50),
	@RUser varchar(50)
AS 
	DECLARE @TempDate as datetime
	SET @TempDate = (SELECT GETDATE())
	INSERT INTO VEHICLETIRE (
		VTID,
		Vehicle,
		Tire,
		RegDate,
		RUser
	)
	VALUES (
		@VTID,
		@Vehicle,
		@Tire,
		@TempDate,
		@RUser
	)
GO