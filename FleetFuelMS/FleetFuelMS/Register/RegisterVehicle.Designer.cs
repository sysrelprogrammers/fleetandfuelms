﻿namespace FleetFuelMS
{
    partial class RegisterVehicle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegisterVehicle));
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.ofdBrowseImage = new System.Windows.Forms.OpenFileDialog();
            this.ddbExport = new DevExpress.XtraEditors.DropDownButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.icbManYear = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.txtPlateNumber = new DevExpress.XtraEditors.TextEdit();
            this.icbPlateCode = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.icbPlateRegion = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.icbType = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.Type = new DevExpress.XtraEditors.LabelControl();
            this.txtGageReading = new DevExpress.XtraEditors.TextEdit();
            this.dtePurchasedDate = new DevExpress.XtraEditors.DateEdit();
            this.sbnBrowseImage = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.sbnClear = new DevExpress.XtraEditors.SimpleButton();
            this.sbnRegister = new DevExpress.XtraEditors.SimpleButton();
            this.imsVehicleImage = new DevExpress.XtraEditors.Controls.ImageSlider();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.icbStatus = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.icbFuelType = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtMileage = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtChassisNumber = new DevExpress.XtraEditors.TextEdit();
            this.icbCountry = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.icbModel = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.icbMake = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtMotorNumber = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtVehicleId = new DevExpress.XtraEditors.TextEdit();
            this.gpcVehicleList = new DevExpress.XtraEditors.GroupControl();
            this.gcVehicleList = new DevExpress.XtraGrid.GridControl();
            this.gvVehicleList = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icbManYear.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlateNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbPlateCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbPlateRegion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGageReading.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtePurchasedDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtePurchasedDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imsVehicleImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbFuelType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMileage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChassisNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbCountry.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbModel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbMake.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMotorNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVehicleId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpcVehicleList)).BeginInit();
            this.gpcVehicleList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcVehicleList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVehicleList)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 1;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Size = new System.Drawing.Size(795, 49);
            // 
            // ddbExport
            // 
            this.ddbExport.Location = new System.Drawing.Point(21, 338);
            this.ddbExport.MenuManager = this.ribbonControl1;
            this.ddbExport.Name = "ddbExport";
            this.ddbExport.Size = new System.Drawing.Size(104, 19);
            this.ddbExport.TabIndex = 0;
            this.ddbExport.Text = "Export PDF";
            this.ddbExport.Click += new System.EventHandler(this.sbnExportToPDF_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.icbManYear);
            this.groupControl1.Controls.Add(this.txtPlateNumber);
            this.groupControl1.Controls.Add(this.icbPlateCode);
            this.groupControl1.Controls.Add(this.icbPlateRegion);
            this.groupControl1.Controls.Add(this.icbType);
            this.groupControl1.Controls.Add(this.Type);
            this.groupControl1.Controls.Add(this.txtGageReading);
            this.groupControl1.Controls.Add(this.dtePurchasedDate);
            this.groupControl1.Controls.Add(this.sbnBrowseImage);
            this.groupControl1.Controls.Add(this.labelControl14);
            this.groupControl1.Controls.Add(this.sbnClear);
            this.groupControl1.Controls.Add(this.sbnRegister);
            this.groupControl1.Controls.Add(this.imsVehicleImage);
            this.groupControl1.Controls.Add(this.labelControl12);
            this.groupControl1.Controls.Add(this.labelControl13);
            this.groupControl1.Controls.Add(this.labelControl9);
            this.groupControl1.Controls.Add(this.icbStatus);
            this.groupControl1.Controls.Add(this.icbFuelType);
            this.groupControl1.Controls.Add(this.labelControl10);
            this.groupControl1.Controls.Add(this.txtMileage);
            this.groupControl1.Controls.Add(this.labelControl11);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Controls.Add(this.txtChassisNumber);
            this.groupControl1.Controls.Add(this.icbCountry);
            this.groupControl1.Controls.Add(this.icbModel);
            this.groupControl1.Controls.Add(this.icbMake);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.txtMotorNumber);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.txtVehicleId);
            this.groupControl1.Location = new System.Drawing.Point(21, 55);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(750, 273);
            this.groupControl1.TabIndex = 45;
            this.groupControl1.Text = "Vehicle Information";
            // 
            // icbManYear
            // 
            this.icbManYear.Location = new System.Drawing.Point(113, 218);
            this.icbManYear.MenuManager = this.ribbonControl1;
            this.icbManYear.Name = "icbManYear";
            this.icbManYear.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbManYear.Size = new System.Drawing.Size(176, 20);
            this.icbManYear.TabIndex = 90;
            // 
            // txtPlateNumber
            // 
            this.txtPlateNumber.Location = new System.Drawing.Point(203, 80);
            this.txtPlateNumber.MenuManager = this.ribbonControl1;
            this.txtPlateNumber.Name = "txtPlateNumber";
            this.txtPlateNumber.Size = new System.Drawing.Size(86, 20);
            this.txtPlateNumber.TabIndex = 78;
            // 
            // icbPlateCode
            // 
            this.icbPlateCode.Location = new System.Drawing.Point(157, 80);
            this.icbPlateCode.MenuManager = this.ribbonControl1;
            this.icbPlateCode.Name = "icbPlateCode";
            this.icbPlateCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbPlateCode.Size = new System.Drawing.Size(44, 20);
            this.icbPlateCode.TabIndex = 77;
            // 
            // icbPlateRegion
            // 
            this.icbPlateRegion.Location = new System.Drawing.Point(113, 80);
            this.icbPlateRegion.MenuManager = this.ribbonControl1;
            this.icbPlateRegion.Name = "icbPlateRegion";
            this.icbPlateRegion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbPlateRegion.Size = new System.Drawing.Size(41, 20);
            this.icbPlateRegion.TabIndex = 76;
            // 
            // icbType
            // 
            this.icbType.Location = new System.Drawing.Point(113, 57);
            this.icbType.MenuManager = this.ribbonControl1;
            this.icbType.Name = "icbType";
            this.icbType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbType.Size = new System.Drawing.Size(176, 20);
            this.icbType.TabIndex = 74;
            // 
            // Type
            // 
            this.Type.Location = new System.Drawing.Point(16, 60);
            this.Type.Name = "Type";
            this.Type.Size = new System.Drawing.Size(24, 13);
            this.Type.TabIndex = 73;
            this.Type.Text = "Type";
            // 
            // txtGageReading
            // 
            this.txtGageReading.Location = new System.Drawing.Point(431, 103);
            this.txtGageReading.MenuManager = this.ribbonControl1;
            this.txtGageReading.Name = "txtGageReading";
            this.txtGageReading.Size = new System.Drawing.Size(176, 20);
            this.txtGageReading.TabIndex = 100;
            // 
            // dtePurchasedDate
            // 
            this.dtePurchasedDate.EditValue = null;
            this.dtePurchasedDate.Location = new System.Drawing.Point(113, 241);
            this.dtePurchasedDate.MenuManager = this.ribbonControl1;
            this.dtePurchasedDate.Name = "dtePurchasedDate";
            this.dtePurchasedDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtePurchasedDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtePurchasedDate.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.Classic;
            this.dtePurchasedDate.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.dtePurchasedDate.Size = new System.Drawing.Size(176, 20);
            this.dtePurchasedDate.TabIndex = 92;
            // 
            // sbnBrowseImage
            // 
            this.sbnBrowseImage.Location = new System.Drawing.Point(332, 179);
            this.sbnBrowseImage.Name = "sbnBrowseImage";
            this.sbnBrowseImage.Size = new System.Drawing.Size(93, 21);
            this.sbnBrowseImage.TabIndex = 102;
            this.sbnBrowseImage.Text = "Image ...";
            this.sbnBrowseImage.Click += new System.EventHandler(this.sbnBrowseImage_Click);
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(332, 160);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(33, 13);
            this.labelControl14.TabIndex = 101;
            this.labelControl14.Text = "Picture";
            // 
            // sbnClear
            // 
            this.sbnClear.Image = ((System.Drawing.Image)(resources.GetObject("sbnClear.Image")));
            this.sbnClear.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.sbnClear.Location = new System.Drawing.Point(655, 103);
            this.sbnClear.Name = "sbnClear";
            this.sbnClear.Size = new System.Drawing.Size(70, 62);
            this.sbnClear.TabIndex = 105;
            this.sbnClear.Text = "Clear";
            // 
            // sbnRegister
            // 
            this.sbnRegister.Image = ((System.Drawing.Image)(resources.GetObject("sbnRegister.Image")));
            this.sbnRegister.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.sbnRegister.Location = new System.Drawing.Point(655, 34);
            this.sbnRegister.Name = "sbnRegister";
            this.sbnRegister.Size = new System.Drawing.Size(70, 62);
            this.sbnRegister.TabIndex = 104;
            this.sbnRegister.Text = "Register";
            this.sbnRegister.Click += new System.EventHandler(this.sbnRegister_Click);
            // 
            // imsVehicleImage
            // 
            this.imsVehicleImage.CurrentImageIndex = -1;
            this.imsVehicleImage.LayoutMode = DevExpress.Utils.Drawing.ImageLayoutMode.Stretch;
            this.imsVehicleImage.Location = new System.Drawing.Point(431, 129);
            this.imsVehicleImage.Name = "imsVehicleImage";
            this.imsVehicleImage.Size = new System.Drawing.Size(176, 136);
            this.imsVehicleImage.TabIndex = 103;
            this.imsVehicleImage.Text = "imageSlider1";
            this.imsVehicleImage.UseDisabledStatePainter = true;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(333, 60);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(36, 13);
            this.labelControl12.TabIndex = 95;
            this.labelControl12.Text = "Mileage";
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(333, 38);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(47, 13);
            this.labelControl13.TabIndex = 93;
            this.labelControl13.Text = "Fuel Type";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(15, 244);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(76, 13);
            this.labelControl9.TabIndex = 91;
            this.labelControl9.Text = "Purchased Date";
            // 
            // icbStatus
            // 
            this.icbStatus.Location = new System.Drawing.Point(431, 80);
            this.icbStatus.MenuManager = this.ribbonControl1;
            this.icbStatus.Name = "icbStatus";
            this.icbStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbStatus.Size = new System.Drawing.Size(176, 20);
            this.icbStatus.TabIndex = 98;
            // 
            // icbFuelType
            // 
            this.icbFuelType.Location = new System.Drawing.Point(431, 34);
            this.icbFuelType.MenuManager = this.ribbonControl1;
            this.icbFuelType.Name = "icbFuelType";
            this.icbFuelType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbFuelType.Size = new System.Drawing.Size(176, 20);
            this.icbFuelType.TabIndex = 94;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(334, 106);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(67, 13);
            this.labelControl10.TabIndex = 99;
            this.labelControl10.Text = "Gage Reading";
            // 
            // txtMileage
            // 
            this.txtMileage.Location = new System.Drawing.Point(431, 57);
            this.txtMileage.MenuManager = this.ribbonControl1;
            this.txtMileage.Name = "txtMileage";
            this.txtMileage.Size = new System.Drawing.Size(176, 20);
            this.txtMileage.TabIndex = 96;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(334, 83);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(31, 13);
            this.labelControl11.TabIndex = 97;
            this.labelControl11.Text = "Status";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(15, 129);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(76, 13);
            this.labelControl8.TabIndex = 81;
            this.labelControl8.Text = "Chassis Number";
            // 
            // txtChassisNumber
            // 
            this.txtChassisNumber.Location = new System.Drawing.Point(113, 126);
            this.txtChassisNumber.MenuManager = this.ribbonControl1;
            this.txtChassisNumber.Name = "txtChassisNumber";
            this.txtChassisNumber.Size = new System.Drawing.Size(176, 20);
            this.txtChassisNumber.TabIndex = 82;
            // 
            // icbCountry
            // 
            this.icbCountry.Location = new System.Drawing.Point(113, 195);
            this.icbCountry.MenuManager = this.ribbonControl1;
            this.icbCountry.Name = "icbCountry";
            this.icbCountry.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbCountry.Size = new System.Drawing.Size(176, 20);
            this.icbCountry.TabIndex = 88;
            // 
            // icbModel
            // 
            this.icbModel.Location = new System.Drawing.Point(113, 172);
            this.icbModel.MenuManager = this.ribbonControl1;
            this.icbModel.Name = "icbModel";
            this.icbModel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbModel.Size = new System.Drawing.Size(176, 20);
            this.icbModel.TabIndex = 86;
            // 
            // icbMake
            // 
            this.icbMake.Location = new System.Drawing.Point(113, 149);
            this.icbMake.MenuManager = this.ribbonControl1;
            this.icbMake.Name = "icbMake";
            this.icbMake.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbMake.Size = new System.Drawing.Size(176, 20);
            this.icbMake.TabIndex = 84;
            this.icbMake.SelectedIndexChanged += new System.EventHandler(this.icbMake_SelectedIndexChanged);
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(15, 221);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(92, 13);
            this.labelControl7.TabIndex = 89;
            this.labelControl7.Text = "Manufactured Year";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(15, 198);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(39, 13);
            this.labelControl4.TabIndex = 87;
            this.labelControl4.Text = "Country";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(15, 175);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(28, 13);
            this.labelControl5.TabIndex = 85;
            this.labelControl5.Text = "Model";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(15, 152);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(25, 13);
            this.labelControl6.TabIndex = 83;
            this.labelControl6.Text = "Make";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(15, 106);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(68, 13);
            this.labelControl3.TabIndex = 79;
            this.labelControl3.Text = "Motor Number";
            // 
            // txtMotorNumber
            // 
            this.txtMotorNumber.Location = new System.Drawing.Point(113, 103);
            this.txtMotorNumber.MenuManager = this.ribbonControl1;
            this.txtMotorNumber.Name = "txtMotorNumber";
            this.txtMotorNumber.Size = new System.Drawing.Size(176, 20);
            this.txtMotorNumber.TabIndex = 80;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(16, 83);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(64, 13);
            this.labelControl2.TabIndex = 75;
            this.labelControl2.Text = "Plate Number";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(16, 38);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(47, 13);
            this.labelControl1.TabIndex = 71;
            this.labelControl1.Text = "Vehicle ID";
            // 
            // txtVehicleId
            // 
            this.txtVehicleId.Enabled = false;
            this.txtVehicleId.Location = new System.Drawing.Point(113, 34);
            this.txtVehicleId.MenuManager = this.ribbonControl1;
            this.txtVehicleId.Name = "txtVehicleId";
            this.txtVehicleId.Size = new System.Drawing.Size(176, 20);
            this.txtVehicleId.TabIndex = 72;
            // 
            // gpcVehicleList
            // 
            this.gpcVehicleList.Controls.Add(this.gcVehicleList);
            this.gpcVehicleList.Location = new System.Drawing.Point(21, 363);
            this.gpcVehicleList.Name = "gpcVehicleList";
            this.gpcVehicleList.Size = new System.Drawing.Size(750, 198);
            this.gpcVehicleList.TabIndex = 46;
            this.gpcVehicleList.Text = "Vehicle List";
            // 
            // gcVehicleList
            // 
            this.gcVehicleList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcVehicleList.Location = new System.Drawing.Point(2, 20);
            this.gcVehicleList.MainView = this.gvVehicleList;
            this.gcVehicleList.MenuManager = this.ribbonControl1;
            this.gcVehicleList.Name = "gcVehicleList";
            this.gcVehicleList.Size = new System.Drawing.Size(746, 176);
            this.gcVehicleList.TabIndex = 39;
            this.gcVehicleList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvVehicleList});
            // 
            // gvVehicleList
            // 
            this.gvVehicleList.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.gvVehicleList.Appearance.OddRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.gvVehicleList.Appearance.OddRow.Options.UseBackColor = true;
            this.gvVehicleList.AppearancePrint.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.gvVehicleList.AppearancePrint.OddRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.gvVehicleList.AppearancePrint.OddRow.Options.UseBackColor = true;
            this.gvVehicleList.GridControl = this.gcVehicleList;
            this.gvVehicleList.Name = "gvVehicleList";
            // 
            // RegisterVehicle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(795, 563);
            this.Controls.Add(this.gpcVehicleList);
            this.Controls.Add(this.ddbExport);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.ribbonControl1);
            this.Name = "RegisterVehicle";
            this.Ribbon = this.ribbonControl1;
            this.Text = "RegisterVehicle";
            this.Load += new System.EventHandler(this.RegisterVehicle_Load);
            this.Resize += new System.EventHandler(this.RegisterVehicle_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icbManYear.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlateNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbPlateCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbPlateRegion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGageReading.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtePurchasedDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtePurchasedDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imsVehicleImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbFuelType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMileage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChassisNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbCountry.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbModel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbMake.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMotorNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVehicleId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpcVehicleList)).EndInit();
            this.gpcVehicleList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcVehicleList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVehicleList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private System.Windows.Forms.OpenFileDialog ofdBrowseImage;
        private DevExpress.XtraEditors.DropDownButton ddbExport;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.ImageComboBoxEdit icbManYear;
        private DevExpress.XtraEditors.TextEdit txtPlateNumber;
        private DevExpress.XtraEditors.ImageComboBoxEdit icbPlateCode;
        private DevExpress.XtraEditors.ImageComboBoxEdit icbPlateRegion;
        private DevExpress.XtraEditors.ImageComboBoxEdit icbType;
        private DevExpress.XtraEditors.LabelControl Type;
        private DevExpress.XtraEditors.TextEdit txtGageReading;
        private DevExpress.XtraEditors.DateEdit dtePurchasedDate;
        private DevExpress.XtraEditors.SimpleButton sbnBrowseImage;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.SimpleButton sbnClear;
        private DevExpress.XtraEditors.SimpleButton sbnRegister;
        private DevExpress.XtraEditors.Controls.ImageSlider imsVehicleImage;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.ImageComboBoxEdit icbStatus;
        private DevExpress.XtraEditors.ImageComboBoxEdit icbFuelType;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txtMileage;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtChassisNumber;
        private DevExpress.XtraEditors.ImageComboBoxEdit icbCountry;
        private DevExpress.XtraEditors.ImageComboBoxEdit icbModel;
        private DevExpress.XtraEditors.ImageComboBoxEdit icbMake;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtMotorNumber;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtVehicleId;
        private DevExpress.XtraEditors.GroupControl gpcVehicleList;
        private DevExpress.XtraGrid.GridControl gcVehicleList;
        private DevExpress.XtraGrid.Views.Grid.GridView gvVehicleList;
    }
}