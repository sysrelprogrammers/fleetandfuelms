﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraEditors.Controls;

namespace FleetFuelMS
{
    public partial class RegisterLubricant : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public RegisterLubricant()
        {
            InitializeComponent();
        }

        private void RegisterLubricant_Load(object sender, EventArgs e)
        {
            loadControls();
        }


        private void loadControls()
        {
            txtLubricantId.Text = Tasks.getNewCode("LUBRICANT", 2);
            loadLubricantType();
            loadLubricantList();

            icbLubricantType.SelectedIndex = 0;
        }

        private void loadLubricantType()
        {
            icbLubricantType.Properties.Items.Add(new ImageComboBoxItem("-- SLECT TYPE --"));
            icbLubricantType.Properties.Items.Add(new ImageComboBoxItem("Grease"));
            icbLubricantType.Properties.Items.Add(new ImageComboBoxItem("Oil"));
        }

        private void loadLubricantList()
        {
            SqlCommand cmdLubricant = new SqlCommand()
            {
                Connection = GloVars.Conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "getLubricantInfo",
                CommandTimeout = 0
            };

            SqlDataReader rdrLubricant = null;

            try
            {
                cmdLubricant.Parameters.Add(new SqlParameter("@LID", "%"));
                rdrLubricant = cmdLubricant.ExecuteReader();

                DataTable dtLubricant = new DataTable("Lubricant");

                dtLubricant.Columns.Add("LubricantID");
                dtLubricant.Columns.Add("Lubricant Name");
                dtLubricant.Columns.Add("Lubricant Type");
                dtLubricant.Columns.Add("Current Price");
                dtLubricant.Columns.Add("Price Start Date");
                dtLubricant.Columns.Add("Description");

                while (rdrLubricant.Read())
                {
                    DataRow drLubricant = dtLubricant.NewRow();
                    drLubricant["LubricantID"] = rdrLubricant["LubricantID"].ToString();
                    drLubricant["Lubricant Name"] = rdrLubricant["LName"].ToString();
                    drLubricant["Lubricant Type"] = rdrLubricant["LType"].ToString();
                    drLubricant["Current Price"] = rdrLubricant["CurrentPrice"].ToString();
                    drLubricant["Price Start Date"] = rdrLubricant["CPriceStartDate"].ToString();
                    drLubricant["Description"] = rdrLubricant["LDescription"].ToString();

                    dtLubricant.Rows.Add(drLubricant);
                }

                gcLubricantList.DataSource = dtLubricant;
                gvLubricantList.BestFitColumns(true);
                for( int i = 0; i < gvLubricantList.Columns.Count; i++)
                    gvLubricantList.Columns[i].BestFit();
            }
            catch (SqlException Ex)
            {

            }
            finally
            {
                rdrLubricant.Close();
                cmdLubricant.Dispose();
            }
        }

        private void sbnRegister_Click(object sender, EventArgs e)
        {
            SqlCommand cmdRegLubricant = new SqlCommand()
            {
                Connection = GloVars.Conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "registerLubricant",
                CommandTimeout = 30
            };

            try
            {
                string newLubricantID = "";

                newLubricantID = Tasks.getNewCode("LUBRICANT", 3);

                cmdRegLubricant.Parameters.Add(new SqlParameter("@LubricantID", newLubricantID));
                cmdRegLubricant.Parameters.Add(new SqlParameter("@LName", txtLubricantName.Text));
                cmdRegLubricant.Parameters.Add(new SqlParameter("@LType", icbLubricantType.Properties.Items[icbLubricantType.SelectedIndex].ToString()));
                cmdRegLubricant.Parameters.Add(new SqlParameter("@CurrentPrice", txtCurrentPrice.Text));
                cmdRegLubricant.Parameters.Add(new SqlParameter("@CPriceStartDate", DateTime.Now));
                cmdRegLubricant.Parameters.Add(new SqlParameter("@LDescription", txtDescription.Text));
                cmdRegLubricant.Parameters.Add(new SqlParameter("@RegDate", Tasks.getCurrentTime()));
                cmdRegLubricant.Parameters.Add(new SqlParameter("@RUser", GloVars.loggedUser.UserID));

                cmdRegLubricant.ExecuteNonQuery();

                // Update the 
                Tasks.updateCodeLog("LUBRICANT", newLubricantID);

                MessageBox.Show(GloVars.MSG_LUBRICANT_REGISTERED);

                loadLubricantList();
            }
            catch (SqlException Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                cmdRegLubricant.Dispose();

            }
        }

        private void RegisterLubricant_Resize(object sender, EventArgs e)
        {

            gpcLubricantList.Width = this.Width - (gpcLubricantList.Left + 30);
            gpcLubricantList.Height = this.Height - (gpcLubricantList.Top + 30);
        }
    }
}
