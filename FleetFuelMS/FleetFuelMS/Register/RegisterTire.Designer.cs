﻿namespace FleetFuelMS
{
    partial class RegisterTire
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegisterTire));
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.lblWidth = new DevExpress.XtraEditors.LabelControl();
            this.lblMaxLoad = new DevExpress.XtraEditors.LabelControl();
            this.icbBrand = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.tbcWidth = new DevExpress.XtraEditors.TrackBarControl();
            this.tbcMaxLoad = new DevExpress.XtraEditors.TrackBarControl();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtCurrentPrice = new DevExpress.XtraEditors.TextEdit();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.Type = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtTireId = new DevExpress.XtraEditors.TextEdit();
            this.txtDOT = new DevExpress.XtraEditors.TextEdit();
            this.gpcTireList = new DevExpress.XtraEditors.GroupControl();
            this.gcTireList = new DevExpress.XtraGrid.GridControl();
            this.gvTireList = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.sbnClear = new DevExpress.XtraEditors.SimpleButton();
            this.sbnRegister = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icbBrand.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcWidth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcMaxLoad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcMaxLoad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrentPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTireId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDOT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpcTireList)).BeginInit();
            this.gpcTireList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTireList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTireList)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 1;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Size = new System.Drawing.Size(698, 49);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.lblWidth);
            this.groupControl1.Controls.Add(this.lblMaxLoad);
            this.groupControl1.Controls.Add(this.icbBrand);
            this.groupControl1.Controls.Add(this.tbcWidth);
            this.groupControl1.Controls.Add(this.tbcMaxLoad);
            this.groupControl1.Controls.Add(this.txtDescription);
            this.groupControl1.Controls.Add(this.labelControl9);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.txtCurrentPrice);
            this.groupControl1.Controls.Add(this.txtName);
            this.groupControl1.Controls.Add(this.Type);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.txtTireId);
            this.groupControl1.Controls.Add(this.txtDOT);
            this.groupControl1.Location = new System.Drawing.Point(12, 52);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(590, 217);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Tire Information";
            // 
            // lblWidth
            // 
            this.lblWidth.Location = new System.Drawing.Point(135, 177);
            this.lblWidth.Name = "lblWidth";
            this.lblWidth.Size = new System.Drawing.Size(6, 13);
            this.lblWidth.TabIndex = 12;
            this.lblWidth.Text = "0";
            // 
            // lblMaxLoad
            // 
            this.lblMaxLoad.Location = new System.Drawing.Point(135, 140);
            this.lblMaxLoad.Name = "lblMaxLoad";
            this.lblMaxLoad.Size = new System.Drawing.Size(6, 13);
            this.lblMaxLoad.TabIndex = 9;
            this.lblMaxLoad.Text = "0";
            // 
            // icbBrand
            // 
            this.icbBrand.Location = new System.Drawing.Point(135, 106);
            this.icbBrand.MenuManager = this.ribbonControl1;
            this.icbBrand.Name = "icbBrand";
            this.icbBrand.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbBrand.Size = new System.Drawing.Size(145, 20);
            this.icbBrand.TabIndex = 7;
            // 
            // tbcWidth
            // 
            this.tbcWidth.EditValue = null;
            this.tbcWidth.Location = new System.Drawing.Point(167, 167);
            this.tbcWidth.MenuManager = this.ribbonControl1;
            this.tbcWidth.Name = "tbcWidth";
            this.tbcWidth.Properties.LabelAppearance.Options.UseTextOptions = true;
            this.tbcWidth.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tbcWidth.Size = new System.Drawing.Size(116, 45);
            this.tbcWidth.TabIndex = 13;
            this.tbcWidth.ValueChanged += new System.EventHandler(this.tbcWidth_ValueChanged);
            // 
            // tbcMaxLoad
            // 
            this.tbcMaxLoad.EditValue = null;
            this.tbcMaxLoad.Location = new System.Drawing.Point(169, 129);
            this.tbcMaxLoad.MenuManager = this.ribbonControl1;
            this.tbcMaxLoad.Name = "tbcMaxLoad";
            this.tbcMaxLoad.Properties.LabelAppearance.Options.UseTextOptions = true;
            this.tbcMaxLoad.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tbcMaxLoad.Properties.ShowLabels = true;
            this.tbcMaxLoad.Size = new System.Drawing.Size(116, 45);
            this.tbcMaxLoad.TabIndex = 10;
            this.tbcMaxLoad.ValueChanged += new System.EventHandler(this.tbcMaxLoad_ValueChanged);
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(306, 86);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(261, 115);
            this.txtDescription.TabIndex = 17;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(306, 62);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(53, 13);
            this.labelControl9.TabIndex = 16;
            this.labelControl9.Text = "Description";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(18, 177);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(28, 13);
            this.labelControl6.TabIndex = 11;
            this.labelControl6.Text = "Width";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(306, 40);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(63, 13);
            this.labelControl7.TabIndex = 14;
            this.labelControl7.Text = "Current Price";
            // 
            // txtCurrentPrice
            // 
            this.txtCurrentPrice.Location = new System.Drawing.Point(423, 38);
            this.txtCurrentPrice.MenuManager = this.ribbonControl1;
            this.txtCurrentPrice.Name = "txtCurrentPrice";
            this.txtCurrentPrice.Properties.Mask.BeepOnError = true;
            this.txtCurrentPrice.Properties.Mask.EditMask = "c";
            this.txtCurrentPrice.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCurrentPrice.Size = new System.Drawing.Size(145, 20);
            this.txtCurrentPrice.TabIndex = 15;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(135, 83);
            this.txtName.MenuManager = this.ribbonControl1;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(145, 20);
            this.txtName.TabIndex = 5;
            // 
            // Type
            // 
            this.Type.Location = new System.Drawing.Point(18, 62);
            this.Type.Name = "Type";
            this.Type.Size = new System.Drawing.Size(68, 13);
            this.Type.TabIndex = 2;
            this.Type.Text = "DOT ID/ Serial";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(18, 140);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(43, 13);
            this.labelControl8.TabIndex = 8;
            this.labelControl8.Text = "MaxLoad";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(18, 109);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(28, 13);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Brand";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(18, 85);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(27, 13);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Name";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(18, 40);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(32, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Tire ID";
            // 
            // txtTireId
            // 
            this.txtTireId.Enabled = false;
            this.txtTireId.Location = new System.Drawing.Point(135, 37);
            this.txtTireId.MenuManager = this.ribbonControl1;
            this.txtTireId.Name = "txtTireId";
            this.txtTireId.Size = new System.Drawing.Size(145, 20);
            this.txtTireId.TabIndex = 1;
            // 
            // txtDOT
            // 
            this.txtDOT.Location = new System.Drawing.Point(135, 60);
            this.txtDOT.MenuManager = this.ribbonControl1;
            this.txtDOT.Name = "txtDOT";
            this.txtDOT.Size = new System.Drawing.Size(145, 20);
            this.txtDOT.TabIndex = 3;
            // 
            // gpcTireList
            // 
            this.gpcTireList.Controls.Add(this.gcTireList);
            this.gpcTireList.Location = new System.Drawing.Point(12, 317);
            this.gpcTireList.Name = "gpcTireList";
            this.gpcTireList.Size = new System.Drawing.Size(677, 210);
            this.gpcTireList.TabIndex = 3;
            this.gpcTireList.Text = "List of Tires";
            // 
            // gcTireList
            // 
            this.gcTireList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcTireList.Location = new System.Drawing.Point(2, 20);
            this.gcTireList.MainView = this.gvTireList;
            this.gcTireList.MenuManager = this.ribbonControl1;
            this.gcTireList.Name = "gcTireList";
            this.gcTireList.Size = new System.Drawing.Size(673, 188);
            this.gcTireList.TabIndex = 0;
            this.gcTireList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvTireList});
            // 
            // gvTireList
            // 
            this.gvTireList.GridControl = this.gcTireList;
            this.gvTireList.Name = "gvTireList";
            // 
            // sbnClear
            // 
            this.sbnClear.Image = ((System.Drawing.Image)(resources.GetObject("sbnClear.Image")));
            this.sbnClear.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.sbnClear.Location = new System.Drawing.Point(619, 120);
            this.sbnClear.Name = "sbnClear";
            this.sbnClear.Size = new System.Drawing.Size(70, 62);
            this.sbnClear.TabIndex = 2;
            this.sbnClear.Text = "Clear";
            // 
            // sbnRegister
            // 
            this.sbnRegister.Image = ((System.Drawing.Image)(resources.GetObject("sbnRegister.Image")));
            this.sbnRegister.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.sbnRegister.Location = new System.Drawing.Point(618, 52);
            this.sbnRegister.Name = "sbnRegister";
            this.sbnRegister.Size = new System.Drawing.Size(70, 62);
            this.sbnRegister.TabIndex = 1;
            this.sbnRegister.Text = "Register";
            this.sbnRegister.Click += new System.EventHandler(this.sbnRegister_Click);
            // 
            // RegisterTire
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(698, 539);
            this.Controls.Add(this.sbnClear);
            this.Controls.Add(this.sbnRegister);
            this.Controls.Add(this.gpcTireList);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.ribbonControl1);
            this.Name = "RegisterTire";
            this.Ribbon = this.ribbonControl1;
            this.Text = "RegisterTire";
            this.Load += new System.EventHandler(this.RegisterTire_Load);
            this.Resize += new System.EventHandler(this.RegisterTire_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icbBrand.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcWidth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcMaxLoad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcMaxLoad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrentPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTireId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDOT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpcTireList)).EndInit();
            this.gpcTireList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcTireList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTireList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraEditors.LabelControl Type;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtTireId;
        private DevExpress.XtraEditors.TextEdit txtDOT;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtCurrentPrice;
        private System.Windows.Forms.TextBox txtDescription;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.GroupControl gpcTireList;
        private DevExpress.XtraEditors.SimpleButton sbnClear;
        private DevExpress.XtraEditors.SimpleButton sbnRegister;
        private DevExpress.XtraGrid.GridControl gcTireList;
        private DevExpress.XtraGrid.Views.Grid.GridView gvTireList;
        private DevExpress.XtraEditors.TrackBarControl tbcMaxLoad;
        private DevExpress.XtraEditors.TrackBarControl tbcWidth;
        private DevExpress.XtraEditors.ImageComboBoxEdit icbBrand;
        private DevExpress.XtraEditors.LabelControl lblWidth;
        private DevExpress.XtraEditors.LabelControl lblMaxLoad;
    }
}