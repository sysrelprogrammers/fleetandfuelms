﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors;

namespace FleetFuelMS
{
    public partial class RegisterTire : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public RegisterTire()
        {
            InitializeComponent();
        }

        private void RegisterTire_Load(object sender, EventArgs e)
        {
            loadControls();
        }

        private void loadControls()
        {
            txtTireId.Text = Tasks.getNewCode("Tire", 3);

            loadTireBrand();

            loadTireList();

            icbBrand.SelectedIndex = 0;
        }
        private void loadTireBrand()
        {
            icbBrand.Properties.Items.Clear();
            icbBrand.Properties.Items.Add(new ImageComboBoxItem("-- SELECT BRAND --"));
            foreach (string tireBrand in GloVars.listOfTireBrands)
            {
                icbBrand.Properties.Items.Add(new ImageComboBoxItem(tireBrand));
            }
        }

        private void loadTireList()
        {
            SqlCommand cmdTire = new SqlCommand() {
                Connection = GloVars.Conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "getTireInfo",
                CommandTimeout = 0
            };

            SqlDataReader rdrTire = null;

            try
            {
                cmdTire.Parameters.Add(new SqlParameter("@TID", "%"));
                rdrTire = cmdTire.ExecuteReader();

                DataTable dtTire = new DataTable("Tire");

                dtTire.Columns.Add("TireID");
                dtTire.Columns.Add("DOTIdentificationNo");
                dtTire.Columns.Add("Tire Name");
                dtTire.Columns.Add("Brand");
                dtTire.Columns.Add("MaxLoad");
                dtTire.Columns.Add("Width");
                dtTire.Columns.Add("CurrentPrice");
                dtTire.Columns.Add("CPriceStartDate");
                dtTire.Columns.Add("Description");

                while (rdrTire.Read())
                {
                    DataRow drTire = dtTire.NewRow();
                    drTire["TireID"] = rdrTire["TireID"].ToString();
                    drTire["DOTIdentificationNo"] = rdrTire["DOTIdentificationNo"].ToString();
                    drTire["Tire Name"] = rdrTire["TName"].ToString();
                    drTire["Brand"] = rdrTire["Brand"].ToString();
                    drTire["MaxLoad"] = rdrTire["MaxLoad"].ToString();
                    drTire["Width"] = rdrTire["Width"].ToString();
                    drTire["CurrentPrice"] = rdrTire["CurrentPrice"].ToString();
                    drTire["CPriceStartDate"] = rdrTire["CPriceStartDate"].ToString();
                    drTire["Description"] = rdrTire["TDescription"].ToString();

                    dtTire.Rows.Add(drTire);
                }

                gcTireList.DataSource = dtTire;
                gvTireList.BestFitColumns(true);
                gvTireList.Columns[0].BestFit();
            }
            catch (SqlException Ex) {

            }
            finally {
                rdrTire.Close();
                cmdTire.Dispose();
            }

        }

        private void RegisterTire_Resize(object sender, EventArgs e)
        {
            gpcTireList.Width = this.Width - (gpcTireList.Left + 30);
            gpcTireList.Height = this.Height - (gpcTireList.Top + 30);
        }

        private void sbnRegister_Click(object sender, EventArgs e)
        {
            SqlCommand cmdRegTire = new SqlCommand()
            {
                Connection = GloVars.Conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "registerTire",
                CommandTimeout = 30
            };

            try
            {
                string newTireID = "";

                newTireID = Tasks.getNewCode("TIRE", 3);

                cmdRegTire.Parameters.Add(new SqlParameter("@TireID", newTireID));
                cmdRegTire.Parameters.Add(new SqlParameter("@DOTidentificationNo", txtDOT.Text));
                cmdRegTire.Parameters.Add(new SqlParameter("@TName", txtName.Text));
                cmdRegTire.Parameters.Add(new SqlParameter("@Brand", icbBrand.Properties.Items[icbBrand.SelectedIndex].ToString()));
                cmdRegTire.Parameters.Add(new SqlParameter("@MaxLoad", tbcMaxLoad.Value));
                cmdRegTire.Parameters.Add(new SqlParameter("@Width", tbcWidth.Value));
                cmdRegTire.Parameters.Add(new SqlParameter("@CurrentPrice", txtCurrentPrice.Text));
                cmdRegTire.Parameters.Add(new SqlParameter("@CPriceStartDate", DateTime.Now));
                cmdRegTire.Parameters.Add(new SqlParameter("@TDescription", txtDescription.Text));
                cmdRegTire.Parameters.Add(new SqlParameter("@RegDate", Tasks.getCurrentTime()));
                cmdRegTire.Parameters.Add(new SqlParameter("@RUser", GloVars.loggedUser.UserID));

                cmdRegTire.ExecuteNonQuery();

                // Update the 
                Tasks.updateCodeLog("TIRE", newTireID);

                MessageBox.Show(GloVars.MSG_TIRE_REGISTERED);
                loadTireList();
            }
            catch (SqlException Ex) {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                cmdRegTire.Dispose();
            }
        }

        private void tbcMaxLoad_ValueChanged(object sender, EventArgs e)
        {
            lblMaxLoad.Text = tbcMaxLoad.Value.ToString();
        }

        private void tbcWidth_ValueChanged(object sender, EventArgs e)
        {
            lblWidth.Text = tbcWidth.Value.ToString();
        }
    }
}