﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraEditors.Controls;
using System.Data.SqlTypes;

namespace FleetFuelMS
{
    public partial class RegisterVehicle : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public RegisterVehicle()
        {
            InitializeComponent();
        }

        private void icbMake_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadVehicleModel(icbMake.Properties.Items[icbMake.SelectedIndex].ToString());
        }

        private void RegisterVehicle_Resize(object sender, EventArgs e)
        {
            gpcVehicleList.Location = new Point(18, 357);
            gpcVehicleList.Height = this.Height - (gpcVehicleList.Top+50);
            gpcVehicleList.Width = this.Width - (2 * gpcVehicleList.Left + 10);
        }

        private void RegisterVehicle_Load(object sender, EventArgs e)
        {
            loadControls();
        }

        private void loadControls()
        {
            txtVehicleId.Text = Tasks.getNewCode("VEHICLE", 3);
            dtePurchasedDate.DateTime = DateTime.Today;

            //Load Plate Regions
            icbPlateRegion.Properties.Items.Add(new ImageComboBoxItem("--"));
            icbPlateRegion.Properties.Items.Add(new ImageComboBoxItem("AA"));
            icbPlateRegion.Properties.Items.Add(new ImageComboBoxItem("SP"));
            icbPlateRegion.Properties.Items.Add(new ImageComboBoxItem("OR"));
            icbPlateRegion.Properties.Items.Add(new ImageComboBoxItem("AM"));
            icbPlateRegion.Properties.Items.Add(new ImageComboBoxItem("GM"));
            icbPlateRegion.Properties.Items.Add(new ImageComboBoxItem("TG"));
            icbPlateRegion.Properties.Items.Add(new ImageComboBoxItem("SM"));

            //Load Plate Codes
            icbPlateCode.Properties.Items.Add(new ImageComboBoxItem("--"));
            icbPlateCode.Properties.Items.Add(new ImageComboBoxItem("01"));
            icbPlateCode.Properties.Items.Add(new ImageComboBoxItem("02"));
            icbPlateCode.Properties.Items.Add(new ImageComboBoxItem("03"));
            icbPlateCode.Properties.Items.Add(new ImageComboBoxItem("04"));
            icbPlateCode.Properties.Items.Add(new ImageComboBoxItem("35"));

            //Load Vehicle Status
            icbStatus.Properties.Items.Add(new ImageComboBoxItem("-- SELECT STATUS --"));
            icbStatus.Properties.Items.Add(new ImageComboBoxItem("Working"));
            icbStatus.Properties.Items.Add(new ImageComboBoxItem("Under Maintenance"));
            icbStatus.Properties.Items.Add(new ImageComboBoxItem("Out Of Service"));

            //Load Year for Manufacture
            for (long i = 1960; i <= 2030; i++)
                icbManYear.Properties.Items.Add(new ImageComboBoxItem(i.ToString()));


            loadVehicleType();
            loadVehicleMake();
            loadCountry();
            loadFuelType();
            loadVehicleList();

            icbType.SelectedIndex = 0;
            icbPlateRegion.SelectedIndex = 0;
            icbPlateCode.SelectedIndex = 0;
            icbMake.SelectedIndex = 0;
            icbModel.SelectedIndex = 0;
            icbCountry.SelectedIndex = 0;
            icbManYear.SelectedIndex = 0;
            icbFuelType.SelectedIndex = 0;
            icbStatus.SelectedIndex = 0;
        }

        private void sbnRegister_Click(object sender, EventArgs e)
        {
            // Check Validation

            //Proceed with registration if validation is complete
            registerVehicle();
        }

        private void loadFuelType()
        {
            SqlCommand cmdLoadFuel = new SqlCommand()
            {
                Connection = GloVars.Conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "getFuelTypes"
            };

            SqlDataReader rdrLoadFuel = null;
            try
            {
                rdrLoadFuel = cmdLoadFuel.ExecuteReader();
                icbFuelType.Properties.Items.Add(new ImageComboBoxItem("-- SELECT FUEL --"));

                while (rdrLoadFuel.Read())
                {
                    icbFuelType.Properties.Items.Add(new ImageComboBoxItem(rdrLoadFuel[0].ToString()));
                }
            }
            catch (SqlException Ex)
            {

            }
            finally
            {
                rdrLoadFuel.Close();
                cmdLoadFuel.Dispose();
            }
        }

        private void loadVehicleMake() {
            SqlCommand cmdLoadVM = new SqlCommand()
            {
                Connection = GloVars.Conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "getVehicleMakes"
            };

            SqlDataReader rdrVehicleMake = null;
            try
            {
                rdrVehicleMake = cmdLoadVM.ExecuteReader();

                icbMake.Properties.Items.Add(new ImageComboBoxItem("-- SELECT MAKE --"));

                while (rdrVehicleMake.Read())
                    icbMake.Properties.Items.Add(new ImageComboBoxItem(rdrVehicleMake[0].ToString()));
            }
            catch (SqlException Ex) { MessageBox.Show(Ex.Message);}
            finally
            {
                rdrVehicleMake.Close();
                cmdLoadVM.Dispose();
            }
        }

        private void loadVehicleType()
        {
            SqlCommand cmdLoadVT = new SqlCommand()
            {
                Connection = GloVars.Conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "getVehicleTypes"
            };

            SqlDataReader rdr = null;

            try
            {
                rdr = cmdLoadVT.ExecuteReader();

                icbType.Properties.Items.Add(new ImageComboBoxItem("-- SELECT TYPE --"));
                while (rdr.Read())
                    icbType.Properties.Items.Add(new ImageComboBoxItem(rdr[0].ToString()));

            }
            catch (SqlException Ex) {
            }
            finally
            { 
                rdr.Close();
                cmdLoadVT.Dispose();
            }
        }

        private void loadVehicleModel(String Make)
        {
            SqlCommand cmdLoadModel = new SqlCommand()
            {
                Connection = GloVars.Conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "getVehicleModels"
            };

            SqlDataReader rdrVehicleModel = null; 

            try
            {
                cmdLoadModel.Parameters.Add(new SqlParameter("@Make", Make));

                rdrVehicleModel = cmdLoadModel.ExecuteReader();

                icbModel.Properties.Items.Clear();

                icbModel.Properties.Items.Add(new ImageComboBoxItem("-- SELECT MODEL --"));
                icbModel.SelectedIndex = 0;

                while (rdrVehicleModel.Read())
                {
                    icbModel.Properties.Items.Add(new ImageComboBoxItem(rdrVehicleModel[0]));
                }
            }

            catch (SqlException Ex) { MessageBox.Show(Ex.Message);}

            finally {
                rdrVehicleModel.Close();
                cmdLoadModel.Dispose();
            }
        }

        private void loadCountry()
        {
            SqlCommand cmdCountry = new SqlCommand()
            {
                Connection = GloVars.Conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "getCountries"
            };

            SqlDataReader rdrLoadCountry = null;

            try
            {
                rdrLoadCountry = cmdCountry.ExecuteReader();

                ImageList ilCountries = new ImageList();

                FileStream fsCImage;
                ImageComboBoxItem icbt;
                icbCountry.Properties.SmallImages = ilCountries;
                icbCountry.Properties.Items.Add(new ImageComboBoxItem("-- SELECT COUNTRY --"));

                int imageIndex = 0;
                
                while (rdrLoadCountry.Read())
                {

                    icbt = new ImageComboBoxItem(rdrLoadCountry["Country"].ToString());

                    fsCImage = new FileStream("temp.png", FileMode.Create);
                    byte[] btCImage = (byte[]) rdrLoadCountry["CImage"];
                    fsCImage.Write(btCImage, 0, btCImage.Length);
                    //ilCountries.Images.Add(Image.FromStream(fsCImage));

                    //icbt.ImageIndex = imageIndex++;

                    fsCImage.Close();

                    icbCountry.Properties.Items.Add(icbt);
                }
            }

            catch (SqlException Ex)
            {
                //Do Stuff here to prevent from crushing when connection is unavailable
            }
            finally
            {
                rdrLoadCountry.Close();
                cmdCountry.Dispose();
            }
        }

        private void loadVehicleList()
        {
            SqlCommand cmdloadV = new SqlCommand()
            {
                Connection = GloVars.Conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "getVehicleInfo"
            };

            SqlDataReader rdrLoadVehicle = null;
            try
            {
                cmdloadV.Parameters.Add(new SqlParameter("@VID", "%"));
                rdrLoadVehicle = cmdloadV.ExecuteReader();

                DataTable dtVehicle = new DataTable();

                dtVehicle.Columns.Add("SNo");
                dtVehicle.Columns.Add("VehicleID");
                dtVehicle.Columns.Add("PlateNo");
                dtVehicle.Columns.Add("MotorNo");
                dtVehicle.Columns.Add("ChassisNo");
                dtVehicle.Columns.Add("VType");
                dtVehicle.Columns.Add("FuelType");
                dtVehicle.Columns.Add("Mileage");
                dtVehicle.Columns.Add("Make");
                dtVehicle.Columns.Add("Model");
                dtVehicle.Columns.Add("Country");
                dtVehicle.Columns.Add("ManufacturedYear");
                dtVehicle.Columns.Add("PurchaseDate");
                dtVehicle.Columns.Add("StartGageReading");
                dtVehicle.Columns.Add("VStatus");

                while (rdrLoadVehicle.Read())
                {
                    DataRow drVehicle = dtVehicle.NewRow();
                    drVehicle["SNo"] = rdrLoadVehicle["SNo"];
                    drVehicle["VehicleID"] = rdrLoadVehicle["VehicleID"];
                    drVehicle["PlateNo"] = rdrLoadVehicle["PlateNo"];
                    drVehicle["MotorNo"] = rdrLoadVehicle["MotorNo"];
                    drVehicle["ChassisNo"] = rdrLoadVehicle["ChassisNo"];
                    drVehicle["VType"] = rdrLoadVehicle["VType"];
                    drVehicle["FuelType"] = rdrLoadVehicle["FName"];
                    drVehicle["Mileage"] = rdrLoadVehicle["Mileage"];
                    drVehicle["Make"] = rdrLoadVehicle["Make"];
                    drVehicle["Model"] = rdrLoadVehicle["Model"];
                    drVehicle["Country"] = rdrLoadVehicle["Country"];
                    drVehicle["ManufacturedYear"] = rdrLoadVehicle["ManufacturedYear"];
                    drVehicle["PurchaseDate"] = rdrLoadVehicle["PurchaseDate"];
                    drVehicle["StartGageReading"] = rdrLoadVehicle["StartGageReading"];
                    drVehicle["VStatus"] = rdrLoadVehicle["VStatus"];

                    dtVehicle.Rows.Add(drVehicle);
                }

                gcVehicleList.DataSource = dtVehicle;
                gvVehicleList.ColumnPanelRowHeight = 25;
                gvVehicleList.OptionsFilter.AllowMRUFilterList = true;
                gvVehicleList.OptionsView.ShowAutoFilterRow = true;
                gvVehicleList.Appearance.HeaderPanel.Options.UseBackColor = true;
                gvVehicleList.Appearance.HeaderPanel.BackColor = Color.Black;

                gvVehicleList.BestFitColumns(true);
                gvVehicleList.Columns[0].BestFit();
            }
            catch (SqlException Ex) { MessageBox.Show(Ex.Message);}
            finally
            {
                rdrLoadVehicle.Close();
                cmdloadV.Dispose();
            }
        }

        private void registerVehicle()
        {
            SqlCommand cmdReg = new SqlCommand()
            {
                Connection = GloVars.Conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "registerVehicle"
            };
            try
            {
                string newVehicleCode = Tasks.getNewCode("VEHICLE", 3);
                string plateNum = string.Format("{0}-{1}-{2}", icbPlateRegion.Text, icbPlateCode.Text, txtPlateNumber.Text);

                cmdReg.Parameters.Add(new SqlParameter("@VehicleID", newVehicleCode)); // getNewCode and assign it as Sql paramter
                cmdReg.Parameters.Add(new SqlParameter("@VType", Tasks.getVehicleTypeID(icbType.Text)));
                cmdReg.Parameters.Add(new SqlParameter("@PlateNo", plateNum));
                cmdReg.Parameters.Add(new SqlParameter("@Make", Tasks.getVehicleMakeID( icbMake.Text, icbModel.Text )));
                cmdReg.Parameters.Add(new SqlParameter("@MotorNo", txtMotorNumber.Text));
                cmdReg.Parameters.Add(new SqlParameter("@ChassisNo", txtChassisNumber.Text));
                cmdReg.Parameters.Add(new SqlParameter("@ManufacturedYear", icbManYear.Text));
                cmdReg.Parameters.Add(new SqlParameter("@ManufacturedCountry", Tasks.getCountryCode(icbCountry.Text)));
                cmdReg.Parameters.Add(new SqlParameter("@PurchaseDate", dtePurchasedDate.DateTime ));
                cmdReg.Parameters.Add(new SqlParameter("@FuelType", Tasks.getFuelID(icbFuelType.Text)));
                cmdReg.Parameters.Add(new SqlParameter("@Mileage", txtMileage.Text));
                cmdReg.Parameters.Add(new SqlParameter("@StartGageReading", txtMileage.Text));
                cmdReg.Parameters.Add(new SqlParameter("@VStatus", icbStatus.Text));
                cmdReg.Parameters.Add(new SqlParameter("@VImage", Tasks.getImageByte(ofdBrowseImage.FileName)));
                cmdReg.Parameters.Add(new SqlParameter("@RegDate", Tasks.getCurrentTime()));
                cmdReg.Parameters.Add(new SqlParameter("@RUser", GloVars.loggedUser.UserID));

                cmdReg.ExecuteNonQuery();

                Tasks.updateCodeLog("VEHICLE", newVehicleCode);

                MessageBox.Show("Complete!");

                loadVehicleList();
            }
            catch(SqlTypeException Ex)
            { 
                MessageBox.Show(Ex.Message);
            }
            catch (SqlException Ex) {
                MessageBox.Show(Ex.Message);
            }
            finally {
                cmdReg.Dispose();
            }
        }
        
        private void sbnBrowseImage_Click(object sender, EventArgs e)
        {
            //imsVehicleImage.
            string ImagePath;

            if (ofdBrowseImage.ShowDialog() == DialogResult.OK)
            {
                ImagePath = ofdBrowseImage.FileName;
                imsVehicleImage.Images.Add(Image.FromFile(ImagePath));
            }

        }

        private void sbnExportToPDF_Click(object sender, EventArgs e)
        {
            SaveFileDialog ofdExportToPdf = new SaveFileDialog()
            {
                Filter = "Portable Document Format | *.pdf",
                Title = "Export to PDF"
            };


            if ( ofdExportToPdf.ShowDialog() == DialogResult.OK )
            {
                gcVehicleList.ExportToPdf(ofdExportToPdf.FileName);

                ViewPDF view = new ViewPDF();
                ViewPDF.PDFPath = ofdExportToPdf.FileName;
                view.MdiParent = GloVars.MDIPARENT;
                view.Show();
            }
        }
    }
}
