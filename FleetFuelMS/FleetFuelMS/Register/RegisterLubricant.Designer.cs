﻿namespace FleetFuelMS
{
    partial class RegisterLubricant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegisterLubricant));
            this.icbLubricantType = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.gvLubricantList = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcLubricantList = new DevExpress.XtraGrid.GridControl();
            this.sbnClear = new DevExpress.XtraEditors.SimpleButton();
            this.sbnRegister = new DevExpress.XtraEditors.SimpleButton();
            this.gpcLubricantList = new DevExpress.XtraEditors.GroupControl();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtCurrentPrice = new DevExpress.XtraEditors.TextEdit();
            this.Type = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtLubricantId = new DevExpress.XtraEditors.TextEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtLubricantName = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.icbLubricantType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLubricantList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcLubricantList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpcLubricantList)).BeginInit();
            this.gpcLubricantList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrentPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLubricantId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLubricantName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // icbLubricantType
            // 
            this.icbLubricantType.Location = new System.Drawing.Point(135, 83);
            this.icbLubricantType.MenuManager = this.ribbonControl1;
            this.icbLubricantType.Name = "icbLubricantType";
            this.icbLubricantType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbLubricantType.Size = new System.Drawing.Size(145, 20);
            this.icbLubricantType.TabIndex = 5;
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 1;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Size = new System.Drawing.Size(706, 49);
            // 
            // gvLubricantList
            // 
            this.gvLubricantList.GridControl = this.gcLubricantList;
            this.gvLubricantList.Name = "gvLubricantList";
            // 
            // gcLubricantList
            // 
            this.gcLubricantList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcLubricantList.Location = new System.Drawing.Point(2, 20);
            this.gcLubricantList.MainView = this.gvLubricantList;
            this.gcLubricantList.MenuManager = this.ribbonControl1;
            this.gcLubricantList.Name = "gcLubricantList";
            this.gcLubricantList.Size = new System.Drawing.Size(673, 188);
            this.gcLubricantList.TabIndex = 0;
            this.gcLubricantList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvLubricantList});
            // 
            // sbnClear
            // 
            this.sbnClear.Image = ((System.Drawing.Image)(resources.GetObject("sbnClear.Image")));
            this.sbnClear.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.sbnClear.Location = new System.Drawing.Point(455, 123);
            this.sbnClear.Name = "sbnClear";
            this.sbnClear.Size = new System.Drawing.Size(70, 62);
            this.sbnClear.TabIndex = 2;
            this.sbnClear.Text = "Clear";
            // 
            // sbnRegister
            // 
            this.sbnRegister.Image = ((System.Drawing.Image)(resources.GetObject("sbnRegister.Image")));
            this.sbnRegister.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.sbnRegister.Location = new System.Drawing.Point(454, 55);
            this.sbnRegister.Name = "sbnRegister";
            this.sbnRegister.Size = new System.Drawing.Size(70, 62);
            this.sbnRegister.TabIndex = 1;
            this.sbnRegister.Text = "Register";
            this.sbnRegister.Click += new System.EventHandler(this.sbnRegister_Click);
            // 
            // gpcLubricantList
            // 
            this.gpcLubricantList.Controls.Add(this.gcLubricantList);
            this.gpcLubricantList.Location = new System.Drawing.Point(12, 313);
            this.gpcLubricantList.Name = "gpcLubricantList";
            this.gpcLubricantList.Size = new System.Drawing.Size(677, 210);
            this.gpcLubricantList.TabIndex = 3;
            this.gpcLubricantList.Text = "List of Tires";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(135, 133);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(261, 68);
            this.txtDescription.TabIndex = 9;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(18, 133);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(53, 13);
            this.labelControl9.TabIndex = 8;
            this.labelControl9.Text = "Description";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(18, 109);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(63, 13);
            this.labelControl7.TabIndex = 6;
            this.labelControl7.Text = "Current Price";
            // 
            // txtCurrentPrice
            // 
            this.txtCurrentPrice.Location = new System.Drawing.Point(135, 107);
            this.txtCurrentPrice.MenuManager = this.ribbonControl1;
            this.txtCurrentPrice.Name = "txtCurrentPrice";
            this.txtCurrentPrice.Properties.Mask.BeepOnError = true;
            this.txtCurrentPrice.Properties.Mask.EditMask = "c";
            this.txtCurrentPrice.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCurrentPrice.Size = new System.Drawing.Size(145, 20);
            this.txtCurrentPrice.TabIndex = 7;
            // 
            // Type
            // 
            this.Type.Location = new System.Drawing.Point(18, 62);
            this.Type.Name = "Type";
            this.Type.Size = new System.Drawing.Size(74, 13);
            this.Type.TabIndex = 2;
            this.Type.Text = "Lubricant Name";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(18, 85);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(71, 13);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Lubricant Type";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(18, 40);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(58, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Lubricant ID";
            // 
            // txtLubricantId
            // 
            this.txtLubricantId.Enabled = false;
            this.txtLubricantId.Location = new System.Drawing.Point(135, 37);
            this.txtLubricantId.MenuManager = this.ribbonControl1;
            this.txtLubricantId.Name = "txtLubricantId";
            this.txtLubricantId.Size = new System.Drawing.Size(145, 20);
            this.txtLubricantId.TabIndex = 1;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.icbLubricantType);
            this.groupControl1.Controls.Add(this.txtDescription);
            this.groupControl1.Controls.Add(this.labelControl9);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.txtCurrentPrice);
            this.groupControl1.Controls.Add(this.Type);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.txtLubricantId);
            this.groupControl1.Controls.Add(this.txtLubricantName);
            this.groupControl1.Location = new System.Drawing.Point(12, 55);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(422, 217);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Tire Information";
            // 
            // txtLubricantName
            // 
            this.txtLubricantName.Location = new System.Drawing.Point(135, 60);
            this.txtLubricantName.MenuManager = this.ribbonControl1;
            this.txtLubricantName.Name = "txtLubricantName";
            this.txtLubricantName.Size = new System.Drawing.Size(145, 20);
            this.txtLubricantName.TabIndex = 3;
            // 
            // RegisterLubricant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(706, 536);
            this.Controls.Add(this.sbnClear);
            this.Controls.Add(this.sbnRegister);
            this.Controls.Add(this.gpcLubricantList);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.ribbonControl1);
            this.Name = "RegisterLubricant";
            this.Ribbon = this.ribbonControl1;
            this.Text = "RegisterLubricant";
            this.Load += new System.EventHandler(this.RegisterLubricant_Load);
            this.Resize += new System.EventHandler(this.RegisterLubricant_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.icbLubricantType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLubricantList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcLubricantList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpcLubricantList)).EndInit();
            this.gpcLubricantList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrentPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLubricantId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLubricantName.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.ImageComboBoxEdit icbLubricantType;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gvLubricantList;
        private DevExpress.XtraGrid.GridControl gcLubricantList;
        private DevExpress.XtraEditors.SimpleButton sbnClear;
        private DevExpress.XtraEditors.SimpleButton sbnRegister;
        private DevExpress.XtraEditors.GroupControl gpcLubricantList;
        private System.Windows.Forms.TextBox txtDescription;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtCurrentPrice;
        private DevExpress.XtraEditors.LabelControl Type;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtLubricantId;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.TextEdit txtLubricantName;
    }
}