﻿namespace FleetFuelMS
{
    partial class RegisterDriver
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegisterDriver));
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.icGender = new DevExpress.Utils.ImageCollection(this.components);
            this.pccLicense = new DevExpress.XtraBars.PopupControlContainer(this.components);
            this.lbcLicenseInfo = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.pcbPhoto = new System.Windows.Forms.PictureBox();
            this.sbnBrowse = new DevExpress.XtraEditors.SimpleButton();
            this.txtPhoneNo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.sbnLicenseInfo = new DevExpress.XtraEditors.SimpleButton();
            this.sbnVehicleInfo = new DevExpress.XtraEditors.SimpleButton();
            this.dteBirthDate = new DevExpress.XtraEditors.DateEdit();
            this.txtMiddleName = new DevExpress.XtraEditors.TextEdit();
            this.Type = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.icbVehicle = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.icbLicense = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtLastName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtDriverId = new DevExpress.XtraEditors.TextEdit();
            this.icbGender = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.txtFirstName = new DevExpress.XtraEditors.TextEdit();
            this.popupControlContainer1 = new DevExpress.XtraBars.PopupControlContainer(this.components);
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.sbnNewGuarantor = new DevExpress.XtraEditors.SimpleButton();
            this.sbnAddGuarantor = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.icbGuarantor = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.gcGuarantors = new DevExpress.XtraGrid.GridControl();
            this.gvGuarantors = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.FullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Gender = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Organization = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Reference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sbnRegister = new DevExpress.XtraEditors.SimpleButton();
            this.sbnClear = new DevExpress.XtraEditors.SimpleButton();
            this.gpcDriverList = new DevExpress.XtraEditors.GroupControl();
            this.gcDriverList = new DevExpress.XtraGrid.GridControl();
            this.gvDriverList = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ofdImage = new System.Windows.Forms.OpenFileDialog();
            this.pccVehicleInfo = new DevExpress.XtraBars.PopupControlContainer(this.components);
            this.lbcMake = new DevExpress.XtraEditors.LabelControl();
            this.lbcDriver = new DevExpress.XtraEditors.LabelControl();
            this.lbcFuel = new DevExpress.XtraEditors.LabelControl();
            this.lbcModel = new DevExpress.XtraEditors.LabelControl();
            this.lbcPlateNo = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.pceVehicleImage = new DevExpress.XtraEditors.PictureEdit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icGender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pccLicense)).BeginInit();
            this.pccLicense.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbPhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhoneNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteBirthDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteBirthDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMiddleName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbVehicle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbLicense.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDriverId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbGender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupControlContainer1)).BeginInit();
            this.popupControlContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icbGuarantor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcGuarantors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvGuarantors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpcDriverList)).BeginInit();
            this.gpcDriverList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcDriverList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDriverList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pccVehicleInfo)).BeginInit();
            this.pccVehicleInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pceVehicleImage.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 1;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Size = new System.Drawing.Size(994, 49);
            // 
            // icGender
            // 
            this.icGender.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("icGender.ImageStream")));
            this.icGender.InsertGalleryImage("employee_16x16.png", "office2013/people/employee_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("office2013/people/employee_16x16.png"), 0);
            this.icGender.Images.SetKeyName(0, "employee_16x16.png");
            this.icGender.InsertGalleryImage("miss_16x16.png", "devav/people/miss_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("devav/people/miss_16x16.png"), 1);
            this.icGender.Images.SetKeyName(1, "miss_16x16.png");
            // 
            // pccLicense
            // 
            this.pccLicense.AutoSize = true;
            this.pccLicense.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pccLicense.Controls.Add(this.lbcLicenseInfo);
            this.pccLicense.Location = new System.Drawing.Point(883, 187);
            this.pccLicense.Name = "pccLicense";
            this.pccLicense.Ribbon = this.ribbonControl1;
            this.pccLicense.ShowCloseButton = true;
            this.pccLicense.Size = new System.Drawing.Size(150, 105);
            this.pccLicense.TabIndex = 6;
            this.pccLicense.Visible = false;
            // 
            // lbcLicenseInfo
            // 
            this.lbcLicenseInfo.Location = new System.Drawing.Point(3, 3);
            this.lbcLicenseInfo.Name = "lbcLicenseInfo";
            this.lbcLicenseInfo.Size = new System.Drawing.Size(69, 13);
            this.lbcLicenseInfo.TabIndex = 0;
            this.lbcLicenseInfo.Text = "labelControl14";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.pcbPhoto);
            this.groupControl2.Controls.Add(this.sbnBrowse);
            this.groupControl2.Controls.Add(this.txtPhoneNo);
            this.groupControl2.Controls.Add(this.labelControl14);
            this.groupControl2.Controls.Add(this.sbnLicenseInfo);
            this.groupControl2.Controls.Add(this.sbnVehicleInfo);
            this.groupControl2.Controls.Add(this.dteBirthDate);
            this.groupControl2.Controls.Add(this.txtMiddleName);
            this.groupControl2.Controls.Add(this.Type);
            this.groupControl2.Controls.Add(this.labelControl8);
            this.groupControl2.Controls.Add(this.icbVehicle);
            this.groupControl2.Controls.Add(this.icbLicense);
            this.groupControl2.Controls.Add(this.labelControl4);
            this.groupControl2.Controls.Add(this.labelControl5);
            this.groupControl2.Controls.Add(this.labelControl6);
            this.groupControl2.Controls.Add(this.labelControl3);
            this.groupControl2.Controls.Add(this.txtLastName);
            this.groupControl2.Controls.Add(this.labelControl2);
            this.groupControl2.Controls.Add(this.labelControl1);
            this.groupControl2.Controls.Add(this.txtDriverId);
            this.groupControl2.Controls.Add(this.icbGender);
            this.groupControl2.Controls.Add(this.txtFirstName);
            this.groupControl2.Controls.Add(this.popupControlContainer1);
            this.groupControl2.Location = new System.Drawing.Point(12, 49);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(369, 268);
            this.groupControl2.TabIndex = 0;
            this.groupControl2.Text = "Driver Information";
            // 
            // pcbPhoto
            // 
            this.pcbPhoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pcbPhoto.Location = new System.Drawing.Point(213, 27);
            this.pcbPhoto.Name = "pcbPhoto";
            this.pcbPhoto.Size = new System.Drawing.Size(123, 134);
            this.pcbPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbPhoto.TabIndex = 66;
            this.pcbPhoto.TabStop = false;
            this.pcbPhoto.MouseLeave += new System.EventHandler(this.pcbPhoto_MouseLeave);
            this.pcbPhoto.MouseHover += new System.EventHandler(this.pcbPhoto_MouseHover);
            // 
            // sbnBrowse
            // 
            this.sbnBrowse.Location = new System.Drawing.Point(121, 140);
            this.sbnBrowse.Name = "sbnBrowse";
            this.sbnBrowse.Size = new System.Drawing.Size(86, 21);
            this.sbnBrowse.TabIndex = 10;
            this.sbnBrowse.Text = "Browse ...";
            this.sbnBrowse.Click += new System.EventHandler(this.sbnBrowse_Click);
            // 
            // txtPhoneNo
            // 
            this.txtPhoneNo.Location = new System.Drawing.Point(121, 187);
            this.txtPhoneNo.MenuManager = this.ribbonControl1;
            this.txtPhoneNo.Name = "txtPhoneNo";
            this.txtPhoneNo.Size = new System.Drawing.Size(215, 20);
            this.txtPhoneNo.TabIndex = 15;
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(23, 190);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(70, 13);
            this.labelControl14.TabIndex = 14;
            this.labelControl14.Text = "Phone Number";
            // 
            // sbnLicenseInfo
            // 
            this.sbnLicenseInfo.Image = ((System.Drawing.Image)(resources.GetObject("sbnLicenseInfo.Image")));
            this.sbnLicenseInfo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbnLicenseInfo.Location = new System.Drawing.Point(308, 210);
            this.sbnLicenseInfo.Name = "sbnLicenseInfo";
            this.sbnLicenseInfo.Size = new System.Drawing.Size(28, 20);
            this.sbnLicenseInfo.TabIndex = 18;
            this.sbnLicenseInfo.Click += new System.EventHandler(this.sbnLicenseInfo_Click);
            // 
            // sbnVehicleInfo
            // 
            this.sbnVehicleInfo.Image = ((System.Drawing.Image)(resources.GetObject("sbnVehicleInfo.Image")));
            this.sbnVehicleInfo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbnVehicleInfo.Location = new System.Drawing.Point(308, 232);
            this.sbnVehicleInfo.Name = "sbnVehicleInfo";
            this.sbnVehicleInfo.Size = new System.Drawing.Size(28, 20);
            this.sbnVehicleInfo.TabIndex = 21;
            this.sbnVehicleInfo.Click += new System.EventHandler(this.sbnVehicleInfo_Click);
            // 
            // dteBirthDate
            // 
            this.dteBirthDate.EditValue = null;
            this.dteBirthDate.Location = new System.Drawing.Point(121, 164);
            this.dteBirthDate.MenuManager = this.ribbonControl1;
            this.dteBirthDate.Name = "dteBirthDate";
            this.dteBirthDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteBirthDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteBirthDate.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.Classic;
            this.dteBirthDate.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.dteBirthDate.Size = new System.Drawing.Size(215, 20);
            this.dteBirthDate.TabIndex = 13;
            // 
            // txtMiddleName
            // 
            this.txtMiddleName.Location = new System.Drawing.Point(121, 73);
            this.txtMiddleName.MenuManager = this.ribbonControl1;
            this.txtMiddleName.Name = "txtMiddleName";
            this.txtMiddleName.Size = new System.Drawing.Size(86, 20);
            this.txtMiddleName.TabIndex = 5;
            // 
            // Type
            // 
            this.Type.Location = new System.Drawing.Point(24, 53);
            this.Type.Name = "Type";
            this.Type.Size = new System.Drawing.Size(51, 13);
            this.Type.TabIndex = 2;
            this.Type.Text = "First Name";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(23, 122);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(35, 13);
            this.labelControl8.TabIndex = 8;
            this.labelControl8.Text = "Gender";
            // 
            // icbVehicle
            // 
            this.icbVehicle.Location = new System.Drawing.Point(121, 232);
            this.icbVehicle.MenuManager = this.ribbonControl1;
            this.icbVehicle.Name = "icbVehicle";
            this.icbVehicle.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbVehicle.Size = new System.Drawing.Size(181, 20);
            this.icbVehicle.TabIndex = 20;
            // 
            // icbLicense
            // 
            this.icbLicense.Location = new System.Drawing.Point(121, 210);
            this.icbLicense.MenuManager = this.ribbonControl1;
            this.icbLicense.Name = "icbLicense";
            this.icbLicense.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbLicense.Size = new System.Drawing.Size(181, 20);
            this.icbLicense.TabIndex = 17;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(23, 235);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(87, 13);
            this.labelControl4.TabIndex = 19;
            this.labelControl4.Text = "Vehicle (Assigned)";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(23, 213);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(35, 13);
            this.labelControl5.TabIndex = 16;
            this.labelControl5.Text = "License";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(23, 167);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(48, 13);
            this.labelControl6.TabIndex = 12;
            this.labelControl6.Text = "Birth Date";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(23, 99);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(50, 13);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Last Name";
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(121, 96);
            this.txtLastName.MenuManager = this.ribbonControl1;
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(86, 20);
            this.txtLastName.TabIndex = 7;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(24, 76);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 13);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Middle Name";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(24, 31);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(43, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Driver ID";
            // 
            // txtDriverId
            // 
            this.txtDriverId.Enabled = false;
            this.txtDriverId.Location = new System.Drawing.Point(121, 27);
            this.txtDriverId.MenuManager = this.ribbonControl1;
            this.txtDriverId.Name = "txtDriverId";
            this.txtDriverId.Size = new System.Drawing.Size(86, 20);
            this.txtDriverId.TabIndex = 1;
            // 
            // icbGender
            // 
            this.icbGender.Location = new System.Drawing.Point(121, 119);
            this.icbGender.MenuManager = this.ribbonControl1;
            this.icbGender.Name = "icbGender";
            this.icbGender.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbGender.Properties.SmallImages = this.icGender;
            this.icbGender.Size = new System.Drawing.Size(86, 20);
            this.icbGender.TabIndex = 9;
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(121, 50);
            this.txtFirstName.MenuManager = this.ribbonControl1;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(86, 20);
            this.txtFirstName.TabIndex = 3;
            // 
            // popupControlContainer1
            // 
            this.popupControlContainer1.AutoSize = true;
            this.popupControlContainer1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.popupControlContainer1.Controls.Add(this.labelControl16);
            this.popupControlContainer1.Controls.Add(this.labelControl17);
            this.popupControlContainer1.Controls.Add(this.labelControl18);
            this.popupControlContainer1.Controls.Add(this.labelControl19);
            this.popupControlContainer1.Controls.Add(this.labelControl20);
            this.popupControlContainer1.Controls.Add(this.labelControl21);
            this.popupControlContainer1.Controls.Add(this.labelControl22);
            this.popupControlContainer1.Controls.Add(this.labelControl23);
            this.popupControlContainer1.Controls.Add(this.labelControl24);
            this.popupControlContainer1.Controls.Add(this.labelControl25);
            this.popupControlContainer1.Controls.Add(this.pictureEdit1);
            this.popupControlContainer1.Location = new System.Drawing.Point(370, 120);
            this.popupControlContainer1.Name = "popupControlContainer1";
            this.popupControlContainer1.Ribbon = this.ribbonControl1;
            this.popupControlContainer1.ShowCloseButton = true;
            this.popupControlContainer1.Size = new System.Drawing.Size(326, 128);
            this.popupControlContainer1.TabIndex = 41;
            this.popupControlContainer1.Visible = false;
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(177, 33);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(25, 13);
            this.labelControl16.TabIndex = 4;
            this.labelControl16.Text = "Make";
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(176, 102);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(29, 13);
            this.labelControl17.TabIndex = 10;
            this.labelControl17.Text = "Driver";
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(176, 79);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(20, 13);
            this.labelControl18.TabIndex = 8;
            this.labelControl18.Text = "Fuel";
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(177, 56);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(28, 13);
            this.labelControl19.TabIndex = 6;
            this.labelControl19.Text = "Model";
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(177, 11);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(35, 13);
            this.labelControl20.TabIndex = 2;
            this.labelControl20.Text = "Plate #";
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl21.Location = new System.Drawing.Point(134, 33);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(31, 13);
            this.labelControl21.TabIndex = 3;
            this.labelControl21.Text = "Make";
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl22.Location = new System.Drawing.Point(130, 102);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(35, 13);
            this.labelControl22.TabIndex = 9;
            this.labelControl22.Text = "Driver";
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl23.Location = new System.Drawing.Point(139, 79);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(23, 13);
            this.labelControl23.TabIndex = 7;
            this.labelControl23.Text = "Fuel";
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl24.Location = new System.Drawing.Point(131, 56);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(34, 13);
            this.labelControl24.TabIndex = 5;
            this.labelControl24.Text = "Model";
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl25.Location = new System.Drawing.Point(124, 11);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(41, 13);
            this.labelControl25.TabIndex = 1;
            this.labelControl25.Text = "Plate #";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Location = new System.Drawing.Point(7, 9);
            this.pictureEdit1.MenuManager = this.ribbonControl1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.pictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit1.Size = new System.Drawing.Size(111, 106);
            this.pictureEdit1.TabIndex = 0;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.sbnNewGuarantor);
            this.groupControl1.Controls.Add(this.sbnAddGuarantor);
            this.groupControl1.Controls.Add(this.simpleButton6);
            this.groupControl1.Controls.Add(this.labelControl13);
            this.groupControl1.Controls.Add(this.icbGuarantor);
            this.groupControl1.Controls.Add(this.simpleButton4);
            this.groupControl1.Controls.Add(this.simpleButton3);
            this.groupControl1.Controls.Add(this.gcGuarantors);
            this.groupControl1.Location = new System.Drawing.Point(396, 49);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(487, 268);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "List of Guarantors";
            // 
            // sbnNewGuarantor
            // 
            this.sbnNewGuarantor.Image = ((System.Drawing.Image)(resources.GetObject("sbnNewGuarantor.Image")));
            this.sbnNewGuarantor.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.sbnNewGuarantor.Location = new System.Drawing.Point(128, 27);
            this.sbnNewGuarantor.Name = "sbnNewGuarantor";
            this.sbnNewGuarantor.Size = new System.Drawing.Size(112, 20);
            this.sbnNewGuarantor.TabIndex = 27;
            this.sbnNewGuarantor.Text = "New Guarantor";
            this.sbnNewGuarantor.Click += new System.EventHandler(this.sbnNewGuarantor_Click);
            // 
            // sbnAddGuarantor
            // 
            this.sbnAddGuarantor.Image = ((System.Drawing.Image)(resources.GetObject("sbnAddGuarantor.Image")));
            this.sbnAddGuarantor.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbnAddGuarantor.Location = new System.Drawing.Point(331, 50);
            this.sbnAddGuarantor.Name = "sbnAddGuarantor";
            this.sbnAddGuarantor.Size = new System.Drawing.Size(28, 20);
            this.sbnAddGuarantor.TabIndex = 30;
            this.sbnAddGuarantor.Click += new System.EventHandler(this.sbnAddGuarantor_Click);
            // 
            // simpleButton6
            // 
            this.simpleButton6.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton6.Image")));
            this.simpleButton6.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton6.Location = new System.Drawing.Point(365, 50);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(28, 20);
            this.simpleButton6.TabIndex = 31;
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(33, 53);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(62, 13);
            this.labelControl13.TabIndex = 28;
            this.labelControl13.Text = "Guarantor(s)";
            // 
            // icbGuarantor
            // 
            this.icbGuarantor.Location = new System.Drawing.Point(128, 50);
            this.icbGuarantor.MenuManager = this.ribbonControl1;
            this.icbGuarantor.Name = "icbGuarantor";
            this.icbGuarantor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbGuarantor.Size = new System.Drawing.Size(193, 20);
            this.icbGuarantor.TabIndex = 29;
            // 
            // simpleButton4
            // 
            this.simpleButton4.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton4.Image")));
            this.simpleButton4.Location = new System.Drawing.Point(4, 102);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(26, 22);
            this.simpleButton4.TabIndex = 1;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton3.Image")));
            this.simpleButton3.Location = new System.Drawing.Point(4, 74);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(26, 22);
            this.simpleButton3.TabIndex = 0;
            // 
            // gcGuarantors
            // 
            this.gcGuarantors.Location = new System.Drawing.Point(33, 74);
            this.gcGuarantors.MainView = this.gvGuarantors;
            this.gcGuarantors.MenuManager = this.ribbonControl1;
            this.gcGuarantors.Name = "gcGuarantors";
            this.gcGuarantors.Size = new System.Drawing.Size(449, 178);
            this.gcGuarantors.TabIndex = 2;
            this.gcGuarantors.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvGuarantors});
            // 
            // gvGuarantors
            // 
            this.gvGuarantors.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gvGuarantors.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvGuarantors.AppearancePrint.HeaderPanel.BackColor = System.Drawing.Color.Black;
            this.gvGuarantors.AppearancePrint.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gvGuarantors.AppearancePrint.HeaderPanel.ForeColor = System.Drawing.Color.White;
            this.gvGuarantors.AppearancePrint.HeaderPanel.Options.UseBackColor = true;
            this.gvGuarantors.AppearancePrint.HeaderPanel.Options.UseFont = true;
            this.gvGuarantors.AppearancePrint.HeaderPanel.Options.UseForeColor = true;
            this.gvGuarantors.AppearancePrint.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.gvGuarantors.AppearancePrint.OddRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.gvGuarantors.AppearancePrint.OddRow.Options.UseBackColor = true;
            this.gvGuarantors.ColumnPanelRowHeight = 30;
            this.gvGuarantors.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.FullName,
            this.Gender,
            this.Organization,
            this.GID,
            this.Reference});
            this.gvGuarantors.GridControl = this.gcGuarantors;
            this.gvGuarantors.Name = "gvGuarantors";
            this.gvGuarantors.OptionsView.EnableAppearanceEvenRow = true;
            this.gvGuarantors.OptionsView.EnableAppearanceOddRow = true;
            this.gvGuarantors.OptionsView.ShowGroupPanel = false;
            // 
            // FullName
            // 
            this.FullName.Caption = "Full Name";
            this.FullName.FieldName = "FullName";
            this.FullName.Name = "FullName";
            this.FullName.Visible = true;
            this.FullName.VisibleIndex = 1;
            this.FullName.Width = 127;
            // 
            // Gender
            // 
            this.Gender.Caption = "Gender";
            this.Gender.FieldName = "Gender";
            this.Gender.Name = "Gender";
            this.Gender.Visible = true;
            this.Gender.VisibleIndex = 2;
            this.Gender.Width = 51;
            // 
            // Organization
            // 
            this.Organization.Caption = "Organization";
            this.Organization.FieldName = "Organization";
            this.Organization.Name = "Organization";
            this.Organization.Visible = true;
            this.Organization.VisibleIndex = 3;
            this.Organization.Width = 94;
            // 
            // GID
            // 
            this.GID.Caption = "ID";
            this.GID.FieldName = "ID";
            this.GID.Name = "GID";
            this.GID.Visible = true;
            this.GID.VisibleIndex = 0;
            this.GID.Width = 49;
            // 
            // Reference
            // 
            this.Reference.Caption = "Reference";
            this.Reference.FieldName = "Reference";
            this.Reference.Name = "Reference";
            this.Reference.Visible = true;
            this.Reference.VisibleIndex = 4;
            this.Reference.Width = 110;
            // 
            // sbnRegister
            // 
            this.sbnRegister.Image = ((System.Drawing.Image)(resources.GetObject("sbnRegister.Image")));
            this.sbnRegister.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.sbnRegister.Location = new System.Drawing.Point(898, 49);
            this.sbnRegister.Name = "sbnRegister";
            this.sbnRegister.Size = new System.Drawing.Size(70, 62);
            this.sbnRegister.TabIndex = 2;
            this.sbnRegister.Text = "Register";
            this.sbnRegister.Click += new System.EventHandler(this.sbnRegister_Click);
            // 
            // sbnClear
            // 
            this.sbnClear.Image = ((System.Drawing.Image)(resources.GetObject("sbnClear.Image")));
            this.sbnClear.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.sbnClear.Location = new System.Drawing.Point(899, 117);
            this.sbnClear.Name = "sbnClear";
            this.sbnClear.Size = new System.Drawing.Size(70, 62);
            this.sbnClear.TabIndex = 3;
            this.sbnClear.Text = "Clear";
            // 
            // gpcDriverList
            // 
            this.gpcDriverList.Controls.Add(this.gcDriverList);
            this.gpcDriverList.Location = new System.Drawing.Point(12, 345);
            this.gpcDriverList.Name = "gpcDriverList";
            this.gpcDriverList.Size = new System.Drawing.Size(871, 188);
            this.gpcDriverList.TabIndex = 4;
            this.gpcDriverList.Text = "List of Drivers";
            // 
            // gcDriverList
            // 
            this.gcDriverList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcDriverList.Location = new System.Drawing.Point(2, 20);
            this.gcDriverList.MainView = this.gvDriverList;
            this.gcDriverList.MenuManager = this.ribbonControl1;
            this.gcDriverList.Name = "gcDriverList";
            this.gcDriverList.Size = new System.Drawing.Size(867, 166);
            this.gcDriverList.TabIndex = 0;
            this.gcDriverList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvDriverList});
            // 
            // gvDriverList
            // 
            this.gvDriverList.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.gvDriverList.Appearance.OddRow.Options.UseBackColor = true;
            this.gvDriverList.GridControl = this.gcDriverList;
            this.gvDriverList.Name = "gvDriverList";
            // 
            // pccVehicleInfo
            // 
            this.pccVehicleInfo.AutoSize = true;
            this.pccVehicleInfo.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.pccVehicleInfo.Controls.Add(this.lbcMake);
            this.pccVehicleInfo.Controls.Add(this.lbcDriver);
            this.pccVehicleInfo.Controls.Add(this.lbcFuel);
            this.pccVehicleInfo.Controls.Add(this.lbcModel);
            this.pccVehicleInfo.Controls.Add(this.lbcPlateNo);
            this.pccVehicleInfo.Controls.Add(this.labelControl7);
            this.pccVehicleInfo.Controls.Add(this.labelControl9);
            this.pccVehicleInfo.Controls.Add(this.labelControl10);
            this.pccVehicleInfo.Controls.Add(this.labelControl11);
            this.pccVehicleInfo.Controls.Add(this.labelControl12);
            this.pccVehicleInfo.Controls.Add(this.pceVehicleImage);
            this.pccVehicleInfo.Location = new System.Drawing.Point(883, 298);
            this.pccVehicleInfo.Name = "pccVehicleInfo";
            this.pccVehicleInfo.Ribbon = this.ribbonControl1;
            this.pccVehicleInfo.ShowCloseButton = true;
            this.pccVehicleInfo.Size = new System.Drawing.Size(326, 128);
            this.pccVehicleInfo.TabIndex = 12;
            this.pccVehicleInfo.Visible = false;
            // 
            // lbcMake
            // 
            this.lbcMake.Location = new System.Drawing.Point(177, 33);
            this.lbcMake.Name = "lbcMake";
            this.lbcMake.Size = new System.Drawing.Size(25, 13);
            this.lbcMake.TabIndex = 4;
            this.lbcMake.Text = "Make";
            // 
            // lbcDriver
            // 
            this.lbcDriver.Location = new System.Drawing.Point(176, 102);
            this.lbcDriver.Name = "lbcDriver";
            this.lbcDriver.Size = new System.Drawing.Size(29, 13);
            this.lbcDriver.TabIndex = 10;
            this.lbcDriver.Text = "Driver";
            // 
            // lbcFuel
            // 
            this.lbcFuel.Location = new System.Drawing.Point(176, 79);
            this.lbcFuel.Name = "lbcFuel";
            this.lbcFuel.Size = new System.Drawing.Size(20, 13);
            this.lbcFuel.TabIndex = 8;
            this.lbcFuel.Text = "Fuel";
            // 
            // lbcModel
            // 
            this.lbcModel.Location = new System.Drawing.Point(177, 56);
            this.lbcModel.Name = "lbcModel";
            this.lbcModel.Size = new System.Drawing.Size(28, 13);
            this.lbcModel.TabIndex = 6;
            this.lbcModel.Text = "Model";
            // 
            // lbcPlateNo
            // 
            this.lbcPlateNo.Location = new System.Drawing.Point(177, 11);
            this.lbcPlateNo.Name = "lbcPlateNo";
            this.lbcPlateNo.Size = new System.Drawing.Size(35, 13);
            this.lbcPlateNo.TabIndex = 2;
            this.lbcPlateNo.Text = "Plate #";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl7.Location = new System.Drawing.Point(134, 33);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(31, 13);
            this.labelControl7.TabIndex = 3;
            this.labelControl7.Text = "Make";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl9.Location = new System.Drawing.Point(130, 102);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(35, 13);
            this.labelControl9.TabIndex = 9;
            this.labelControl9.Text = "Driver";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl10.Location = new System.Drawing.Point(139, 79);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(23, 13);
            this.labelControl10.TabIndex = 7;
            this.labelControl10.Text = "Fuel";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl11.Location = new System.Drawing.Point(131, 56);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(34, 13);
            this.labelControl11.TabIndex = 5;
            this.labelControl11.Text = "Model";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl12.Location = new System.Drawing.Point(124, 11);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(41, 13);
            this.labelControl12.TabIndex = 1;
            this.labelControl12.Text = "Plate #";
            // 
            // pceVehicleImage
            // 
            this.pceVehicleImage.Location = new System.Drawing.Point(7, 9);
            this.pceVehicleImage.MenuManager = this.ribbonControl1;
            this.pceVehicleImage.Name = "pceVehicleImage";
            this.pceVehicleImage.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.pceVehicleImage.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pceVehicleImage.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pceVehicleImage.Size = new System.Drawing.Size(111, 106);
            this.pceVehicleImage.TabIndex = 0;
            // 
            // RegisterDriver
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 545);
            this.Controls.Add(this.pccVehicleInfo);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.sbnClear);
            this.Controls.Add(this.sbnRegister);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.pccLicense);
            this.Controls.Add(this.gpcDriverList);
            this.Controls.Add(this.ribbonControl1);
            this.Name = "RegisterDriver";
            this.Ribbon = this.ribbonControl1;
            this.Text = "Register Driver";
            this.Load += new System.EventHandler(this.RegisterDriver_Load);
            this.Resize += new System.EventHandler(this.RegisterDriver_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icGender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pccLicense)).EndInit();
            this.pccLicense.ResumeLayout(false);
            this.pccLicense.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbPhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhoneNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteBirthDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteBirthDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMiddleName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbVehicle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbLicense.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDriverId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbGender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupControlContainer1)).EndInit();
            this.popupControlContainer1.ResumeLayout(false);
            this.popupControlContainer1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icbGuarantor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcGuarantors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvGuarantors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpcDriverList)).EndInit();
            this.gpcDriverList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcDriverList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDriverList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pccVehicleInfo)).EndInit();
            this.pccVehicleInfo.ResumeLayout(false);
            this.pccVehicleInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pceVehicleImage.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.Utils.ImageCollection icGender;
        private DevExpress.XtraBars.PopupControlContainer pccLicense;
        private DevExpress.XtraEditors.LabelControl lbcLicenseInfo;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SimpleButton sbnBrowse;
        private System.Windows.Forms.PictureBox pcbPhoto;
        private DevExpress.XtraEditors.TextEdit txtPhoneNo;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.SimpleButton sbnLicenseInfo;
        private DevExpress.XtraEditors.SimpleButton sbnVehicleInfo;
        private DevExpress.XtraEditors.DateEdit dteBirthDate;
        private DevExpress.XtraEditors.TextEdit txtMiddleName;
        private DevExpress.XtraEditors.LabelControl Type;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.ImageComboBoxEdit icbVehicle;
        private DevExpress.XtraEditors.ImageComboBoxEdit icbLicense;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtLastName;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtDriverId;
        private DevExpress.XtraEditors.ImageComboBoxEdit icbGender;
        private DevExpress.XtraEditors.TextEdit txtFirstName;
        private DevExpress.XtraBars.PopupControlContainer popupControlContainer1;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl gcGuarantors;
        private DevExpress.XtraGrid.Views.Grid.GridView gvGuarantors;
        private DevExpress.XtraGrid.Columns.GridColumn FullName;
        private DevExpress.XtraGrid.Columns.GridColumn Gender;
        private DevExpress.XtraGrid.Columns.GridColumn Organization;
        private DevExpress.XtraGrid.Columns.GridColumn GID;
        private DevExpress.XtraGrid.Columns.GridColumn Reference;
        private DevExpress.XtraEditors.SimpleButton sbnRegister;
        private DevExpress.XtraEditors.SimpleButton sbnClear;
        private DevExpress.XtraEditors.GroupControl gpcDriverList;
        private DevExpress.XtraGrid.GridControl gcDriverList;
        private DevExpress.XtraGrid.Views.Grid.GridView gvDriverList;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton sbnNewGuarantor;
        private DevExpress.XtraEditors.SimpleButton sbnAddGuarantor;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.ImageComboBoxEdit icbGuarantor;
        private System.Windows.Forms.OpenFileDialog ofdImage;
        private DevExpress.XtraBars.PopupControlContainer pccVehicleInfo;
        private DevExpress.XtraEditors.LabelControl lbcMake;
        private DevExpress.XtraEditors.LabelControl lbcDriver;
        private DevExpress.XtraEditors.LabelControl lbcFuel;
        private DevExpress.XtraEditors.LabelControl lbcModel;
        private DevExpress.XtraEditors.LabelControl lbcPlateNo;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.PictureEdit pceVehicleImage;
    }
}