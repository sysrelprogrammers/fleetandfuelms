﻿namespace FleetFuelMS
{
    partial class RegisterGuarantor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegisterGuarantor));
            this.spcDriverList = new System.Windows.Forms.SplitContainer();
            this.gcDriverList = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.sbnRegisterGuarantor = new DevExpress.XtraEditors.SimpleButton();
            this.sbnBrowsePhoto = new DevExpress.XtraEditors.SimpleButton();
            this.pcbGurantor = new System.Windows.Forms.PictureBox();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtPhoneNo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.icbGender = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.txtFirstName = new DevExpress.XtraEditors.TextEdit();
            this.dteBirthDate = new DevExpress.XtraEditors.DateEdit();
            this.txtMiddleName = new DevExpress.XtraEditors.TextEdit();
            this.Type = new DevExpress.XtraEditors.LabelControl();
            this.sbnClear = new DevExpress.XtraEditors.SimpleButton();
            this.sbnRegister = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtLastName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtGuarantorId = new DevExpress.XtraEditors.TextEdit();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.ofdGPhoto = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.spcDriverList)).BeginInit();
            this.spcDriverList.Panel2.SuspendLayout();
            this.spcDriverList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcDriverList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbGurantor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhoneNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbGender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteBirthDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteBirthDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMiddleName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGuarantorId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // spcDriverList
            // 
            this.spcDriverList.Location = new System.Drawing.Point(12, 362);
            this.spcDriverList.Name = "spcDriverList";
            this.spcDriverList.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spcDriverList.Panel2
            // 
            this.spcDriverList.Panel2.Controls.Add(this.gcDriverList);
            this.spcDriverList.Size = new System.Drawing.Size(808, 194);
            this.spcDriverList.SplitterDistance = 32;
            this.spcDriverList.TabIndex = 3;
            // 
            // gcDriverList
            // 
            this.gcDriverList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcDriverList.Location = new System.Drawing.Point(0, 0);
            this.gcDriverList.MainView = this.gridView1;
            this.gcDriverList.Name = "gcDriverList";
            this.gcDriverList.Size = new System.Drawing.Size(808, 158);
            this.gcDriverList.TabIndex = 0;
            this.gcDriverList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gcDriverList;
            this.gridView1.Name = "gridView1";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Location = new System.Drawing.Point(15, 55);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.labelControl15);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.simpleButton1);
            this.splitContainer2.Panel2.Controls.Add(this.sbnRegisterGuarantor);
            this.splitContainer2.Panel2.Controls.Add(this.sbnBrowsePhoto);
            this.splitContainer2.Panel2.Controls.Add(this.pcbGurantor);
            this.splitContainer2.Panel2.Controls.Add(this.labelControl4);
            this.splitContainer2.Panel2.Controls.Add(this.txtPhoneNo);
            this.splitContainer2.Panel2.Controls.Add(this.labelControl14);
            this.splitContainer2.Panel2.Controls.Add(this.icbGender);
            this.splitContainer2.Panel2.Controls.Add(this.txtFirstName);
            this.splitContainer2.Panel2.Controls.Add(this.dteBirthDate);
            this.splitContainer2.Panel2.Controls.Add(this.txtMiddleName);
            this.splitContainer2.Panel2.Controls.Add(this.Type);
            this.splitContainer2.Panel2.Controls.Add(this.sbnClear);
            this.splitContainer2.Panel2.Controls.Add(this.sbnRegister);
            this.splitContainer2.Panel2.Controls.Add(this.labelControl8);
            this.splitContainer2.Panel2.Controls.Add(this.labelControl6);
            this.splitContainer2.Panel2.Controls.Add(this.labelControl3);
            this.splitContainer2.Panel2.Controls.Add(this.txtLastName);
            this.splitContainer2.Panel2.Controls.Add(this.labelControl2);
            this.splitContainer2.Panel2.Controls.Add(this.labelControl1);
            this.splitContainer2.Panel2.Controls.Add(this.txtGuarantorId);
            this.splitContainer2.Panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer2_Panel2_Paint);
            this.splitContainer2.Size = new System.Drawing.Size(808, 284);
            this.splitContainer2.SplitterDistance = 72;
            this.splitContainer2.TabIndex = 2;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Location = new System.Drawing.Point(13, 21);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(259, 25);
            this.labelControl15.TabIndex = 0;
            this.labelControl15.Text = "Driver Registration Form";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.simpleButton1.Location = new System.Drawing.Point(601, 94);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(70, 62);
            this.simpleButton1.TabIndex = 34;
            this.simpleButton1.Text = "Clear";
            // 
            // sbnRegisterGuarantor
            // 
            this.sbnRegisterGuarantor.Image = ((System.Drawing.Image)(resources.GetObject("sbnRegisterGuarantor.Image")));
            this.sbnRegisterGuarantor.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.sbnRegisterGuarantor.Location = new System.Drawing.Point(601, 25);
            this.sbnRegisterGuarantor.Name = "sbnRegisterGuarantor";
            this.sbnRegisterGuarantor.Size = new System.Drawing.Size(70, 62);
            this.sbnRegisterGuarantor.TabIndex = 33;
            this.sbnRegisterGuarantor.Text = "Register";
            this.sbnRegisterGuarantor.Click += new System.EventHandler(this.sbnRegisterGuarantor_Click);
            // 
            // sbnBrowsePhoto
            // 
            this.sbnBrowsePhoto.Location = new System.Drawing.Point(393, 164);
            this.sbnBrowsePhoto.Name = "sbnBrowsePhoto";
            this.sbnBrowsePhoto.Size = new System.Drawing.Size(75, 20);
            this.sbnBrowsePhoto.TabIndex = 32;
            this.sbnBrowsePhoto.Text = "Browse ...";
            this.sbnBrowsePhoto.Click += new System.EventHandler(this.sbnBrowsePhoto_Click);
            // 
            // pcbGurantor
            // 
            this.pcbGurantor.Location = new System.Drawing.Point(393, 25);
            this.pcbGurantor.Name = "pcbGurantor";
            this.pcbGurantor.Size = new System.Drawing.Size(148, 135);
            this.pcbGurantor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbGurantor.TabIndex = 31;
            this.pcbGurantor.TabStop = false;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(328, 29);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(28, 13);
            this.labelControl4.TabIndex = 29;
            this.labelControl4.Text = "Photo";
            // 
            // txtPhoneNo
            // 
            this.txtPhoneNo.Location = new System.Drawing.Point(116, 163);
            this.txtPhoneNo.Name = "txtPhoneNo";
            this.txtPhoneNo.Size = new System.Drawing.Size(176, 20);
            this.txtPhoneNo.TabIndex = 13;
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(18, 166);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(70, 13);
            this.labelControl14.TabIndex = 12;
            this.labelControl14.Text = "Phone Number";
            // 
            // icbGender
            // 
            this.icbGender.Location = new System.Drawing.Point(116, 117);
            this.icbGender.Name = "icbGender";
            this.icbGender.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbGender.Size = new System.Drawing.Size(176, 20);
            this.icbGender.TabIndex = 9;
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(116, 48);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(176, 20);
            this.txtFirstName.TabIndex = 3;
            // 
            // dteBirthDate
            // 
            this.dteBirthDate.EditValue = null;
            this.dteBirthDate.Location = new System.Drawing.Point(116, 140);
            this.dteBirthDate.Name = "dteBirthDate";
            this.dteBirthDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteBirthDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteBirthDate.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.Classic;
            this.dteBirthDate.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.dteBirthDate.Size = new System.Drawing.Size(176, 20);
            this.dteBirthDate.TabIndex = 11;
            // 
            // txtMiddleName
            // 
            this.txtMiddleName.Location = new System.Drawing.Point(116, 71);
            this.txtMiddleName.Name = "txtMiddleName";
            this.txtMiddleName.Size = new System.Drawing.Size(176, 20);
            this.txtMiddleName.TabIndex = 5;
            // 
            // Type
            // 
            this.Type.Location = new System.Drawing.Point(19, 51);
            this.Type.Name = "Type";
            this.Type.Size = new System.Drawing.Size(51, 13);
            this.Type.TabIndex = 2;
            this.Type.Text = "First Name";
            // 
            // sbnClear
            // 
            this.sbnClear.Image = ((System.Drawing.Image)(resources.GetObject("sbnClear.Image")));
            this.sbnClear.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.sbnClear.Location = new System.Drawing.Point(832, 134);
            this.sbnClear.Name = "sbnClear";
            this.sbnClear.Size = new System.Drawing.Size(70, 62);
            this.sbnClear.TabIndex = 28;
            this.sbnClear.Text = "Clear";
            // 
            // sbnRegister
            // 
            this.sbnRegister.Image = ((System.Drawing.Image)(resources.GetObject("sbnRegister.Image")));
            this.sbnRegister.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.sbnRegister.Location = new System.Drawing.Point(832, 65);
            this.sbnRegister.Name = "sbnRegister";
            this.sbnRegister.Size = new System.Drawing.Size(70, 62);
            this.sbnRegister.TabIndex = 27;
            this.sbnRegister.Text = "Register";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(18, 120);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(35, 13);
            this.labelControl8.TabIndex = 8;
            this.labelControl8.Text = "Gender";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(18, 143);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(48, 13);
            this.labelControl6.TabIndex = 10;
            this.labelControl6.Text = "Birth Date";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(18, 97);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(50, 13);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Last Name";
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(116, 94);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(176, 20);
            this.txtLastName.TabIndex = 7;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(19, 74);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 13);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Middle Name";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(19, 29);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(63, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Guarantor ID";
            // 
            // txtGuarantorId
            // 
            this.txtGuarantorId.Enabled = false;
            this.txtGuarantorId.Location = new System.Drawing.Point(116, 25);
            this.txtGuarantorId.Name = "txtGuarantorId";
            this.txtGuarantorId.Size = new System.Drawing.Size(176, 20);
            this.txtGuarantorId.TabIndex = 1;
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 1;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Size = new System.Drawing.Size(844, 49);
            // 
            // ofdGPhoto
            // 
            this.ofdGPhoto.Filter = "Supported Picture Files | *.jpg; *.jpeg; *png; | All Files | *.*";
            // 
            // RegisterGuarantor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 581);
            this.Controls.Add(this.spcDriverList);
            this.Controls.Add(this.splitContainer2);
            this.Controls.Add(this.ribbonControl1);
            this.Name = "RegisterGuarantor";
            this.Ribbon = this.ribbonControl1;
            this.Text = "RegisterGuarantor";
            this.Load += new System.EventHandler(this.RegisterGuarantor_Load);
            this.spcDriverList.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spcDriverList)).EndInit();
            this.spcDriverList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcDriverList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pcbGurantor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhoneNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbGender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteBirthDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteBirthDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMiddleName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGuarantorId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer spcDriverList;
        private DevExpress.XtraGrid.GridControl gcDriverList;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txtPhoneNo;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.ImageComboBoxEdit icbGender;
        private DevExpress.XtraEditors.TextEdit txtFirstName;
        private DevExpress.XtraEditors.DateEdit dteBirthDate;
        private DevExpress.XtraEditors.TextEdit txtMiddleName;
        private DevExpress.XtraEditors.LabelControl Type;
        private DevExpress.XtraEditors.SimpleButton sbnClear;
        private DevExpress.XtraEditors.SimpleButton sbnRegister;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtLastName;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtGuarantorId;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraEditors.SimpleButton sbnBrowsePhoto;
        private System.Windows.Forms.PictureBox pcbGurantor;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private System.Windows.Forms.OpenFileDialog ofdGPhoto;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton sbnRegisterGuarantor;
    }
}