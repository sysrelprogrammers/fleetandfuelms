﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using System.Data.SqlClient;
using System.IO;

namespace FleetFuelMS
{
    public partial class RegisterGuarantor : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public string strGuarantorID = "";

        public RegisterGuarantor()
        {
            InitializeComponent();
        }

        private void splitContainer2_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void sbnBrowsePhoto_Click(object sender, EventArgs e)
        {
            if( ofdGPhoto.ShowDialog() == DialogResult.OK)
            {
                pcbGurantor.Image = Image.FromFile(ofdGPhoto.FileName);
            }
        }

        private void RegisterGuarantor_Load(object sender, EventArgs e)
        {
            loadControls();
        }

        private void loadControls()
        {
            txtGuarantorId.Text = Tasks.getNewCode("GUARANTOR", 3);

            icbGender.Properties.Items.Add(new ImageComboBoxItem("- SELECT GENDER -"));
            icbGender.Properties.Items.Add(new ImageComboBoxItem("Male", 0));
            icbGender.Properties.Items.Add(new ImageComboBoxItem("Female", 1));

            icbGender.SelectedIndex = 0;
        }

        private void sbnRegisterGuarantor_Click(object sender, EventArgs e)
        {
            SqlCommand cmdRegGuarantor = null;
            try
            {
                cmdRegGuarantor = new SqlCommand() {
                    Connection = GloVars.Conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "RegisterGuarantor"
                };

                string newGuarantorID = Tasks.getNewCode("GUARANTOR", 3);

                cmdRegGuarantor.Parameters.Add(new SqlParameter("@GuarantorID", newGuarantorID));
                cmdRegGuarantor.Parameters.Add(new SqlParameter("@FirstName",txtFirstName.Text));
                cmdRegGuarantor.Parameters.Add(new SqlParameter("@MiddleName",txtMiddleName.Text));
                cmdRegGuarantor.Parameters.Add(new SqlParameter("@LastName",txtLastName.Text));
                cmdRegGuarantor.Parameters.Add(new SqlParameter("@Gender",icbGender.Properties.Items[icbGender.SelectedIndex].ToString()));
                cmdRegGuarantor.Parameters.Add(new SqlParameter("@BirthDate", dteBirthDate.DateTime));
                cmdRegGuarantor.Parameters.Add(new SqlParameter("@Phone", txtPhoneNo.Text));

                //Create filessytem object to stream the image into
                //binary data
                byte[] bytImage = null;

                if (ofdGPhoto.FileName != "") {
                    FileStream fsImage = new FileStream(ofdGPhoto.FileName, FileMode.Open, FileAccess.Read);
                    bytImage = new byte[fsImage.Length];
                    fsImage.Read(bytImage, 0, System.Convert.ToInt32(fsImage.Length));
                }

                //Here create an appropriate image paramater and 
                SqlParameter sqpImage = new SqlParameter() {
                    SqlDbType = SqlDbType.Image,
                    ParameterName = "GImage",
                    Value = bytImage 
                };

                cmdRegGuarantor.Parameters.Add(sqpImage);
                cmdRegGuarantor.Parameters.Add(new SqlParameter("@RegDate", Tasks.getCurrentTime()));
                cmdRegGuarantor.Parameters.Add(new SqlParameter("@RUser", GloVars.loggedUser.UserID));

                cmdRegGuarantor.ExecuteNonQuery();

                this.strGuarantorID = newGuarantorID;

                //Now Update the CODELOG table for GUARANTOR
                cmdRegGuarantor = new SqlCommand()
                {
                    Connection = GloVars.Conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "RegisterGuarantor"
                };

                cmdRegGuarantor.Parameters.Add(new SqlParameter("@TName", "GUARANTOR"));
                cmdRegGuarantor.Parameters.Add(new SqlParameter("@LargeCode", int.Parse(newGuarantorID.Substring(3))));

                MessageBox.Show(GloVars.MSG_GUARANTOR_REGISTERED);
                this.DialogResult = DialogResult.OK;
            }
            catch(SqlException Ex)
            {

            }
            finally
            {
                if (cmdRegGuarantor != null)
                    cmdRegGuarantor.Dispose();
            }
        }
    }
}
