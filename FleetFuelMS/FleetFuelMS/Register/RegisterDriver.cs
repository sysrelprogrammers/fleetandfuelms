﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.Grid;
using System.IO;

namespace FleetFuelMS
{
    public partial class RegisterDriver : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        private DataTable dtGuarantor = new DataTable(); 

        public RegisterDriver()
        {
            InitializeComponent();
        }

        private void RegisterDriver_Load(object sender, EventArgs e)
        {
            loadControls();
        }

        private void loadControls()
        {
            txtDriverId.Text = Tasks.getNewCode("DRIVER", 2);

            icbGender.Properties.Items.Add(new ImageComboBoxItem("- SELECT GENDER -"));
            icbGender.Properties.Items.Add(new ImageComboBoxItem("Male",0));
            icbGender.Properties.Items.Add(new ImageComboBoxItem("Female",1));

            //Initialize Guarantor Datatable
            dtGuarantor = new DataTable("Guarantor");
            dtGuarantor.Columns.Add("ID");
            dtGuarantor.Columns.Add("FullName");
            dtGuarantor.Columns.Add("Gender");
            dtGuarantor.Columns.Add("Organization");
            dtGuarantor.Columns.Add("Reference");

            gcGuarantors.DataSource = dtGuarantor;
            
            loadLicenses();
            loadVehicles();
            loadGuarantor();
            loadDriverList();

            icbGender.SelectedIndex = 0;
            icbLicense.SelectedIndex = 0;
            icbVehicle.SelectedIndex = 0;
            icbGuarantor.SelectedIndex = 0;
        }

        private void loadLicenses()
        {
            SqlCommand cmdLicense = null;
            SqlDataReader rdrLicense = null;

            try
            {
                cmdLicense = new SqlCommand()
                {
                    Connection = GloVars.Conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "getLicenseInfo"
                };

                cmdLicense.Parameters.Add(new SqlParameter("@LID", "%"));
                rdrLicense = cmdLicense.ExecuteReader();

                icbLicense.Properties.Items.Clear();
                icbLicense.Properties.Items.Add(new ImageComboBoxItem("- SELECT LICENSE -"));
                while(rdrLicense.Read())
                {
                    string strTemp = string.Format("{0} - {1}", rdrLicense["LName"].ToString(),rdrLicense["LLevel"].ToString());
                    ImageComboBoxItem icbiLicense = new ImageComboBoxItem(strTemp);
                    icbiLicense.Value = rdrLicense["LicenseID"];
                    icbLicense.Properties.Items.Add(icbiLicense);
                }
            }
            catch (SqlException Ex)
            {

            }
            finally
            {
                if (rdrLicense != null)
                    rdrLicense.Close();
                cmdLicense.Dispose();
            }
        }

        private void loadVehicles()
        {
            SqlCommand cmdVehicles = null;
            SqlDataReader rdrVehicles = null;

            try
            {
                cmdVehicles = new SqlCommand()
                {
                    Connection = GloVars.Conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "getVehicleInfo"
                };

                cmdVehicles.Parameters.Add(new SqlParameter("@VID", "%"));

                rdrVehicles = cmdVehicles.ExecuteReader();

                icbVehicle.Properties.Items.Clear();
                icbVehicle.Properties.Items.Add(new ImageComboBoxItem("- SELECT VEHICLE -"));
                while (rdrVehicles.Read())
                {
                    string strTemp = string.Format("{0} | {1} | {2}", 
                                     rdrVehicles["Make"].ToString(), rdrVehicles["Model"].ToString(), rdrVehicles["PlateNo"].ToString());
                    ImageComboBoxItem tempICItem = new ImageComboBoxItem(strTemp);
                    tempICItem.Value = rdrVehicles["VehicleID"].ToString();
                    icbVehicle.Properties.Items.Add(tempICItem);
                }
            }
            catch (SqlException Ex)
            {

            }
            finally
            {
                if (rdrVehicles != null)
                    rdrVehicles.Close();
                cmdVehicles.Dispose();
            }

        }

        private void loadGuarantor()
        {
            SqlCommand cmdGuarantor = null;
            SqlDataReader rdrGuarantor = null;

            try
            {
                cmdGuarantor = new SqlCommand()
                {
                    Connection = GloVars.Conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "getGuarantor"
                };

                rdrGuarantor = cmdGuarantor.ExecuteReader();

                icbGuarantor.Properties.Items.Clear();
                icbGuarantor.Properties.Items.Add(new ImageComboBoxItem("- SELECT GUARANTOR -"));
                while (rdrGuarantor.Read())
                {
                     string strTemp = string.Format("{0}  {1}  {2}",
                                     rdrGuarantor["FirstName"].ToString(), rdrGuarantor["MiddleName"].ToString(), rdrGuarantor["LastName"].ToString());

                    ImageComboBoxItem newItem = new ImageComboBoxItem(strTemp, rdrGuarantor["GuarantorID"]);

                    icbGuarantor.Properties.Items.Add(newItem);
                }
            }
            catch (SqlException Ex)
            {

            }
            finally
            {
                if (rdrGuarantor != null)
                    rdrGuarantor.Close();
                cmdGuarantor.Dispose();
            }
        }

        private void loadDriverList()
        {
            SqlCommand cmdloadD = new SqlCommand()
            {
                Connection = GloVars.Conn,
                CommandType = System.Data.CommandType.StoredProcedure,
                CommandText = "getDriverInfo"
            };

            SqlDataReader rdrLoadDriver = null;

            try
            {
                rdrLoadDriver = cmdloadD.ExecuteReader();

                DataTable dtDriver = new DataTable();

                dtDriver.Columns.Add("SNo");
                dtDriver.Columns.Add("First Name");
                dtDriver.Columns.Add("Middle Name");
                dtDriver.Columns.Add("Last Name");
                dtDriver.Columns.Add("Gender");
                dtDriver.Columns.Add("Birth Date");
                dtDriver.Columns.Add("Phone");
                dtDriver.Columns.Add("License");
                dtDriver.Columns.Add("Vehicle");


                while (rdrLoadDriver.Read())
                {
                    DataRow drVehicle = dtDriver.NewRow();
                    drVehicle["SNo"] = rdrLoadDriver["SNo"];
                    drVehicle["First Name"] = rdrLoadDriver["First Name"];
                    drVehicle["Middle Name"] = rdrLoadDriver["Middle Name"];
                    drVehicle["Last Name"] = rdrLoadDriver["Last Name"];
                    drVehicle["Gender"] = rdrLoadDriver["Gender"];
                    drVehicle["Birth Date"] = rdrLoadDriver["Birth Date"];
                    drVehicle["Phone"] = rdrLoadDriver["Phone"];
                    drVehicle["License"] = rdrLoadDriver["License"];
                    drVehicle["Vehicle"] = rdrLoadDriver["Vehicle"];

                    dtDriver.Rows.Add(drVehicle);
                }

                gcDriverList.DataSource = dtDriver;
                gvDriverList.BestFitColumns(true);
                gvDriverList.Columns[0].BestFit();

            }
            catch (SqlException Ex)
            {

            }
            finally
            {
                rdrLoadDriver.Close();
                cmdloadD.Dispose();
            }
        }

        private void RegisterDriver_Resize(object sender, EventArgs e)
        {
            gpcDriverList.Location = new Point(12, 343);
            gpcDriverList.Height = this.Height - (gpcDriverList.Top + 50);
            gpcDriverList.Width = this.Width - (2 * gpcDriverList.Left + 10);
        }

        private void sbnLicenseInfo_Click(object sender, EventArgs e)
        {
            if (icbLicense.SelectedIndex <= 0)
            {
                MessageBox.Show(GloVars.MSG_);
                return;
            }

            SqlCommand cmdLicense = null;
            SqlDataReader rdrLicense = null;

            try
            {
                cmdLicense = new SqlCommand()
                {
                    Connection = GloVars.Conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "getLicenseInfo"
                };

                string strLicenseID = icbLicense.Properties.Items[icbLicense.SelectedIndex].Value.ToString();

                cmdLicense.Parameters.Add(new SqlParameter("@LID", strLicenseID));

                rdrLicense = cmdLicense.ExecuteReader();

                if (rdrLicense.Read())
                {
                    lbcLicenseInfo.Text = "";

                    if (rdrLicense["LDescription"] != DBNull.Value)
                        lbcLicenseInfo.Text =  rdrLicense["LDescription"].ToString(); // check this if it is DBNull?
                }
                pccLicense.ShowPopup(Cursor.Position);
            }
            catch (SqlException Ex) { MessageBox.Show(Ex.Message);}
            finally {
                rdrLicense.Close();
                cmdLicense.Dispose();
            }
        }

        private void sbnVehicleInfo_Click(object sender, EventArgs e)
        {
            if (icbVehicle.SelectedIndex <= 0)
                return;

            SqlCommand cmdVehicle = null;
            SqlDataReader rdrVehicle = null;

            try
            {
                cmdVehicle = new SqlCommand()
                {
                    Connection = GloVars.Conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "getVehicleInfo"
                };

                string strVehicleID = icbVehicle.Properties.Items[icbVehicle.SelectedIndex].Value.ToString();

                cmdVehicle.Parameters.Add(new SqlParameter("@VID", strVehicleID));

                rdrVehicle = cmdVehicle.ExecuteReader();

                if( rdrVehicle.Read() )
                {
                    lbcPlateNo.Text = rdrVehicle["PlateNo"].ToString();
                    lbcMake.Text = rdrVehicle["Make"].ToString();
                    lbcModel.Text = rdrVehicle["Model"].ToString();
                    lbcFuel.Text = rdrVehicle["FName"].ToString();
                    //lbcDriver.Text = rdrVehicle[""].ToString();

                    // Validate Photo if DBNULL
                    if (rdrVehicle["VImage"] != DBNull.Value)
                    {
                        byte[] VehicleImage = (byte[]) rdrVehicle["VImage"];
                        pceVehicleImage.Image = Image.FromFile(Tasks.getByteImage(VehicleImage, "PNG"));
                    }
                    else
                    {
                    }
                }

                pccVehicleInfo.ShowPopup(Cursor.Position);
            }
            catch (SqlException Ex) { MessageBox.Show(Ex.Message);}
            catch (Exception Ex) { }
            finally {
                rdrVehicle.Close();
                cmdVehicle.Dispose();
            }
        }

        private void pccVehicleInfo_Leave(object sender, EventArgs e)
        {
            if ( pceVehicleImage.Image != null )
                pceVehicleImage.Image.Dispose();
        }

        private void sbnRegister_Click(object sender, EventArgs e)
        {
            // Go for Validation here

            SqlCommand cmdRegDriver = null;

            try
            {
                cmdRegDriver = new SqlCommand()
                {
                    Connection = GloVars.Conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "RegisterDriver"
                };

                string newDriverCode = Tasks.getNewCode("DRIVER", 2);

                //Pass the necessary Parameters for registration
                cmdRegDriver.Parameters.Add(new SqlParameter("@DriverID", newDriverCode));
                cmdRegDriver.Parameters.Add(new SqlParameter("@FirstName", txtFirstName.Text));
                cmdRegDriver.Parameters.Add(new SqlParameter("@MiddleName", txtMiddleName.Text));
                cmdRegDriver.Parameters.Add(new SqlParameter("@LastName", txtLastName.Text));
                cmdRegDriver.Parameters.Add(new SqlParameter("@Gender", icbGender.Properties.Items[icbGender.SelectedIndex].ToString()));
                cmdRegDriver.Parameters.Add(new SqlParameter("@BirthDate", dteBirthDate.DateTime));
                cmdRegDriver.Parameters.Add(new SqlParameter("@Phone", txtPhoneNo.Text));
                cmdRegDriver.Parameters.Add(new SqlParameter("@License", icbLicense.Properties.Items[icbLicense.SelectedIndex].Value));
                cmdRegDriver.Parameters.Add(new SqlParameter("@Vehicle", icbVehicle.Properties.Items[icbVehicle.SelectedIndex].Value));

                // Here create byte stream for the image 
                // to be passesed as parameter 

                byte[] bytImage = null;

                if (ofdImage.FileName != "") {
                    FileStream fsImage = new FileStream(ofdImage.FileName, FileMode.Open, FileAccess.Read);
                    bytImage = new byte[fsImage.Length];
                    fsImage.Read(bytImage, 0, System.Convert.ToInt32(fsImage.Length));
                }

                // Pass the streamed image byte to sql parameter
                SqlParameter tempImage = new SqlParameter()
                {
                    ParameterName = "@DImage",
                    Value = bytImage,
                    SqlDbType = SqlDbType.Image
                };

                cmdRegDriver.Parameters.Add(tempImage);
                cmdRegDriver.Parameters.Add(new SqlParameter("@RegDate", Tasks.getCurrentTime()));
                cmdRegDriver.Parameters.Add(new SqlParameter("@RUser", GloVars.loggedUser.UserID));

                cmdRegDriver.ExecuteNonQuery();


                // Now the Driver is registered
                // and it is time to update the code log table
                Tasks.updateCodeLog("DRIVER", newDriverCode);

                // Finally Register all the Guarantors of the driver
                // if there are any?
                if (gvGuarantors.DataRowCount > 0)
                {
                    // Go through each rows in the data table and
                    // register them all
                    foreach (DataRow item in dtGuarantor.Rows)
                    {
                        // Add the selected DriverGuarantor
                        SqlCommand cmdRegDrGu = null;
                        cmdRegDrGu = new SqlCommand()
                        {
                            Connection = GloVars.Conn,
                            CommandType = CommandType.StoredProcedure,
                            CommandText = "registerDriverGuarantor"
                        };

                        string newDriverGuarantor = Tasks.getNewCode("DRIVERGUARANTORS", 3);

                        cmdRegDrGu.Parameters.Add(new SqlParameter("@DGID", newDriverGuarantor));
                        cmdRegDrGu.Parameters.Add(new SqlParameter("@Driver", newDriverCode));
                        cmdRegDrGu.Parameters.Add(new SqlParameter("@Guarantor", item["ID"]));
                        cmdRegDrGu.Parameters.Add(new SqlParameter("@Organization", item["Organization"]));
                        cmdRegDrGu.Parameters.Add(new SqlParameter("@LetterReference", item["Reference"]));
                        cmdRegDrGu.Parameters.Add(new SqlParameter("@RegDate", Tasks.getCurrentTime()));
                        cmdRegDrGu.Parameters.Add(new SqlParameter("@RUser", GloVars.loggedUser.UserID));

                        cmdRegDrGu.ExecuteNonQuery();

                        //Update Code log for DriverGuarantor
                        Tasks.updateCodeLog("DRIVERGUARANTORS",newDriverGuarantor);
                    }
                }

                MessageBox.Show(GloVars.MSG_DRIVER_REGISTERED);

                loadDriverList();
            }
            catch (SqlException Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally {
                cmdRegDriver.Dispose();
            }
        }

        private void sbnAddGuarantor_Click(object sender, EventArgs e)
        {
            if (icbGuarantor.SelectedIndex < 1)
                return;

            // Add row to datatable dtGuarantor and refresh
            // the datasource for the grid control

            DataRow drNewGuarantor = dtGuarantor.NewRow();
            drNewGuarantor["ID"] = icbGuarantor.Properties.Items[icbGuarantor.SelectedIndex].Value.ToString();
            drNewGuarantor["FullName"] = icbGuarantor.Properties.Items[icbGuarantor.SelectedIndex].ToString();
            drNewGuarantor["Gender"] = "";
            drNewGuarantor["Organization"] = "";
            drNewGuarantor["Reference"] = "";

            dtGuarantor.Rows.Add(drNewGuarantor);

            gcGuarantors.DataSource = null;
            gcGuarantors.DataSource = dtGuarantor;
            gcGuarantors.Refresh();
        }

        private void sbnNewGuarantor_Click(object sender, EventArgs e)
        {
            RegisterGuarantor frmRegGuar = new RegisterGuarantor();

            if (frmRegGuar.ShowDialog() == DialogResult.OK)
            {
                loadGuarantor();
                ImageComboBoxItem cbiTemp = icbGuarantor.Properties.Items.GetItem(frmRegGuar.strGuarantorID);
                icbGuarantor.SelectedItem = cbiTemp;
            }
        }

        private void sbnBrowse_Click(object sender, EventArgs e)
        {
            ofdImage.Title = "Locate Driver Image";
            ofdImage.Filter = "Support Image files | *.jpeg; *.jpg; *.png | All file types | *.*";
            ofdImage.FileName = "";

            if(ofdImage.ShowDialog() == DialogResult.OK )
            {
                pcbPhoto.Image = Image.FromFile(ofdImage.FileName);
            }
        }

        private void pcbPhoto_MouseHover(object sender, EventArgs e)
        {
            pcbPhoto.Size = new Size(186, 200);
            pcbPhoto.Location = new Point(182, 27);
        }

        private void pcbPhoto_MouseLeave(object sender, EventArgs e)
        {
            pcbPhoto.Size = new Size(123, 134);
            pcbPhoto.Location = new Point(213, 27);
        }
    }
}
