﻿namespace FleetFuelMS
{
    partial class VehicleDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroup2DColumn chartControlCommandGalleryItemGroup2DColumn2 = new DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroup2DColumn();
            DevExpress.XtraCharts.UI.CreateBarChartItem createBarChartItem2 = new DevExpress.XtraCharts.UI.CreateBarChartItem();
            DevExpress.XtraCharts.UI.CreateFullStackedBarChartItem createFullStackedBarChartItem2 = new DevExpress.XtraCharts.UI.CreateFullStackedBarChartItem();
            DevExpress.XtraCharts.UI.CreateSideBySideFullStackedBarChartItem createSideBySideFullStackedBarChartItem2 = new DevExpress.XtraCharts.UI.CreateSideBySideFullStackedBarChartItem();
            DevExpress.XtraCharts.UI.CreateSideBySideStackedBarChartItem createSideBySideStackedBarChartItem2 = new DevExpress.XtraCharts.UI.CreateSideBySideStackedBarChartItem();
            DevExpress.XtraCharts.UI.CreateStackedBarChartItem createStackedBarChartItem2 = new DevExpress.XtraCharts.UI.CreateStackedBarChartItem();
            DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroup3DColumn chartControlCommandGalleryItemGroup3DColumn2 = new DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroup3DColumn();
            DevExpress.XtraCharts.UI.CreateBar3DChartItem createBar3DChartItem2 = new DevExpress.XtraCharts.UI.CreateBar3DChartItem();
            DevExpress.XtraCharts.UI.CreateFullStackedBar3DChartItem createFullStackedBar3DChartItem2 = new DevExpress.XtraCharts.UI.CreateFullStackedBar3DChartItem();
            DevExpress.XtraCharts.UI.CreateManhattanBarChartItem createManhattanBarChartItem2 = new DevExpress.XtraCharts.UI.CreateManhattanBarChartItem();
            DevExpress.XtraCharts.UI.CreateSideBySideFullStackedBar3DChartItem createSideBySideFullStackedBar3DChartItem2 = new DevExpress.XtraCharts.UI.CreateSideBySideFullStackedBar3DChartItem();
            DevExpress.XtraCharts.UI.CreateSideBySideStackedBar3DChartItem createSideBySideStackedBar3DChartItem2 = new DevExpress.XtraCharts.UI.CreateSideBySideStackedBar3DChartItem();
            DevExpress.XtraCharts.UI.CreateStackedBar3DChartItem createStackedBar3DChartItem2 = new DevExpress.XtraCharts.UI.CreateStackedBar3DChartItem();
            DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroupCylinderColumn chartControlCommandGalleryItemGroupCylinderColumn2 = new DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroupCylinderColumn();
            DevExpress.XtraCharts.UI.CreateCylinderBar3DChartItem createCylinderBar3DChartItem2 = new DevExpress.XtraCharts.UI.CreateCylinderBar3DChartItem();
            DevExpress.XtraCharts.UI.CreateCylinderFullStackedBar3DChartItem createCylinderFullStackedBar3DChartItem2 = new DevExpress.XtraCharts.UI.CreateCylinderFullStackedBar3DChartItem();
            DevExpress.XtraCharts.UI.CreateCylinderManhattanBarChartItem createCylinderManhattanBarChartItem2 = new DevExpress.XtraCharts.UI.CreateCylinderManhattanBarChartItem();
            DevExpress.XtraCharts.UI.CreateCylinderSideBySideFullStackedBar3DChartItem createCylinderSideBySideFullStackedBar3DChartItem2 = new DevExpress.XtraCharts.UI.CreateCylinderSideBySideFullStackedBar3DChartItem();
            DevExpress.XtraCharts.UI.CreateCylinderSideBySideStackedBar3DChartItem createCylinderSideBySideStackedBar3DChartItem2 = new DevExpress.XtraCharts.UI.CreateCylinderSideBySideStackedBar3DChartItem();
            DevExpress.XtraCharts.UI.CreateCylinderStackedBar3DChartItem createCylinderStackedBar3DChartItem2 = new DevExpress.XtraCharts.UI.CreateCylinderStackedBar3DChartItem();
            DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroupConeColumn chartControlCommandGalleryItemGroupConeColumn2 = new DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroupConeColumn();
            DevExpress.XtraCharts.UI.CreateConeBar3DChartItem createConeBar3DChartItem2 = new DevExpress.XtraCharts.UI.CreateConeBar3DChartItem();
            DevExpress.XtraCharts.UI.CreateConeFullStackedBar3DChartItem createConeFullStackedBar3DChartItem2 = new DevExpress.XtraCharts.UI.CreateConeFullStackedBar3DChartItem();
            DevExpress.XtraCharts.UI.CreateConeManhattanBarChartItem createConeManhattanBarChartItem2 = new DevExpress.XtraCharts.UI.CreateConeManhattanBarChartItem();
            DevExpress.XtraCharts.UI.CreateConeSideBySideFullStackedBar3DChartItem createConeSideBySideFullStackedBar3DChartItem2 = new DevExpress.XtraCharts.UI.CreateConeSideBySideFullStackedBar3DChartItem();
            DevExpress.XtraCharts.UI.CreateConeSideBySideStackedBar3DChartItem createConeSideBySideStackedBar3DChartItem2 = new DevExpress.XtraCharts.UI.CreateConeSideBySideStackedBar3DChartItem();
            DevExpress.XtraCharts.UI.CreateConeStackedBar3DChartItem createConeStackedBar3DChartItem2 = new DevExpress.XtraCharts.UI.CreateConeStackedBar3DChartItem();
            DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroupPyramidColumn chartControlCommandGalleryItemGroupPyramidColumn2 = new DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroupPyramidColumn();
            DevExpress.XtraCharts.UI.CreatePyramidBar3DChartItem createPyramidBar3DChartItem2 = new DevExpress.XtraCharts.UI.CreatePyramidBar3DChartItem();
            DevExpress.XtraCharts.UI.CreatePyramidFullStackedBar3DChartItem createPyramidFullStackedBar3DChartItem2 = new DevExpress.XtraCharts.UI.CreatePyramidFullStackedBar3DChartItem();
            DevExpress.XtraCharts.UI.CreatePyramidManhattanBarChartItem createPyramidManhattanBarChartItem2 = new DevExpress.XtraCharts.UI.CreatePyramidManhattanBarChartItem();
            DevExpress.XtraCharts.UI.CreatePyramidSideBySideFullStackedBar3DChartItem createPyramidSideBySideFullStackedBar3DChartItem2 = new DevExpress.XtraCharts.UI.CreatePyramidSideBySideFullStackedBar3DChartItem();
            DevExpress.XtraCharts.UI.CreatePyramidSideBySideStackedBar3DChartItem createPyramidSideBySideStackedBar3DChartItem2 = new DevExpress.XtraCharts.UI.CreatePyramidSideBySideStackedBar3DChartItem();
            DevExpress.XtraCharts.UI.CreatePyramidStackedBar3DChartItem createPyramidStackedBar3DChartItem2 = new DevExpress.XtraCharts.UI.CreatePyramidStackedBar3DChartItem();
            DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroup2DLine chartControlCommandGalleryItemGroup2DLine2 = new DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroup2DLine();
            DevExpress.XtraCharts.UI.CreateLineChartItem createLineChartItem2 = new DevExpress.XtraCharts.UI.CreateLineChartItem();
            DevExpress.XtraCharts.UI.CreateFullStackedLineChartItem createFullStackedLineChartItem2 = new DevExpress.XtraCharts.UI.CreateFullStackedLineChartItem();
            DevExpress.XtraCharts.UI.CreateScatterLineChartItem createScatterLineChartItem2 = new DevExpress.XtraCharts.UI.CreateScatterLineChartItem();
            DevExpress.XtraCharts.UI.CreateSplineChartItem createSplineChartItem2 = new DevExpress.XtraCharts.UI.CreateSplineChartItem();
            DevExpress.XtraCharts.UI.CreateStackedLineChartItem createStackedLineChartItem2 = new DevExpress.XtraCharts.UI.CreateStackedLineChartItem();
            DevExpress.XtraCharts.UI.CreateStepLineChartItem createStepLineChartItem2 = new DevExpress.XtraCharts.UI.CreateStepLineChartItem();
            DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroup3DLine chartControlCommandGalleryItemGroup3DLine2 = new DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroup3DLine();
            DevExpress.XtraCharts.UI.CreateLine3DChartItem createLine3DChartItem2 = new DevExpress.XtraCharts.UI.CreateLine3DChartItem();
            DevExpress.XtraCharts.UI.CreateFullStackedLine3DChartItem createFullStackedLine3DChartItem2 = new DevExpress.XtraCharts.UI.CreateFullStackedLine3DChartItem();
            DevExpress.XtraCharts.UI.CreateSpline3DChartItem createSpline3DChartItem2 = new DevExpress.XtraCharts.UI.CreateSpline3DChartItem();
            DevExpress.XtraCharts.UI.CreateStackedLine3DChartItem createStackedLine3DChartItem2 = new DevExpress.XtraCharts.UI.CreateStackedLine3DChartItem();
            DevExpress.XtraCharts.UI.CreateStepLine3DChartItem createStepLine3DChartItem2 = new DevExpress.XtraCharts.UI.CreateStepLine3DChartItem();
            DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroup2DPie chartControlCommandGalleryItemGroup2DPie2 = new DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroup2DPie();
            DevExpress.XtraCharts.UI.CreatePieChartItem createPieChartItem2 = new DevExpress.XtraCharts.UI.CreatePieChartItem();
            DevExpress.XtraCharts.UI.CreateDoughnutChartItem createDoughnutChartItem2 = new DevExpress.XtraCharts.UI.CreateDoughnutChartItem();
            DevExpress.XtraCharts.UI.CreateNestedDoughnutChartItem createNestedDoughnutChartItem2 = new DevExpress.XtraCharts.UI.CreateNestedDoughnutChartItem();
            DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroup3DPie chartControlCommandGalleryItemGroup3DPie2 = new DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroup3DPie();
            DevExpress.XtraCharts.UI.CreatePie3DChartItem createPie3DChartItem2 = new DevExpress.XtraCharts.UI.CreatePie3DChartItem();
            DevExpress.XtraCharts.UI.CreateDoughnut3DChartItem createDoughnut3DChartItem2 = new DevExpress.XtraCharts.UI.CreateDoughnut3DChartItem();
            DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroup2DBar chartControlCommandGalleryItemGroup2DBar2 = new DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroup2DBar();
            DevExpress.XtraCharts.UI.CreateRotatedBarChartItem createRotatedBarChartItem2 = new DevExpress.XtraCharts.UI.CreateRotatedBarChartItem();
            DevExpress.XtraCharts.UI.CreateRotatedFullStackedBarChartItem createRotatedFullStackedBarChartItem2 = new DevExpress.XtraCharts.UI.CreateRotatedFullStackedBarChartItem();
            DevExpress.XtraCharts.UI.CreateRotatedSideBySideFullStackedBarChartItem createRotatedSideBySideFullStackedBarChartItem2 = new DevExpress.XtraCharts.UI.CreateRotatedSideBySideFullStackedBarChartItem();
            DevExpress.XtraCharts.UI.CreateRotatedSideBySideStackedBarChartItem createRotatedSideBySideStackedBarChartItem2 = new DevExpress.XtraCharts.UI.CreateRotatedSideBySideStackedBarChartItem();
            DevExpress.XtraCharts.UI.CreateRotatedStackedBarChartItem createRotatedStackedBarChartItem2 = new DevExpress.XtraCharts.UI.CreateRotatedStackedBarChartItem();
            DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroup2DArea chartControlCommandGalleryItemGroup2DArea2 = new DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroup2DArea();
            DevExpress.XtraCharts.UI.CreateAreaChartItem createAreaChartItem2 = new DevExpress.XtraCharts.UI.CreateAreaChartItem();
            DevExpress.XtraCharts.UI.CreateFullStackedAreaChartItem createFullStackedAreaChartItem2 = new DevExpress.XtraCharts.UI.CreateFullStackedAreaChartItem();
            DevExpress.XtraCharts.UI.CreateFullStackedSplineAreaChartItem createFullStackedSplineAreaChartItem2 = new DevExpress.XtraCharts.UI.CreateFullStackedSplineAreaChartItem();
            DevExpress.XtraCharts.UI.CreateSplineAreaChartItem createSplineAreaChartItem2 = new DevExpress.XtraCharts.UI.CreateSplineAreaChartItem();
            DevExpress.XtraCharts.UI.CreateStackedAreaChartItem createStackedAreaChartItem2 = new DevExpress.XtraCharts.UI.CreateStackedAreaChartItem();
            DevExpress.XtraCharts.UI.CreateStackedSplineAreaChartItem createStackedSplineAreaChartItem2 = new DevExpress.XtraCharts.UI.CreateStackedSplineAreaChartItem();
            DevExpress.XtraCharts.UI.CreateStepAreaChartItem createStepAreaChartItem2 = new DevExpress.XtraCharts.UI.CreateStepAreaChartItem();
            DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroup3DArea chartControlCommandGalleryItemGroup3DArea2 = new DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroup3DArea();
            DevExpress.XtraCharts.UI.CreateArea3DChartItem createArea3DChartItem2 = new DevExpress.XtraCharts.UI.CreateArea3DChartItem();
            DevExpress.XtraCharts.UI.CreateFullStackedArea3DChartItem createFullStackedArea3DChartItem2 = new DevExpress.XtraCharts.UI.CreateFullStackedArea3DChartItem();
            DevExpress.XtraCharts.UI.CreateFullStackedSplineArea3DChartItem createFullStackedSplineArea3DChartItem2 = new DevExpress.XtraCharts.UI.CreateFullStackedSplineArea3DChartItem();
            DevExpress.XtraCharts.UI.CreateSplineArea3DChartItem createSplineArea3DChartItem2 = new DevExpress.XtraCharts.UI.CreateSplineArea3DChartItem();
            DevExpress.XtraCharts.UI.CreateStackedArea3DChartItem createStackedArea3DChartItem2 = new DevExpress.XtraCharts.UI.CreateStackedArea3DChartItem();
            DevExpress.XtraCharts.UI.CreateStackedSplineArea3DChartItem createStackedSplineArea3DChartItem2 = new DevExpress.XtraCharts.UI.CreateStackedSplineArea3DChartItem();
            DevExpress.XtraCharts.UI.CreateStepArea3DChartItem createStepArea3DChartItem2 = new DevExpress.XtraCharts.UI.CreateStepArea3DChartItem();
            DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroupPoint chartControlCommandGalleryItemGroupPoint2 = new DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroupPoint();
            DevExpress.XtraCharts.UI.CreatePointChartItem createPointChartItem2 = new DevExpress.XtraCharts.UI.CreatePointChartItem();
            DevExpress.XtraCharts.UI.CreateBubbleChartItem createBubbleChartItem2 = new DevExpress.XtraCharts.UI.CreateBubbleChartItem();
            DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroupFunnel chartControlCommandGalleryItemGroupFunnel2 = new DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroupFunnel();
            DevExpress.XtraCharts.UI.CreateFunnelChartItem createFunnelChartItem2 = new DevExpress.XtraCharts.UI.CreateFunnelChartItem();
            DevExpress.XtraCharts.UI.CreateFunnel3DChartItem createFunnel3DChartItem2 = new DevExpress.XtraCharts.UI.CreateFunnel3DChartItem();
            DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroupFinancial chartControlCommandGalleryItemGroupFinancial2 = new DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroupFinancial();
            DevExpress.XtraCharts.UI.CreateStockChartItem createStockChartItem2 = new DevExpress.XtraCharts.UI.CreateStockChartItem();
            DevExpress.XtraCharts.UI.CreateCandleStickChartItem createCandleStickChartItem2 = new DevExpress.XtraCharts.UI.CreateCandleStickChartItem();
            DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroupRadar chartControlCommandGalleryItemGroupRadar2 = new DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroupRadar();
            DevExpress.XtraCharts.UI.CreateRadarPointChartItem createRadarPointChartItem2 = new DevExpress.XtraCharts.UI.CreateRadarPointChartItem();
            DevExpress.XtraCharts.UI.CreateRadarLineChartItem createRadarLineChartItem2 = new DevExpress.XtraCharts.UI.CreateRadarLineChartItem();
            DevExpress.XtraCharts.UI.CreateRadarAreaChartItem createRadarAreaChartItem2 = new DevExpress.XtraCharts.UI.CreateRadarAreaChartItem();
            DevExpress.XtraCharts.UI.CreateScatterRadarLineChartItem createScatterRadarLineChartItem2 = new DevExpress.XtraCharts.UI.CreateScatterRadarLineChartItem();
            DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroupPolar chartControlCommandGalleryItemGroupPolar2 = new DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroupPolar();
            DevExpress.XtraCharts.UI.CreatePolarPointChartItem createPolarPointChartItem2 = new DevExpress.XtraCharts.UI.CreatePolarPointChartItem();
            DevExpress.XtraCharts.UI.CreatePolarLineChartItem createPolarLineChartItem2 = new DevExpress.XtraCharts.UI.CreatePolarLineChartItem();
            DevExpress.XtraCharts.UI.CreatePolarAreaChartItem createPolarAreaChartItem2 = new DevExpress.XtraCharts.UI.CreatePolarAreaChartItem();
            DevExpress.XtraCharts.UI.CreateScatterPolarLineChartItem createScatterPolarLineChartItem2 = new DevExpress.XtraCharts.UI.CreateScatterPolarLineChartItem();
            DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroupRange chartControlCommandGalleryItemGroupRange2 = new DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroupRange();
            DevExpress.XtraCharts.UI.CreateRangeBarChartItem createRangeBarChartItem2 = new DevExpress.XtraCharts.UI.CreateRangeBarChartItem();
            DevExpress.XtraCharts.UI.CreateSideBySideRangeBarChartItem createSideBySideRangeBarChartItem2 = new DevExpress.XtraCharts.UI.CreateSideBySideRangeBarChartItem();
            DevExpress.XtraCharts.UI.CreateRangeAreaChartItem createRangeAreaChartItem2 = new DevExpress.XtraCharts.UI.CreateRangeAreaChartItem();
            DevExpress.XtraCharts.UI.CreateRangeArea3DChartItem createRangeArea3DChartItem2 = new DevExpress.XtraCharts.UI.CreateRangeArea3DChartItem();
            DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroupGantt chartControlCommandGalleryItemGroupGantt2 = new DevExpress.XtraCharts.UI.ChartControlCommandGalleryItemGroupGantt();
            DevExpress.XtraCharts.UI.CreateGanttChartItem createGanttChartItem2 = new DevExpress.XtraCharts.UI.CreateGanttChartItem();
            DevExpress.XtraCharts.UI.CreateSideBySideGanttChartItem createSideBySideGanttChartItem2 = new DevExpress.XtraCharts.UI.CreateSideBySideGanttChartItem();
            DevExpress.Skins.SkinPaddingEdges skinPaddingEdges3 = new DevExpress.Skins.SkinPaddingEdges();
            DevExpress.Skins.SkinPaddingEdges skinPaddingEdges4 = new DevExpress.Skins.SkinPaddingEdges();
            DevExpress.XtraCharts.XYDiagram xyDiagram2 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SeriesPoint seriesPoint6 = new DevExpress.XtraCharts.SeriesPoint("0", new object[] {
            ((object)(0.2D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint7 = new DevExpress.XtraCharts.SeriesPoint("1", new object[] {
            ((object)(6.3D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint8 = new DevExpress.XtraCharts.SeriesPoint("2", new object[] {
            ((object)(6D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint9 = new DevExpress.XtraCharts.SeriesPoint("3", new object[] {
            ((object)(3.2D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint10 = new DevExpress.XtraCharts.SeriesPoint("4", new object[] {
            ((object)(9D))});
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VehicleDetail));
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.createBarBaseItem1 = new DevExpress.XtraCharts.UI.CreateBarBaseItem();
            this.commandBarGalleryDropDown1 = new DevExpress.XtraBars.Commands.CommandBarGalleryDropDown(this.components);
            this.createLineBaseItem1 = new DevExpress.XtraCharts.UI.CreateLineBaseItem();
            this.commandBarGalleryDropDown2 = new DevExpress.XtraBars.Commands.CommandBarGalleryDropDown(this.components);
            this.createPieBaseItem1 = new DevExpress.XtraCharts.UI.CreatePieBaseItem();
            this.commandBarGalleryDropDown3 = new DevExpress.XtraBars.Commands.CommandBarGalleryDropDown(this.components);
            this.createRotatedBarBaseItem1 = new DevExpress.XtraCharts.UI.CreateRotatedBarBaseItem();
            this.commandBarGalleryDropDown4 = new DevExpress.XtraBars.Commands.CommandBarGalleryDropDown(this.components);
            this.createAreaBaseItem1 = new DevExpress.XtraCharts.UI.CreateAreaBaseItem();
            this.commandBarGalleryDropDown5 = new DevExpress.XtraBars.Commands.CommandBarGalleryDropDown(this.components);
            this.createOtherSeriesTypesBaseItem1 = new DevExpress.XtraCharts.UI.CreateOtherSeriesTypesBaseItem();
            this.commandBarGalleryDropDown6 = new DevExpress.XtraBars.Commands.CommandBarGalleryDropDown(this.components);
            this.changePaletteGalleryBaseItem1 = new DevExpress.XtraCharts.UI.ChangePaletteGalleryBaseItem();
            this.commandBarGalleryDropDown7 = new DevExpress.XtraBars.Commands.CommandBarGalleryDropDown(this.components);
            this.changeAppearanceGalleryBaseItem1 = new DevExpress.XtraCharts.UI.ChangeAppearanceGalleryBaseItem();
            this.runDesignerChartItem1 = new DevExpress.XtraCharts.UI.RunDesignerChartItem();
            this.saveAsTemplateChartItem1 = new DevExpress.XtraCharts.UI.SaveAsTemplateChartItem();
            this.loadTemplateChartItem1 = new DevExpress.XtraCharts.UI.LoadTemplateChartItem();
            this.printPreviewChartItem1 = new DevExpress.XtraCharts.UI.PrintPreviewChartItem();
            this.printChartItem1 = new DevExpress.XtraCharts.UI.PrintChartItem();
            this.createExportBaseItem1 = new DevExpress.XtraCharts.UI.CreateExportBaseItem();
            this.exportToPDFChartItem1 = new DevExpress.XtraCharts.UI.ExportToPDFChartItem();
            this.exportToHTMLChartItem1 = new DevExpress.XtraCharts.UI.ExportToHTMLChartItem();
            this.exportToMHTChartItem1 = new DevExpress.XtraCharts.UI.ExportToMHTChartItem();
            this.exportToXLSChartItem1 = new DevExpress.XtraCharts.UI.ExportToXLSChartItem();
            this.exportToXLSXChartItem1 = new DevExpress.XtraCharts.UI.ExportToXLSXChartItem();
            this.exportToRTFChartItem1 = new DevExpress.XtraCharts.UI.ExportToRTFChartItem();
            this.createExportToImageBaseItem1 = new DevExpress.XtraCharts.UI.CreateExportToImageBaseItem();
            this.exportToBMPChartItem1 = new DevExpress.XtraCharts.UI.ExportToBMPChartItem();
            this.exportToGIFChartItem1 = new DevExpress.XtraCharts.UI.ExportToGIFChartItem();
            this.exportToJPEGChartItem1 = new DevExpress.XtraCharts.UI.ExportToJPEGChartItem();
            this.exportToPNGChartItem1 = new DevExpress.XtraCharts.UI.ExportToPNGChartItem();
            this.exportToTIFFChartItem1 = new DevExpress.XtraCharts.UI.ExportToTIFFChartItem();
            this.chartRibbonPageCategory1 = new DevExpress.XtraCharts.UI.ChartRibbonPageCategory();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.createChartRibbonPage1 = new DevExpress.XtraCharts.UI.CreateChartRibbonPage();
            this.chartTypeRibbonPageGroup1 = new DevExpress.XtraCharts.UI.ChartTypeRibbonPageGroup();
            this.chartAppearanceRibbonPageGroup1 = new DevExpress.XtraCharts.UI.ChartAppearanceRibbonPageGroup();
            this.createChartOtherRibbonPage1 = new DevExpress.XtraCharts.UI.CreateChartOtherRibbonPage();
            this.chartWizardRibbonPageGroup1 = new DevExpress.XtraCharts.UI.ChartWizardRibbonPageGroup();
            this.chartTemplatesRibbonPageGroup1 = new DevExpress.XtraCharts.UI.ChartTemplatesRibbonPageGroup();
            this.chartPrintExportRibbonPageGroup1 = new DevExpress.XtraCharts.UI.ChartPrintExportRibbonPageGroup();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gagTanker = new DevExpress.XtraGauges.Win.GaugeControl();
            this.pcbDriver = new System.Windows.Forms.PictureBox();
            this.pcbVehicle = new System.Windows.Forms.PictureBox();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gcVehicleList = new DevExpress.XtraGrid.GridControl();
            this.gvVehicleList = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.PlateNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Make = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Model = new DevExpress.XtraGrid.Columns.GridColumn();
            this.VehicleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtcVehicleDetails = new DevExpress.XtraTab.XtraTabControl();
            this.xtpLubricant = new DevExpress.XtraTab.XtraTabPage();
            this.icbLubricant = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.gcVehicleLubricant = new DevExpress.XtraGrid.GridControl();
            this.gvVehicleLubricant = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.sbnAddLubricant = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.xtpTire = new DevExpress.XtraTab.XtraTabPage();
            this.icbTire = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.gcVehicleTire = new DevExpress.XtraGrid.GridControl();
            this.gvVehicleTire = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.sbnAddTire = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.chartBarController1 = new DevExpress.XtraCharts.UI.ChartBarController();
            this.gaugeControl1 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.circularGauge1 = new DevExpress.XtraGauges.Win.Gauges.Circular.CircularGauge();
            this.arcScaleComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent();
            this.arcScaleBackgroundLayerComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent();
            this.arcScaleBackgroundLayerComponent2 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent();
            this.arcScaleNeedleComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent();
            this.lscTanker = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleEffectLayerComponent();
            this.digitalGauge1 = new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge();
            this.digitalBackgroundLayerComponent1 = new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandBarGalleryDropDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandBarGalleryDropDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandBarGalleryDropDown3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandBarGalleryDropDown4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandBarGalleryDropDown5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandBarGalleryDropDown6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandBarGalleryDropDown7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbDriver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbVehicle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcVehicleList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVehicleList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtcVehicleDetails)).BeginInit();
            this.xtcVehicleDetails.SuspendLayout();
            this.xtpLubricant.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icbLubricant.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcVehicleLubricant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVehicleLubricant)).BeginInit();
            this.xtpTire.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icbTire.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcVehicleTire)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVehicleTire)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartBarController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.circularGauge1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleBackgroundLayerComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleBackgroundLayerComponent2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleNeedleComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lscTanker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalGauge1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalBackgroundLayerComponent1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.createBarBaseItem1,
            this.createLineBaseItem1,
            this.createPieBaseItem1,
            this.createRotatedBarBaseItem1,
            this.createAreaBaseItem1,
            this.createOtherSeriesTypesBaseItem1,
            this.changePaletteGalleryBaseItem1,
            this.changeAppearanceGalleryBaseItem1,
            this.runDesignerChartItem1,
            this.saveAsTemplateChartItem1,
            this.loadTemplateChartItem1,
            this.printPreviewChartItem1,
            this.printChartItem1,
            this.createExportBaseItem1,
            this.exportToPDFChartItem1,
            this.exportToHTMLChartItem1,
            this.exportToMHTChartItem1,
            this.exportToXLSChartItem1,
            this.exportToXLSXChartItem1,
            this.exportToRTFChartItem1,
            this.exportToBMPChartItem1,
            this.exportToGIFChartItem1,
            this.exportToJPEGChartItem1,
            this.exportToPNGChartItem1,
            this.exportToTIFFChartItem1,
            this.createExportToImageBaseItem1});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 27;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.PageCategories.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageCategory[] {
            this.chartRibbonPageCategory1});
            this.ribbonControl1.Size = new System.Drawing.Size(1304, 27);
            // 
            // createBarBaseItem1
            // 
            this.createBarBaseItem1.DropDownControl = this.commandBarGalleryDropDown1;
            this.createBarBaseItem1.Id = 1;
            this.createBarBaseItem1.Name = "createBarBaseItem1";
            // 
            // commandBarGalleryDropDown1
            // 
            // 
            // 
            // 
            this.commandBarGalleryDropDown1.Gallery.AllowFilter = false;
            this.commandBarGalleryDropDown1.Gallery.ColumnCount = 4;
            chartControlCommandGalleryItemGroup2DColumn2.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            createBarChartItem2,
            createFullStackedBarChartItem2,
            createSideBySideFullStackedBarChartItem2,
            createSideBySideStackedBarChartItem2,
            createStackedBarChartItem2});
            chartControlCommandGalleryItemGroup3DColumn2.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            createBar3DChartItem2,
            createFullStackedBar3DChartItem2,
            createManhattanBarChartItem2,
            createSideBySideFullStackedBar3DChartItem2,
            createSideBySideStackedBar3DChartItem2,
            createStackedBar3DChartItem2});
            chartControlCommandGalleryItemGroupCylinderColumn2.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            createCylinderBar3DChartItem2,
            createCylinderFullStackedBar3DChartItem2,
            createCylinderManhattanBarChartItem2,
            createCylinderSideBySideFullStackedBar3DChartItem2,
            createCylinderSideBySideStackedBar3DChartItem2,
            createCylinderStackedBar3DChartItem2});
            chartControlCommandGalleryItemGroupConeColumn2.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            createConeBar3DChartItem2,
            createConeFullStackedBar3DChartItem2,
            createConeManhattanBarChartItem2,
            createConeSideBySideFullStackedBar3DChartItem2,
            createConeSideBySideStackedBar3DChartItem2,
            createConeStackedBar3DChartItem2});
            chartControlCommandGalleryItemGroupPyramidColumn2.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            createPyramidBar3DChartItem2,
            createPyramidFullStackedBar3DChartItem2,
            createPyramidManhattanBarChartItem2,
            createPyramidSideBySideFullStackedBar3DChartItem2,
            createPyramidSideBySideStackedBar3DChartItem2,
            createPyramidStackedBar3DChartItem2});
            this.commandBarGalleryDropDown1.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            chartControlCommandGalleryItemGroup2DColumn2,
            chartControlCommandGalleryItemGroup3DColumn2,
            chartControlCommandGalleryItemGroupCylinderColumn2,
            chartControlCommandGalleryItemGroupConeColumn2,
            chartControlCommandGalleryItemGroupPyramidColumn2});
            this.commandBarGalleryDropDown1.Gallery.ImageSize = new System.Drawing.Size(32, 32);
            this.commandBarGalleryDropDown1.Gallery.RowCount = 10;
            this.commandBarGalleryDropDown1.Gallery.ShowScrollBar = DevExpress.XtraBars.Ribbon.Gallery.ShowScrollBar.Auto;
            this.commandBarGalleryDropDown1.Name = "commandBarGalleryDropDown1";
            this.commandBarGalleryDropDown1.Ribbon = this.ribbonControl1;
            // 
            // createLineBaseItem1
            // 
            this.createLineBaseItem1.DropDownControl = this.commandBarGalleryDropDown2;
            this.createLineBaseItem1.Id = 2;
            this.createLineBaseItem1.Name = "createLineBaseItem1";
            // 
            // commandBarGalleryDropDown2
            // 
            // 
            // 
            // 
            this.commandBarGalleryDropDown2.Gallery.AllowFilter = false;
            this.commandBarGalleryDropDown2.Gallery.ColumnCount = 3;
            chartControlCommandGalleryItemGroup2DLine2.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            createLineChartItem2,
            createFullStackedLineChartItem2,
            createScatterLineChartItem2,
            createSplineChartItem2,
            createStackedLineChartItem2,
            createStepLineChartItem2});
            chartControlCommandGalleryItemGroup3DLine2.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            createLine3DChartItem2,
            createFullStackedLine3DChartItem2,
            createSpline3DChartItem2,
            createStackedLine3DChartItem2,
            createStepLine3DChartItem2});
            this.commandBarGalleryDropDown2.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            chartControlCommandGalleryItemGroup2DLine2,
            chartControlCommandGalleryItemGroup3DLine2});
            this.commandBarGalleryDropDown2.Gallery.ImageSize = new System.Drawing.Size(32, 32);
            this.commandBarGalleryDropDown2.Gallery.RowCount = 4;
            this.commandBarGalleryDropDown2.Gallery.ShowScrollBar = DevExpress.XtraBars.Ribbon.Gallery.ShowScrollBar.Auto;
            this.commandBarGalleryDropDown2.Name = "commandBarGalleryDropDown2";
            this.commandBarGalleryDropDown2.Ribbon = this.ribbonControl1;
            // 
            // createPieBaseItem1
            // 
            this.createPieBaseItem1.DropDownControl = this.commandBarGalleryDropDown3;
            this.createPieBaseItem1.Id = 3;
            this.createPieBaseItem1.Name = "createPieBaseItem1";
            // 
            // commandBarGalleryDropDown3
            // 
            // 
            // 
            // 
            this.commandBarGalleryDropDown3.Gallery.AllowFilter = false;
            this.commandBarGalleryDropDown3.Gallery.ColumnCount = 3;
            chartControlCommandGalleryItemGroup2DPie2.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            createPieChartItem2,
            createDoughnutChartItem2,
            createNestedDoughnutChartItem2});
            chartControlCommandGalleryItemGroup3DPie2.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            createPie3DChartItem2,
            createDoughnut3DChartItem2});
            this.commandBarGalleryDropDown3.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            chartControlCommandGalleryItemGroup2DPie2,
            chartControlCommandGalleryItemGroup3DPie2});
            this.commandBarGalleryDropDown3.Gallery.ImageSize = new System.Drawing.Size(32, 32);
            this.commandBarGalleryDropDown3.Gallery.RowCount = 2;
            this.commandBarGalleryDropDown3.Gallery.ShowScrollBar = DevExpress.XtraBars.Ribbon.Gallery.ShowScrollBar.Auto;
            this.commandBarGalleryDropDown3.Name = "commandBarGalleryDropDown3";
            this.commandBarGalleryDropDown3.Ribbon = this.ribbonControl1;
            // 
            // createRotatedBarBaseItem1
            // 
            this.createRotatedBarBaseItem1.DropDownControl = this.commandBarGalleryDropDown4;
            this.createRotatedBarBaseItem1.Id = 4;
            this.createRotatedBarBaseItem1.Name = "createRotatedBarBaseItem1";
            // 
            // commandBarGalleryDropDown4
            // 
            // 
            // 
            // 
            this.commandBarGalleryDropDown4.Gallery.AllowFilter = false;
            this.commandBarGalleryDropDown4.Gallery.ColumnCount = 3;
            chartControlCommandGalleryItemGroup2DBar2.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            createRotatedBarChartItem2,
            createRotatedFullStackedBarChartItem2,
            createRotatedSideBySideFullStackedBarChartItem2,
            createRotatedSideBySideStackedBarChartItem2,
            createRotatedStackedBarChartItem2});
            this.commandBarGalleryDropDown4.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            chartControlCommandGalleryItemGroup2DBar2});
            this.commandBarGalleryDropDown4.Gallery.ImageSize = new System.Drawing.Size(32, 32);
            this.commandBarGalleryDropDown4.Gallery.RowCount = 2;
            this.commandBarGalleryDropDown4.Gallery.ShowScrollBar = DevExpress.XtraBars.Ribbon.Gallery.ShowScrollBar.Auto;
            this.commandBarGalleryDropDown4.Name = "commandBarGalleryDropDown4";
            this.commandBarGalleryDropDown4.Ribbon = this.ribbonControl1;
            // 
            // createAreaBaseItem1
            // 
            this.createAreaBaseItem1.DropDownControl = this.commandBarGalleryDropDown5;
            this.createAreaBaseItem1.Id = 5;
            this.createAreaBaseItem1.Name = "createAreaBaseItem1";
            // 
            // commandBarGalleryDropDown5
            // 
            // 
            // 
            // 
            this.commandBarGalleryDropDown5.Gallery.AllowFilter = false;
            this.commandBarGalleryDropDown5.Gallery.ColumnCount = 4;
            chartControlCommandGalleryItemGroup2DArea2.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            createAreaChartItem2,
            createFullStackedAreaChartItem2,
            createFullStackedSplineAreaChartItem2,
            createSplineAreaChartItem2,
            createStackedAreaChartItem2,
            createStackedSplineAreaChartItem2,
            createStepAreaChartItem2});
            chartControlCommandGalleryItemGroup3DArea2.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            createArea3DChartItem2,
            createFullStackedArea3DChartItem2,
            createFullStackedSplineArea3DChartItem2,
            createSplineArea3DChartItem2,
            createStackedArea3DChartItem2,
            createStackedSplineArea3DChartItem2,
            createStepArea3DChartItem2});
            this.commandBarGalleryDropDown5.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            chartControlCommandGalleryItemGroup2DArea2,
            chartControlCommandGalleryItemGroup3DArea2});
            this.commandBarGalleryDropDown5.Gallery.ImageSize = new System.Drawing.Size(32, 32);
            this.commandBarGalleryDropDown5.Gallery.RowCount = 4;
            this.commandBarGalleryDropDown5.Gallery.ShowScrollBar = DevExpress.XtraBars.Ribbon.Gallery.ShowScrollBar.Auto;
            this.commandBarGalleryDropDown5.Name = "commandBarGalleryDropDown5";
            this.commandBarGalleryDropDown5.Ribbon = this.ribbonControl1;
            // 
            // createOtherSeriesTypesBaseItem1
            // 
            this.createOtherSeriesTypesBaseItem1.DropDownControl = this.commandBarGalleryDropDown6;
            this.createOtherSeriesTypesBaseItem1.Id = 6;
            this.createOtherSeriesTypesBaseItem1.Name = "createOtherSeriesTypesBaseItem1";
            // 
            // commandBarGalleryDropDown6
            // 
            // 
            // 
            // 
            this.commandBarGalleryDropDown6.Gallery.AllowFilter = false;
            this.commandBarGalleryDropDown6.Gallery.ColumnCount = 4;
            chartControlCommandGalleryItemGroupPoint2.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            createPointChartItem2,
            createBubbleChartItem2});
            chartControlCommandGalleryItemGroupFunnel2.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            createFunnelChartItem2,
            createFunnel3DChartItem2});
            chartControlCommandGalleryItemGroupFinancial2.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            createStockChartItem2,
            createCandleStickChartItem2});
            chartControlCommandGalleryItemGroupRadar2.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            createRadarPointChartItem2,
            createRadarLineChartItem2,
            createRadarAreaChartItem2,
            createScatterRadarLineChartItem2});
            chartControlCommandGalleryItemGroupPolar2.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            createPolarPointChartItem2,
            createPolarLineChartItem2,
            createPolarAreaChartItem2,
            createScatterPolarLineChartItem2});
            chartControlCommandGalleryItemGroupRange2.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            createRangeBarChartItem2,
            createSideBySideRangeBarChartItem2,
            createRangeAreaChartItem2,
            createRangeArea3DChartItem2});
            chartControlCommandGalleryItemGroupGantt2.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            createGanttChartItem2,
            createSideBySideGanttChartItem2});
            this.commandBarGalleryDropDown6.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            chartControlCommandGalleryItemGroupPoint2,
            chartControlCommandGalleryItemGroupFunnel2,
            chartControlCommandGalleryItemGroupFinancial2,
            chartControlCommandGalleryItemGroupRadar2,
            chartControlCommandGalleryItemGroupPolar2,
            chartControlCommandGalleryItemGroupRange2,
            chartControlCommandGalleryItemGroupGantt2});
            this.commandBarGalleryDropDown6.Gallery.ImageSize = new System.Drawing.Size(32, 32);
            this.commandBarGalleryDropDown6.Gallery.RowCount = 7;
            this.commandBarGalleryDropDown6.Gallery.ShowScrollBar = DevExpress.XtraBars.Ribbon.Gallery.ShowScrollBar.Auto;
            this.commandBarGalleryDropDown6.Name = "commandBarGalleryDropDown6";
            this.commandBarGalleryDropDown6.Ribbon = this.ribbonControl1;
            // 
            // changePaletteGalleryBaseItem1
            // 
            this.changePaletteGalleryBaseItem1.DropDownControl = this.commandBarGalleryDropDown7;
            this.changePaletteGalleryBaseItem1.Id = 7;
            this.changePaletteGalleryBaseItem1.Name = "changePaletteGalleryBaseItem1";
            // 
            // commandBarGalleryDropDown7
            // 
            // 
            // 
            // 
            this.commandBarGalleryDropDown7.Gallery.AllowFilter = false;
            this.commandBarGalleryDropDown7.Gallery.Appearance.ItemCaptionAppearance.Hovered.Options.UseFont = true;
            this.commandBarGalleryDropDown7.Gallery.Appearance.ItemCaptionAppearance.Hovered.Options.UseTextOptions = true;
            this.commandBarGalleryDropDown7.Gallery.Appearance.ItemCaptionAppearance.Hovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.commandBarGalleryDropDown7.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseFont = true;
            this.commandBarGalleryDropDown7.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseTextOptions = true;
            this.commandBarGalleryDropDown7.Gallery.Appearance.ItemCaptionAppearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.commandBarGalleryDropDown7.Gallery.Appearance.ItemCaptionAppearance.Pressed.Options.UseFont = true;
            this.commandBarGalleryDropDown7.Gallery.Appearance.ItemCaptionAppearance.Pressed.Options.UseTextOptions = true;
            this.commandBarGalleryDropDown7.Gallery.Appearance.ItemCaptionAppearance.Pressed.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.commandBarGalleryDropDown7.Gallery.Appearance.ItemDescriptionAppearance.Hovered.Options.UseFont = true;
            this.commandBarGalleryDropDown7.Gallery.Appearance.ItemDescriptionAppearance.Hovered.Options.UseTextOptions = true;
            this.commandBarGalleryDropDown7.Gallery.Appearance.ItemDescriptionAppearance.Hovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.commandBarGalleryDropDown7.Gallery.Appearance.ItemDescriptionAppearance.Normal.Options.UseFont = true;
            this.commandBarGalleryDropDown7.Gallery.Appearance.ItemDescriptionAppearance.Normal.Options.UseTextOptions = true;
            this.commandBarGalleryDropDown7.Gallery.Appearance.ItemDescriptionAppearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.commandBarGalleryDropDown7.Gallery.Appearance.ItemDescriptionAppearance.Pressed.Options.UseFont = true;
            this.commandBarGalleryDropDown7.Gallery.Appearance.ItemDescriptionAppearance.Pressed.Options.UseTextOptions = true;
            this.commandBarGalleryDropDown7.Gallery.Appearance.ItemDescriptionAppearance.Pressed.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.commandBarGalleryDropDown7.Gallery.ColumnCount = 1;
            this.commandBarGalleryDropDown7.Gallery.ImageSize = new System.Drawing.Size(160, 10);
            this.commandBarGalleryDropDown7.Gallery.ItemImageLayout = DevExpress.Utils.Drawing.ImageLayoutMode.MiddleLeft;
            this.commandBarGalleryDropDown7.Gallery.ItemImageLocation = DevExpress.Utils.Locations.Right;
            skinPaddingEdges3.Bottom = -3;
            skinPaddingEdges3.Top = -3;
            this.commandBarGalleryDropDown7.Gallery.ItemImagePadding = skinPaddingEdges3;
            skinPaddingEdges4.Bottom = -3;
            skinPaddingEdges4.Top = -3;
            this.commandBarGalleryDropDown7.Gallery.ItemTextPadding = skinPaddingEdges4;
            this.commandBarGalleryDropDown7.Gallery.RowCount = 10;
            this.commandBarGalleryDropDown7.Gallery.ShowGroupCaption = false;
            this.commandBarGalleryDropDown7.Gallery.ShowItemText = true;
            this.commandBarGalleryDropDown7.Gallery.ShowScrollBar = DevExpress.XtraBars.Ribbon.Gallery.ShowScrollBar.Auto;
            this.commandBarGalleryDropDown7.Name = "commandBarGalleryDropDown7";
            this.commandBarGalleryDropDown7.Ribbon = this.ribbonControl1;
            // 
            // changeAppearanceGalleryBaseItem1
            // 
            // 
            // 
            // 
            this.changeAppearanceGalleryBaseItem1.Gallery.ColumnCount = 7;
            this.changeAppearanceGalleryBaseItem1.Gallery.ImageSize = new System.Drawing.Size(80, 50);
            this.changeAppearanceGalleryBaseItem1.Gallery.RowCount = 4;
            this.changeAppearanceGalleryBaseItem1.Id = 8;
            this.changeAppearanceGalleryBaseItem1.Name = "changeAppearanceGalleryBaseItem1";
            // 
            // runDesignerChartItem1
            // 
            this.runDesignerChartItem1.Id = 9;
            this.runDesignerChartItem1.Name = "runDesignerChartItem1";
            // 
            // saveAsTemplateChartItem1
            // 
            this.saveAsTemplateChartItem1.Id = 10;
            this.saveAsTemplateChartItem1.Name = "saveAsTemplateChartItem1";
            // 
            // loadTemplateChartItem1
            // 
            this.loadTemplateChartItem1.Id = 11;
            this.loadTemplateChartItem1.Name = "loadTemplateChartItem1";
            // 
            // printPreviewChartItem1
            // 
            this.printPreviewChartItem1.Id = 12;
            this.printPreviewChartItem1.Name = "printPreviewChartItem1";
            // 
            // printChartItem1
            // 
            this.printChartItem1.Id = 13;
            this.printChartItem1.Name = "printChartItem1";
            // 
            // createExportBaseItem1
            // 
            this.createExportBaseItem1.Id = 14;
            this.createExportBaseItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.exportToPDFChartItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.exportToHTMLChartItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.exportToMHTChartItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.exportToXLSChartItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.exportToXLSXChartItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.exportToRTFChartItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.createExportToImageBaseItem1)});
            this.createExportBaseItem1.MenuDrawMode = DevExpress.XtraBars.MenuDrawMode.SmallImagesText;
            this.createExportBaseItem1.Name = "createExportBaseItem1";
            // 
            // exportToPDFChartItem1
            // 
            this.exportToPDFChartItem1.Id = 15;
            this.exportToPDFChartItem1.Name = "exportToPDFChartItem1";
            // 
            // exportToHTMLChartItem1
            // 
            this.exportToHTMLChartItem1.Id = 16;
            this.exportToHTMLChartItem1.Name = "exportToHTMLChartItem1";
            // 
            // exportToMHTChartItem1
            // 
            this.exportToMHTChartItem1.Id = 17;
            this.exportToMHTChartItem1.Name = "exportToMHTChartItem1";
            // 
            // exportToXLSChartItem1
            // 
            this.exportToXLSChartItem1.Id = 18;
            this.exportToXLSChartItem1.Name = "exportToXLSChartItem1";
            // 
            // exportToXLSXChartItem1
            // 
            this.exportToXLSXChartItem1.Id = 19;
            this.exportToXLSXChartItem1.Name = "exportToXLSXChartItem1";
            // 
            // exportToRTFChartItem1
            // 
            this.exportToRTFChartItem1.Id = 20;
            this.exportToRTFChartItem1.Name = "exportToRTFChartItem1";
            // 
            // createExportToImageBaseItem1
            // 
            this.createExportToImageBaseItem1.Id = 26;
            this.createExportToImageBaseItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.exportToBMPChartItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.exportToGIFChartItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.exportToJPEGChartItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.exportToPNGChartItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.exportToTIFFChartItem1)});
            this.createExportToImageBaseItem1.MenuDrawMode = DevExpress.XtraBars.MenuDrawMode.SmallImagesText;
            this.createExportToImageBaseItem1.Name = "createExportToImageBaseItem1";
            // 
            // exportToBMPChartItem1
            // 
            this.exportToBMPChartItem1.Id = 21;
            this.exportToBMPChartItem1.Name = "exportToBMPChartItem1";
            // 
            // exportToGIFChartItem1
            // 
            this.exportToGIFChartItem1.Id = 22;
            this.exportToGIFChartItem1.Name = "exportToGIFChartItem1";
            // 
            // exportToJPEGChartItem1
            // 
            this.exportToJPEGChartItem1.Id = 23;
            this.exportToJPEGChartItem1.Name = "exportToJPEGChartItem1";
            // 
            // exportToPNGChartItem1
            // 
            this.exportToPNGChartItem1.Id = 24;
            this.exportToPNGChartItem1.Name = "exportToPNGChartItem1";
            // 
            // exportToTIFFChartItem1
            // 
            this.exportToTIFFChartItem1.Id = 25;
            this.exportToTIFFChartItem1.Name = "exportToTIFFChartItem1";
            // 
            // chartRibbonPageCategory1
            // 
            this.chartRibbonPageCategory1.Control = this.chartControl1;
            this.chartRibbonPageCategory1.Name = "chartRibbonPageCategory1";
            this.chartRibbonPageCategory1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.createChartRibbonPage1,
            this.createChartOtherRibbonPage1});
            // 
            // chartControl1
            // 
            xyDiagram2.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram2.AxisY.VisibleInPanesSerializable = "-1";
            this.chartControl1.Diagram = xyDiagram2;
            this.chartControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl1.Location = new System.Drawing.Point(2, 20);
            this.chartControl1.Name = "chartControl1";
            this.chartControl1.PaletteName = "Equity";
            series2.LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
            series2.Name = "Series 1";
            series2.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint6,
            seriesPoint7,
            seriesPoint8,
            seriesPoint9,
            seriesPoint10});
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series2};
            this.chartControl1.SeriesTemplate.ArgumentDataMember = "TableName";
            this.chartControl1.SeriesTemplate.ColorDataMember = "Prefix";
            this.chartControl1.SeriesTemplate.ValueDataMembersSerializable = "LargeCode";
            this.chartControl1.Size = new System.Drawing.Size(455, 298);
            this.chartControl1.TabIndex = 0;
            // 
            // createChartRibbonPage1
            // 
            this.createChartRibbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.chartTypeRibbonPageGroup1,
            this.chartAppearanceRibbonPageGroup1});
            this.createChartRibbonPage1.Name = "createChartRibbonPage1";
            // 
            // chartTypeRibbonPageGroup1
            // 
            this.chartTypeRibbonPageGroup1.ItemLinks.Add(this.createBarBaseItem1);
            this.chartTypeRibbonPageGroup1.ItemLinks.Add(this.createLineBaseItem1);
            this.chartTypeRibbonPageGroup1.ItemLinks.Add(this.createPieBaseItem1);
            this.chartTypeRibbonPageGroup1.ItemLinks.Add(this.createRotatedBarBaseItem1);
            this.chartTypeRibbonPageGroup1.ItemLinks.Add(this.createAreaBaseItem1);
            this.chartTypeRibbonPageGroup1.ItemLinks.Add(this.createOtherSeriesTypesBaseItem1);
            this.chartTypeRibbonPageGroup1.Name = "chartTypeRibbonPageGroup1";
            // 
            // chartAppearanceRibbonPageGroup1
            // 
            this.chartAppearanceRibbonPageGroup1.ItemLinks.Add(this.changePaletteGalleryBaseItem1);
            this.chartAppearanceRibbonPageGroup1.ItemLinks.Add(this.changeAppearanceGalleryBaseItem1);
            this.chartAppearanceRibbonPageGroup1.Name = "chartAppearanceRibbonPageGroup1";
            // 
            // createChartOtherRibbonPage1
            // 
            this.createChartOtherRibbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.chartWizardRibbonPageGroup1,
            this.chartTemplatesRibbonPageGroup1,
            this.chartPrintExportRibbonPageGroup1});
            this.createChartOtherRibbonPage1.Name = "createChartOtherRibbonPage1";
            // 
            // chartWizardRibbonPageGroup1
            // 
            this.chartWizardRibbonPageGroup1.ItemLinks.Add(this.runDesignerChartItem1);
            this.chartWizardRibbonPageGroup1.Name = "chartWizardRibbonPageGroup1";
            // 
            // chartTemplatesRibbonPageGroup1
            // 
            this.chartTemplatesRibbonPageGroup1.ItemLinks.Add(this.saveAsTemplateChartItem1);
            this.chartTemplatesRibbonPageGroup1.ItemLinks.Add(this.loadTemplateChartItem1);
            this.chartTemplatesRibbonPageGroup1.Name = "chartTemplatesRibbonPageGroup1";
            // 
            // chartPrintExportRibbonPageGroup1
            // 
            this.chartPrintExportRibbonPageGroup1.ItemLinks.Add(this.printPreviewChartItem1);
            this.chartPrintExportRibbonPageGroup1.ItemLinks.Add(this.printChartItem1);
            this.chartPrintExportRibbonPageGroup1.ItemLinks.Add(this.createExportBaseItem1);
            this.chartPrintExportRibbonPageGroup1.Name = "chartPrintExportRibbonPageGroup1";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 27);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gaugeControl1);
            this.splitContainerControl1.Panel1.Controls.Add(this.gagTanker);
            this.splitContainerControl1.Panel1.Controls.Add(this.pcbDriver);
            this.splitContainerControl1.Panel1.Controls.Add(this.pcbVehicle);
            this.splitContainerControl1.Panel1.Controls.Add(this.groupControl2);
            this.splitContainerControl1.Panel1.Controls.Add(this.groupControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.xtcVehicleDetails);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1304, 614);
            this.splitContainerControl1.SplitterPosition = 327;
            this.splitContainerControl1.TabIndex = 9;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gagTanker
            // 
            this.gagTanker.AutoLayout = false;
            this.gagTanker.BackColor = System.Drawing.Color.Transparent;
            this.gagTanker.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.digitalGauge1});
            this.gagTanker.Location = new System.Drawing.Point(579, 6);
            this.gagTanker.Name = "gagTanker";
            this.gagTanker.Size = new System.Drawing.Size(250, 106);
            this.gagTanker.TabIndex = 131;
            // 
            // pcbDriver
            // 
            this.pcbDriver.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pcbDriver.Location = new System.Drawing.Point(632, 118);
            this.pcbDriver.Name = "pcbDriver";
            this.pcbDriver.Size = new System.Drawing.Size(197, 208);
            this.pcbDriver.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbDriver.TabIndex = 130;
            this.pcbDriver.TabStop = false;
            // 
            // pcbVehicle
            // 
            this.pcbVehicle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pcbVehicle.Location = new System.Drawing.Point(318, 118);
            this.pcbVehicle.Name = "pcbVehicle";
            this.pcbVehicle.Size = new System.Drawing.Size(308, 208);
            this.pcbVehicle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbVehicle.TabIndex = 129;
            this.pcbVehicle.TabStop = false;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.chartControl1);
            this.groupControl2.Location = new System.Drawing.Point(833, 6);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(459, 320);
            this.groupControl2.TabIndex = 128;
            this.groupControl2.Text = "Vehicle Summerized Chart";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.gcVehicleList);
            this.groupControl1.Location = new System.Drawing.Point(6, 6);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(306, 320);
            this.groupControl1.TabIndex = 127;
            this.groupControl1.Text = "Select Vehicle Here";
            // 
            // gcVehicleList
            // 
            this.gcVehicleList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcVehicleList.Location = new System.Drawing.Point(2, 20);
            this.gcVehicleList.MainView = this.gvVehicleList;
            this.gcVehicleList.MenuManager = this.ribbonControl1;
            this.gcVehicleList.Name = "gcVehicleList";
            this.gcVehicleList.Size = new System.Drawing.Size(302, 298);
            this.gcVehicleList.TabIndex = 127;
            this.gcVehicleList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvVehicleList});
            // 
            // gvVehicleList
            // 
            this.gvVehicleList.AppearancePrint.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.gvVehicleList.AppearancePrint.OddRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.gvVehicleList.AppearancePrint.OddRow.Options.UseBackColor = true;
            this.gvVehicleList.AppearancePrint.Row.Options.UseTextOptions = true;
            this.gvVehicleList.AppearancePrint.Row.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gvVehicleList.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.PlateNo,
            this.Make,
            this.Model,
            this.VehicleID});
            this.gvVehicleList.CustomizationFormBounds = new System.Drawing.Rectangle(147, 353, 200, 164);
            this.gvVehicleList.GridControl = this.gcVehicleList;
            this.gvVehicleList.GroupCount = 1;
            this.gvVehicleList.Name = "gvVehicleList";
            this.gvVehicleList.OptionsSelection.MultiSelect = true;
            this.gvVehicleList.OptionsView.ColumnAutoWidth = false;
            this.gvVehicleList.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.Make, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gvVehicleList.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gvVehicleList_SelectionChanged);
            // 
            // PlateNo
            // 
            this.PlateNo.Caption = "Plate No";
            this.PlateNo.FieldName = "PlateNo";
            this.PlateNo.Name = "PlateNo";
            this.PlateNo.Visible = true;
            this.PlateNo.VisibleIndex = 0;
            this.PlateNo.Width = 94;
            // 
            // Make
            // 
            this.Make.Caption = "Make";
            this.Make.FieldName = "Make";
            this.Make.Name = "Make";
            this.Make.Visible = true;
            this.Make.VisibleIndex = 1;
            this.Make.Width = 109;
            // 
            // Model
            // 
            this.Model.Caption = "Model";
            this.Model.FieldName = "Model";
            this.Model.Name = "Model";
            this.Model.Visible = true;
            this.Model.VisibleIndex = 1;
            this.Model.Width = 135;
            // 
            // VehicleID
            // 
            this.VehicleID.Caption = "VehicleID";
            this.VehicleID.FieldName = "VehicleID";
            this.VehicleID.Name = "VehicleID";
            // 
            // xtcVehicleDetails
            // 
            this.xtcVehicleDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtcVehicleDetails.Location = new System.Drawing.Point(0, 0);
            this.xtcVehicleDetails.Name = "xtcVehicleDetails";
            this.xtcVehicleDetails.SelectedTabPage = this.xtpLubricant;
            this.xtcVehicleDetails.Size = new System.Drawing.Size(1304, 282);
            this.xtcVehicleDetails.TabIndex = 0;
            this.xtcVehicleDetails.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtpLubricant,
            this.xtpTire});
            // 
            // xtpLubricant
            // 
            this.xtpLubricant.Controls.Add(this.icbLubricant);
            this.xtpLubricant.Controls.Add(this.gcVehicleLubricant);
            this.xtpLubricant.Controls.Add(this.labelControl1);
            this.xtpLubricant.Controls.Add(this.sbnAddLubricant);
            this.xtpLubricant.Controls.Add(this.labelControl4);
            this.xtpLubricant.Image = ((System.Drawing.Image)(resources.GetObject("xtpLubricant.Image")));
            this.xtpLubricant.Name = "xtpLubricant";
            this.xtpLubricant.Size = new System.Drawing.Size(1298, 235);
            this.xtpLubricant.Text = "Lubricant";
            // 
            // icbLubricant
            // 
            this.icbLubricant.Location = new System.Drawing.Point(66, 37);
            this.icbLubricant.MenuManager = this.ribbonControl1;
            this.icbLubricant.Name = "icbLubricant";
            this.icbLubricant.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbLubricant.Size = new System.Drawing.Size(171, 20);
            this.icbLubricant.TabIndex = 152;
            // 
            // gcVehicleLubricant
            // 
            this.gcVehicleLubricant.Location = new System.Drawing.Point(66, 64);
            this.gcVehicleLubricant.MainView = this.gvVehicleLubricant;
            this.gcVehicleLubricant.MenuManager = this.ribbonControl1;
            this.gcVehicleLubricant.Name = "gcVehicleLubricant";
            this.gcVehicleLubricant.Size = new System.Drawing.Size(1225, 285);
            this.gcVehicleLubricant.TabIndex = 125;
            this.gcVehicleLubricant.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvVehicleLubricant});
            // 
            // gvVehicleLubricant
            // 
            this.gvVehicleLubricant.GridControl = this.gcVehicleLubricant;
            this.gvVehicleLubricant.Name = "gvVehicleLubricant";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl1.Appearance.Image")));
            this.labelControl1.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl1.Location = new System.Drawing.Point(5, 10);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(479, 20);
            this.labelControl1.TabIndex = 124;
            this.labelControl1.Text = "Under this category you keep track of various type of vehicle lubricants for the " +
    "selected vehicle";
            // 
            // sbnAddLubricant
            // 
            this.sbnAddLubricant.Location = new System.Drawing.Point(253, 37);
            this.sbnAddLubricant.Name = "sbnAddLubricant";
            this.sbnAddLubricant.Size = new System.Drawing.Size(78, 21);
            this.sbnAddLubricant.TabIndex = 123;
            this.sbnAddLubricant.Text = "Add Lubricant";
            this.sbnAddLubricant.Click += new System.EventHandler(this.sbnAddLubricant_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(7, 40);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(44, 13);
            this.labelControl4.TabIndex = 117;
            this.labelControl4.Text = "Lubricant";
            // 
            // xtpTire
            // 
            this.xtpTire.Controls.Add(this.icbTire);
            this.xtpTire.Controls.Add(this.gcVehicleTire);
            this.xtpTire.Controls.Add(this.labelControl7);
            this.xtpTire.Controls.Add(this.sbnAddTire);
            this.xtpTire.Controls.Add(this.labelControl8);
            this.xtpTire.Image = ((System.Drawing.Image)(resources.GetObject("xtpTire.Image")));
            this.xtpTire.Name = "xtpTire";
            this.xtpTire.Size = new System.Drawing.Size(1298, 235);
            this.xtpTire.Text = "Tire       ";
            // 
            // icbTire
            // 
            this.icbTire.Location = new System.Drawing.Point(66, 37);
            this.icbTire.MenuManager = this.ribbonControl1;
            this.icbTire.Name = "icbTire";
            this.icbTire.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbTire.Size = new System.Drawing.Size(171, 20);
            this.icbTire.TabIndex = 157;
            // 
            // gcVehicleTire
            // 
            this.gcVehicleTire.Location = new System.Drawing.Point(66, 64);
            this.gcVehicleTire.MainView = this.gvVehicleTire;
            this.gcVehicleTire.MenuManager = this.ribbonControl1;
            this.gcVehicleTire.Name = "gcVehicleTire";
            this.gcVehicleTire.Size = new System.Drawing.Size(1225, 285);
            this.gcVehicleTire.TabIndex = 156;
            this.gcVehicleTire.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvVehicleTire});
            // 
            // gvVehicleTire
            // 
            this.gvVehicleTire.GridControl = this.gcVehicleTire;
            this.gvVehicleTire.Name = "gvVehicleTire";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl7.Appearance.Image")));
            this.labelControl7.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl7.Location = new System.Drawing.Point(5, 10);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(454, 20);
            this.labelControl7.TabIndex = 155;
            this.labelControl7.Text = "Under this category you keep track of various type of vehicle tires for the selec" +
    "ted vehicle";
            // 
            // sbnAddTire
            // 
            this.sbnAddTire.Location = new System.Drawing.Point(253, 37);
            this.sbnAddTire.Name = "sbnAddTire";
            this.sbnAddTire.Size = new System.Drawing.Size(78, 21);
            this.sbnAddTire.TabIndex = 154;
            this.sbnAddTire.Text = "Add Tire";
            this.sbnAddTire.Click += new System.EventHandler(this.sbnAddTire_Click);
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(7, 40);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(18, 13);
            this.labelControl8.TabIndex = 153;
            this.labelControl8.Text = "Tire";
            // 
            // chartBarController1
            // 
            this.chartBarController1.BarItems.Add(this.createBarBaseItem1);
            this.chartBarController1.BarItems.Add(this.createLineBaseItem1);
            this.chartBarController1.BarItems.Add(this.createPieBaseItem1);
            this.chartBarController1.BarItems.Add(this.createRotatedBarBaseItem1);
            this.chartBarController1.BarItems.Add(this.createAreaBaseItem1);
            this.chartBarController1.BarItems.Add(this.createOtherSeriesTypesBaseItem1);
            this.chartBarController1.BarItems.Add(this.changePaletteGalleryBaseItem1);
            this.chartBarController1.BarItems.Add(this.changeAppearanceGalleryBaseItem1);
            this.chartBarController1.BarItems.Add(this.runDesignerChartItem1);
            this.chartBarController1.BarItems.Add(this.saveAsTemplateChartItem1);
            this.chartBarController1.BarItems.Add(this.loadTemplateChartItem1);
            this.chartBarController1.BarItems.Add(this.printPreviewChartItem1);
            this.chartBarController1.BarItems.Add(this.printChartItem1);
            this.chartBarController1.BarItems.Add(this.createExportBaseItem1);
            this.chartBarController1.BarItems.Add(this.exportToPDFChartItem1);
            this.chartBarController1.BarItems.Add(this.exportToHTMLChartItem1);
            this.chartBarController1.BarItems.Add(this.exportToMHTChartItem1);
            this.chartBarController1.BarItems.Add(this.exportToXLSChartItem1);
            this.chartBarController1.BarItems.Add(this.exportToXLSXChartItem1);
            this.chartBarController1.BarItems.Add(this.exportToRTFChartItem1);
            this.chartBarController1.BarItems.Add(this.createExportToImageBaseItem1);
            this.chartBarController1.BarItems.Add(this.exportToBMPChartItem1);
            this.chartBarController1.BarItems.Add(this.exportToGIFChartItem1);
            this.chartBarController1.BarItems.Add(this.exportToJPEGChartItem1);
            this.chartBarController1.BarItems.Add(this.exportToPNGChartItem1);
            this.chartBarController1.BarItems.Add(this.exportToTIFFChartItem1);
            this.chartBarController1.Control = this.chartControl1;
            // 
            // gaugeControl1
            // 
            this.gaugeControl1.AutoLayout = false;
            this.gaugeControl1.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl1.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.circularGauge1});
            this.gaugeControl1.Location = new System.Drawing.Point(316, 6);
            this.gaugeControl1.Name = "gaugeControl1";
            this.gaugeControl1.Size = new System.Drawing.Size(260, 106);
            this.gaugeControl1.TabIndex = 132;
            // 
            // circularGauge1
            // 
            this.circularGauge1.BackgroundLayers.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent[] {
            this.arcScaleBackgroundLayerComponent1,
            this.arcScaleBackgroundLayerComponent2});
            this.circularGauge1.Bounds = new System.Drawing.Rectangle(6, 6, 248, 99);
            this.circularGauge1.EffectLayers.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleEffectLayerComponent[] {
            this.lscTanker});
            this.circularGauge1.Name = "circularGauge1";
            this.circularGauge1.Needles.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent[] {
            this.arcScaleNeedleComponent1});
            this.circularGauge1.Scales.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent[] {
            this.arcScaleComponent1});
            // 
            // arcScaleComponent1
            // 
            this.arcScaleComponent1.AppearanceTickmarkText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Silver");
            this.arcScaleComponent1.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(125F, 520F);
            this.arcScaleComponent1.EndAngle = -76.7F;
            this.arcScaleComponent1.MajorTickCount = 7;
            this.arcScaleComponent1.MajorTickmark.FormatString = "{0:F0}";
            this.arcScaleComponent1.MajorTickmark.ShapeOffset = -10F;
            this.arcScaleComponent1.MajorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style8_5;
            this.arcScaleComponent1.MajorTickmark.TextOffset = -20F;
            this.arcScaleComponent1.MajorTickmark.TextOrientation = DevExpress.XtraGauges.Core.Model.LabelOrientation.LeftToRight;
            this.arcScaleComponent1.MaxValue = 80F;
            this.arcScaleComponent1.MinorTickCount = 4;
            this.arcScaleComponent1.MinorTickmark.ShapeOffset = -6F;
            this.arcScaleComponent1.MinorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style8_4;
            this.arcScaleComponent1.MinValue = 20F;
            this.arcScaleComponent1.Name = "scale1";
            this.arcScaleComponent1.RadiusX = 420F;
            this.arcScaleComponent1.RadiusY = 420F;
            this.arcScaleComponent1.StartAngle = -103.3F;
            this.arcScaleComponent1.Value = 50F;
            // 
            // arcScaleBackgroundLayerComponent1
            // 
            this.arcScaleBackgroundLayerComponent1.ArcScale = this.arcScaleComponent1;
            this.arcScaleBackgroundLayerComponent1.Name = "bg1";
            this.arcScaleBackgroundLayerComponent1.ScaleCenterPos = new DevExpress.XtraGauges.Core.Base.PointF2D(0.5F, 6.95F);
            this.arcScaleBackgroundLayerComponent1.ShapeType = DevExpress.XtraGauges.Core.Model.BackgroundLayerShapeType.CircularWide_Style8;
            this.arcScaleBackgroundLayerComponent1.Size = new System.Drawing.SizeF(224F, 62F);
            this.arcScaleBackgroundLayerComponent1.ZOrder = 1000;
            // 
            // arcScaleBackgroundLayerComponent2
            // 
            this.arcScaleBackgroundLayerComponent2.ArcScale = this.arcScaleComponent1;
            this.arcScaleBackgroundLayerComponent2.Name = "bg2";
            this.arcScaleBackgroundLayerComponent2.ScaleCenterPos = new DevExpress.XtraGauges.Core.Base.PointF2D(0.5F, 5.05F);
            this.arcScaleBackgroundLayerComponent2.ShapeType = DevExpress.XtraGauges.Core.Model.BackgroundLayerShapeType.CircularWide_Style8_1;
            this.arcScaleBackgroundLayerComponent2.Size = new System.Drawing.SizeF(250F, 88F);
            this.arcScaleBackgroundLayerComponent2.ZOrder = -500;
            // 
            // arcScaleNeedleComponent1
            // 
            this.arcScaleNeedleComponent1.ArcScale = this.arcScaleComponent1;
            this.arcScaleNeedleComponent1.Name = "needle1";
            this.arcScaleNeedleComponent1.ShapeType = DevExpress.XtraGauges.Core.Model.NeedleShapeType.CircularWide_Style8;
            this.arcScaleNeedleComponent1.StartOffset = 368F;
            this.arcScaleNeedleComponent1.ZOrder = -50;
            // 
            // lscTanker
            // 
            this.lscTanker.ArcScale = this.arcScaleComponent1;
            this.lscTanker.Name = "effect1";
            this.lscTanker.ScaleCenterPos = new DevExpress.XtraGauges.Core.Base.PointF2D(0.5F, 8.3F);
            this.lscTanker.Shader = new DevExpress.XtraGauges.Core.Drawing.OpacityShader("Opacity[0.5]");
            this.lscTanker.ShapeType = DevExpress.XtraGauges.Core.Model.EffectLayerShapeType.CircularWide_Style8;
            this.lscTanker.Size = new System.Drawing.SizeF(222F, 52F);
            this.lscTanker.ZOrder = -1000;
            // 
            // digitalGauge1
            // 
            this.digitalGauge1.AppearanceOff.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#80FFC0C0");
            this.digitalGauge1.AppearanceOn.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:WhiteSmoke");
            this.digitalGauge1.AppearanceOn.BorderWidth = 2F;
            this.digitalGauge1.AppearanceOn.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#C80000");
            this.digitalGauge1.BackgroundLayers.AddRange(new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent[] {
            this.digitalBackgroundLayerComponent1});
            this.digitalGauge1.Bounds = new System.Drawing.Rectangle(5, 4, 238, 98);
            this.digitalGauge1.DigitCount = 5;
            this.digitalGauge1.Name = "digitalGauge1";
            this.digitalGauge1.Text = "00,000";
            // 
            // digitalBackgroundLayerComponent1
            // 
            this.digitalBackgroundLayerComponent1.BottomRight = new DevExpress.XtraGauges.Core.Base.PointF2D(259.8125F, 99.9625F);
            this.digitalBackgroundLayerComponent1.Name = "digitalBackgroundLayerComponent13";
            this.digitalBackgroundLayerComponent1.ShapeType = DevExpress.XtraGauges.Core.Model.DigitalBackgroundShapeSetType.Style14;
            this.digitalBackgroundLayerComponent1.TopLeft = new DevExpress.XtraGauges.Core.Base.PointF2D(20F, 0F);
            this.digitalBackgroundLayerComponent1.ZOrder = 1000;
            // 
            // VehicleDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1304, 641);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.ribbonControl1);
            this.Name = "VehicleDetail";
            this.Ribbon = this.ribbonControl1;
            this.RibbonVisibility = DevExpress.XtraBars.Ribbon.RibbonVisibility.Hidden;
            this.Text = "Vehicle Details";
            this.Load += new System.EventHandler(this.VehicleItems_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandBarGalleryDropDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandBarGalleryDropDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandBarGalleryDropDown3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandBarGalleryDropDown4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandBarGalleryDropDown5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandBarGalleryDropDown6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandBarGalleryDropDown7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pcbDriver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbVehicle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcVehicleList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVehicleList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtcVehicleDetails)).EndInit();
            this.xtcVehicleDetails.ResumeLayout(false);
            this.xtpLubricant.ResumeLayout(false);
            this.xtpLubricant.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icbLubricant.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcVehicleLubricant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVehicleLubricant)).EndInit();
            this.xtpTire.ResumeLayout(false);
            this.xtpTire.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icbTire.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcVehicleTire)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVehicleTire)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartBarController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.circularGauge1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleBackgroundLayerComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleBackgroundLayerComponent2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleNeedleComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lscTanker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalGauge1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalBackgroundLayerComponent1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTab.XtraTabControl xtcVehicleDetails;
        private DevExpress.XtraTab.XtraTabPage xtpLubricant;
        private DevExpress.XtraTab.XtraTabPage xtpTire;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton sbnAddLubricant;
        private DevExpress.XtraGrid.GridControl gcVehicleLubricant;
        private DevExpress.XtraGrid.Views.Grid.GridView gvVehicleLubricant;
        private DevExpress.XtraEditors.ImageComboBoxEdit icbLubricant;
        private DevExpress.XtraEditors.ImageComboBoxEdit icbTire;
        private DevExpress.XtraGrid.GridControl gcVehicleTire;
        private DevExpress.XtraGrid.Views.Grid.GridView gvVehicleTire;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.SimpleButton sbnAddTire;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl gcVehicleList;
        private DevExpress.XtraCharts.UI.CreateBarBaseItem createBarBaseItem1;
        private DevExpress.XtraBars.Commands.CommandBarGalleryDropDown commandBarGalleryDropDown1;
        private DevExpress.XtraCharts.UI.CreateLineBaseItem createLineBaseItem1;
        private DevExpress.XtraBars.Commands.CommandBarGalleryDropDown commandBarGalleryDropDown2;
        private DevExpress.XtraCharts.UI.CreatePieBaseItem createPieBaseItem1;
        private DevExpress.XtraBars.Commands.CommandBarGalleryDropDown commandBarGalleryDropDown3;
        private DevExpress.XtraCharts.UI.CreateRotatedBarBaseItem createRotatedBarBaseItem1;
        private DevExpress.XtraBars.Commands.CommandBarGalleryDropDown commandBarGalleryDropDown4;
        private DevExpress.XtraCharts.UI.CreateAreaBaseItem createAreaBaseItem1;
        private DevExpress.XtraBars.Commands.CommandBarGalleryDropDown commandBarGalleryDropDown5;
        private DevExpress.XtraCharts.UI.CreateOtherSeriesTypesBaseItem createOtherSeriesTypesBaseItem1;
        private DevExpress.XtraBars.Commands.CommandBarGalleryDropDown commandBarGalleryDropDown6;
        private DevExpress.XtraCharts.UI.ChangePaletteGalleryBaseItem changePaletteGalleryBaseItem1;
        private DevExpress.XtraBars.Commands.CommandBarGalleryDropDown commandBarGalleryDropDown7;
        private DevExpress.XtraCharts.UI.ChangeAppearanceGalleryBaseItem changeAppearanceGalleryBaseItem1;
        private DevExpress.XtraCharts.UI.RunDesignerChartItem runDesignerChartItem1;
        private DevExpress.XtraCharts.UI.SaveAsTemplateChartItem saveAsTemplateChartItem1;
        private DevExpress.XtraCharts.UI.LoadTemplateChartItem loadTemplateChartItem1;
        private DevExpress.XtraCharts.UI.PrintPreviewChartItem printPreviewChartItem1;
        private DevExpress.XtraCharts.UI.PrintChartItem printChartItem1;
        private DevExpress.XtraCharts.UI.CreateExportBaseItem createExportBaseItem1;
        private DevExpress.XtraCharts.UI.ExportToPDFChartItem exportToPDFChartItem1;
        private DevExpress.XtraCharts.UI.ExportToHTMLChartItem exportToHTMLChartItem1;
        private DevExpress.XtraCharts.UI.ExportToMHTChartItem exportToMHTChartItem1;
        private DevExpress.XtraCharts.UI.ExportToXLSChartItem exportToXLSChartItem1;
        private DevExpress.XtraCharts.UI.ExportToXLSXChartItem exportToXLSXChartItem1;
        private DevExpress.XtraCharts.UI.ExportToRTFChartItem exportToRTFChartItem1;
        private DevExpress.XtraCharts.UI.CreateExportToImageBaseItem createExportToImageBaseItem1;
        private DevExpress.XtraCharts.UI.ExportToBMPChartItem exportToBMPChartItem1;
        private DevExpress.XtraCharts.UI.ExportToGIFChartItem exportToGIFChartItem1;
        private DevExpress.XtraCharts.UI.ExportToJPEGChartItem exportToJPEGChartItem1;
        private DevExpress.XtraCharts.UI.ExportToPNGChartItem exportToPNGChartItem1;
        private DevExpress.XtraCharts.UI.ExportToTIFFChartItem exportToTIFFChartItem1;
        private DevExpress.XtraCharts.UI.ChartRibbonPageCategory chartRibbonPageCategory1;
        private DevExpress.XtraCharts.ChartControl chartControl1;
        private DevExpress.XtraCharts.UI.CreateChartRibbonPage createChartRibbonPage1;
        private DevExpress.XtraCharts.UI.ChartTypeRibbonPageGroup chartTypeRibbonPageGroup1;
        private DevExpress.XtraCharts.UI.ChartAppearanceRibbonPageGroup chartAppearanceRibbonPageGroup1;
        private DevExpress.XtraCharts.UI.CreateChartOtherRibbonPage createChartOtherRibbonPage1;
        private DevExpress.XtraCharts.UI.ChartWizardRibbonPageGroup chartWizardRibbonPageGroup1;
        private DevExpress.XtraCharts.UI.ChartTemplatesRibbonPageGroup chartTemplatesRibbonPageGroup1;
        private DevExpress.XtraCharts.UI.ChartPrintExportRibbonPageGroup chartPrintExportRibbonPageGroup1;
        private DevExpress.XtraCharts.UI.ChartBarController chartBarController1;
        private System.Windows.Forms.PictureBox pcbVehicle;
        private System.Windows.Forms.PictureBox pcbDriver;
        private DevExpress.XtraGauges.Win.GaugeControl gagTanker;
        private DevExpress.XtraGrid.Views.Grid.GridView gvVehicleList;
        private DevExpress.XtraGrid.Columns.GridColumn PlateNo;
        private DevExpress.XtraGrid.Columns.GridColumn Make;
        private DevExpress.XtraGrid.Columns.GridColumn Model;
        private DevExpress.XtraGrid.Columns.GridColumn VehicleID;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.CircularGauge circularGauge1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent arcScaleBackgroundLayerComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent arcScaleComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent arcScaleBackgroundLayerComponent2;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleEffectLayerComponent lscTanker;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent arcScaleNeedleComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge digitalGauge1;
        private DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent digitalBackgroundLayerComponent1;
    }
}