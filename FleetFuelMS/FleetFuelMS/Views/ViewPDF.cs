﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FleetFuelMS
{
    public partial class ViewPDF : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public static string PDFPath = "";
        public ViewPDF()
        {
            InitializeComponent();
        }

        private void ViewPDF_Load(object sender, EventArgs e)
        {
            if ( PDFPath != "" )
                pdfViewer.LoadDocument(PDFPath);
        }

        private void pdfViewer_Load(object sender, EventArgs e)
        {

        }
    }
}
