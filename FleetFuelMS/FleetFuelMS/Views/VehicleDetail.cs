﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using System.Drawing.Drawing2D;

namespace FleetFuelMS
{
    public partial class VehicleDetail : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        static string vehicleID = "";
        public VehicleDetail()
        {
            InitializeComponent();
        }

        private void VehicleItems_Load(object sender, EventArgs e)
        {
            loadControls();
        }

        private void loadControls()
        {
            //pcbVehicle.Image = Image.FromFile(@"D:\_PROJECTS\FFMS Development Center\icons\Car-icon.jpg");
            //GraphicsPath gp = new GraphicsPath();
            //gp.AddEllipse(pcbVehicle.DisplayRectangle);
            //pcbVehicle.Region = new Region(gp);

            loadVehicles();
            loadLubricant();
            loadTire();
            
            icbTire.SelectedIndex = 0;
            icbLubricant.SelectedIndex = 0;
        }

        private void loadLubricant()
        {
            SqlCommand cmdLoadLubricant = new SqlCommand()
            {
                Connection = GloVars.Conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "getLubricantInfo"
            };

            SqlDataReader rdrLubricant = null;
            try
            {
                cmdLoadLubricant.Parameters.Add(new SqlParameter("@LID", "%"));
                rdrLubricant = cmdLoadLubricant.ExecuteReader();

                icbLubricant.Properties.Items.Add(new ImageComboBoxItem("-- SELECT LUBRICANT --"));

                while (rdrLubricant.Read())
                {
                    ImageComboBoxItem newImageComboBoxItem =
                    new ImageComboBoxItem(string.Format("{0} | {1} | {2}", rdrLubricant[3].ToString(), rdrLubricant[2].ToString(), rdrLubricant[1].ToString()))
                    {
                        Value = rdrLubricant[0].ToString()
                    };

                    icbLubricant.Properties.Items.Add(newImageComboBoxItem);
                }
            }
            catch (SqlException Ex) { MessageBox.Show(Ex.Message); }
            finally
            {
                if (rdrLubricant != null)
                    rdrLubricant.Close();
                cmdLoadLubricant.Dispose();
            }
        }

        private void loadTire()
        {
            SqlCommand cmdLoadTire = new SqlCommand()
            {
                Connection = GloVars.Conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "getTireInfo"
            };

            SqlDataReader rdrTire = null;
            try
            {
                cmdLoadTire.Parameters.Add(new SqlParameter("@TID", "%"));
                rdrTire = cmdLoadTire.ExecuteReader();

                icbTire.Properties.Items.Add(new ImageComboBoxItem("-- SELECT TIRE --"));

                while (rdrTire.Read())
                {
                    ImageComboBoxItem newImageComboBoxItem = 
                    new ImageComboBoxItem(string.Format("{0} | {1}", rdrTire[1].ToString(), rdrTire[2].ToString()))
                    {
                        Value = rdrTire[0].ToString()
                    };

                    icbTire.Properties.Items.Add(newImageComboBoxItem);
                }
            }
            catch (SqlException Ex) { MessageBox.Show(Ex.Message); }
            finally
            {
                if( rdrTire != null )
                    rdrTire.Close();
                cmdLoadTire.Dispose();
            }
        }

        private void loadVehicles()
        {
            SqlCommand cmdloadV = new SqlCommand()
            {
                Connection = GloVars.Conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "getVehicleInfo"
            };

            SqlDataReader rdrLoadVehicle = null;
            try
            {
                cmdloadV.Parameters.Add(new SqlParameter("@VID", "%"));
                rdrLoadVehicle = cmdloadV.ExecuteReader();

                DataTable dtVehicle = new DataTable();
                dtVehicle.Columns.Add("VehicleID");
                dtVehicle.Columns.Add("PlateNo");
                dtVehicle.Columns.Add("Make");
                dtVehicle.Columns.Add("Model");

                while (rdrLoadVehicle.Read())
                {
                    DataRow drVehicle = dtVehicle.NewRow();

                    drVehicle["VehicleID"] = rdrLoadVehicle["VehicleID"];
                    drVehicle["PlateNo"] = rdrLoadVehicle["PlateNo"];
                    drVehicle["Make"] = rdrLoadVehicle["Make"];
                    drVehicle["Model"] = rdrLoadVehicle["Model"];

                    dtVehicle.Rows.Add(drVehicle);
                }

                gcVehicleList.DataSource = dtVehicle;
                gvVehicleList.ColumnPanelRowHeight = 25;
                gvVehicleList.OptionsFilter.AllowMRUFilterList = true;
                gvVehicleList.OptionsView.ShowAutoFilterRow = true;
                gvVehicleList.Appearance.HeaderPanel.Options.UseBackColor = true;
                gvVehicleList.Appearance.HeaderPanel.BackColor = Color.Black;

                gvVehicleList.BestFitColumns(true);
                gvVehicleList.Columns[0].BestFit();
            }
            catch (SqlException Ex) { MessageBox.Show(Ex.Message);}
            finally
            {
                rdrLoadVehicle.Close();
                cmdloadV.Dispose();
            }


        }

        private void sbnAddLubricant_Click(object sender, EventArgs e)
        {
            //TO DO: 

            SqlCommand cmdAddLub = new SqlCommand() {
                Connection = GloVars.Conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "registerVehicleLubricant"
            };

            try
            {
                string strVLCode = Tasks.getNewCode("VEHICLELUBRICANT", 4);
                cmdAddLub.Parameters.Add(new SqlParameter("@VLID", strVLCode));
                cmdAddLub.Parameters.Add(new SqlParameter("@Vehicle", vehicleID));
                cmdAddLub.Parameters.Add(new SqlParameter("@Lubricant", icbLubricant.Properties.Items[icbLubricant.SelectedIndex].Value.ToString()));
                cmdAddLub.Parameters.Add(new SqlParameter("@Ruser", GloVars.loggedUser.UserID));

                cmdAddLub.ExecuteNonQuery();

                Tasks.updateCodeLog("VEHICLELUBRICANT", strVLCode);
                loadVehicleLubricant(vehicleID);
                MessageBox.Show(GloVars.MSG_VEHICLE_LUBRICANT_REGISTERED);
            }
            catch(Exception Ex) { MessageBox.Show(Ex.Message); }
            finally { cmdAddLub.Dispose(); }
        }

        private void sbnAddTire_Click(object sender, EventArgs e)
        {
            //TO DO: 

            SqlCommand cmdAddTire = new SqlCommand()
            {
                Connection = GloVars.Conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "registerVehicleTire"
            };

            try
            {
                string strVLCode = Tasks.getNewCode("VEHICLETIRE", 4);
                cmdAddTire.Parameters.Add(new SqlParameter("@VTID", strVLCode));
                cmdAddTire.Parameters.Add(new SqlParameter("@Vehicle", vehicleID));
                cmdAddTire.Parameters.Add(new SqlParameter("@Tire", icbTire.Properties.Items[icbTire.SelectedIndex].Value.ToString()));
                cmdAddTire.Parameters.Add(new SqlParameter("@RUser", GloVars.loggedUser.UserID));

                cmdAddTire.ExecuteNonQuery();

                Tasks.updateCodeLog("VEHICLETIRE", strVLCode);
                loadVehicleTire(vehicleID);
                MessageBox.Show(GloVars.MSG_VEHICLE_TIRE_REGISTERED);
            }
            catch (Exception Ex) { MessageBox.Show(Ex.Message); }
            finally { cmdAddTire.Dispose(); }
        }

        private void loadVehicleLubricant(string vehicleID)
        {
            SqlCommand cmdloadL = new SqlCommand()
            {
                Connection = Tasks.getNewConnection(GloVars.Conn.ConnectionString),
                CommandType = CommandType.StoredProcedure,
                CommandText = "getVehicleLubricant"
            };

            SqlDataReader rdrLoadLubricant = null;
            try
            {
                cmdloadL.Parameters.Add(new SqlParameter("@VID", vehicleID));
                rdrLoadLubricant = cmdloadL.ExecuteReader();

                DataTable dtLubricant = new DataTable();
                dtLubricant.Columns.Add("Lubricant ID");
                dtLubricant.Columns.Add("Lub. Name");
                dtLubricant.Columns.Add("Type");
                dtLubricant.Columns.Add("Current Price");
                dtLubricant.Columns.Add("Current Price Start Date");
                dtLubricant.Columns.Add("Description");

                while (rdrLoadLubricant.Read())
                {
                    DataRow drLubricant = dtLubricant.NewRow();

                    drLubricant["Lubricant ID"] = rdrLoadLubricant["LubricantID"];
                    drLubricant["Lub. Name"] = rdrLoadLubricant["LName"];
                    drLubricant["Type"] = rdrLoadLubricant["LType"];
                    drLubricant["Current Price"] = rdrLoadLubricant["CurrentPrice"]; 
                    drLubricant["Current Price Start Date"] = rdrLoadLubricant["CPriceStartDate"];
                    drLubricant["Description"] = rdrLoadLubricant["LDescription"];

                    dtLubricant.Rows.Add(drLubricant);
                }

                gcVehicleLubricant.DataSource = dtLubricant;
                gvVehicleLubricant.ColumnPanelRowHeight = 25;
                gvVehicleLubricant.OptionsFilter.AllowMRUFilterList = true;
                gvVehicleLubricant.OptionsView.ShowAutoFilterRow = true;
                gvVehicleLubricant.Appearance.HeaderPanel.Options.UseBackColor = true;
                gvVehicleLubricant.Appearance.HeaderPanel.BackColor = Color.Black;

                gvVehicleLubricant.BestFitColumns(true);
                gvVehicleLubricant.Columns[0].BestFit();
            }
            catch (SqlException Ex) { MessageBox.Show(Ex.Message); }
            finally
            {
                if(rdrLoadLubricant != null ) rdrLoadLubricant.Close();
                cmdloadL.Dispose();
            }

        }

        private void loadVehicleTire(string vehicleID)
        {
            SqlCommand cmdloadT = new SqlCommand()
            {
                Connection = Tasks.getNewConnection(GloVars.Conn.ConnectionString),
                CommandType = CommandType.StoredProcedure,
                CommandText = "getVehicleTire"
            };

            SqlDataReader rdrLoadTire = null;
            try
            {
                cmdloadT.Parameters.Add(new SqlParameter("@VID", vehicleID));
                rdrLoadTire = cmdloadT.ExecuteReader();

                DataTable dtTire = new DataTable();
                dtTire.Columns.Add("Tire ID");
                dtTire.Columns.Add("Serial - DOT");
                dtTire.Columns.Add("Tire Name");
                dtTire.Columns.Add("Brand");
                dtTire.Columns.Add("MaxLoad");
                dtTire.Columns.Add("Width");
                dtTire.Columns.Add("Current Price");
                dtTire.Columns.Add("Current Price Start Date");
                dtTire.Columns.Add("Description");

                while (rdrLoadTire.Read())
                {
                    DataRow drTire = dtTire.NewRow();

                    drTire["Tire ID"] = rdrLoadTire["TireID"];
                    drTire["Serial - DOT"] = rdrLoadTire["DOTidentificationNo"];
                    drTire["Tire Name"] = rdrLoadTire["TName"];
                    drTire["Brand"] = rdrLoadTire["Brand"];
                    drTire["MaxLoad"] = rdrLoadTire["MaxLoad"];
                    drTire["Width"] = rdrLoadTire["Width"];
                    drTire["Current Price"] = rdrLoadTire["CurrentPrice"];
                    drTire["Current Price Start Date"] = rdrLoadTire["CPriceStartDate"];
                    drTire["Description"] = rdrLoadTire["TDescription"];

                    dtTire.Rows.Add(drTire);
                }

                gcVehicleTire.DataSource = dtTire;
                gvVehicleTire.ColumnPanelRowHeight = 25;
                gvVehicleTire.OptionsFilter.AllowMRUFilterList = true;
                gvVehicleTire.OptionsView.ShowAutoFilterRow = true;
                gvVehicleTire.Appearance.HeaderPanel.Options.UseBackColor = true;
                gvVehicleTire.Appearance.HeaderPanel.BackColor = Color.Black;

                gvVehicleLubricant.BestFitColumns(true);
                gvVehicleLubricant.Columns[0].BestFit();
            }
            catch (SqlException Ex) { MessageBox.Show(Ex.Message); }
            finally
            {
                if (rdrLoadTire != null) rdrLoadTire.Close();
                cmdloadT.Dispose();
            }

        }

        private void gvVehicleList_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            SqlCommand cmdloadVehicle = new SqlCommand()
            {
                Connection = Tasks.getNewConnection(GloVars.Conn.ConnectionString),
                CommandType = CommandType.StoredProcedure,
                CommandText = "getVehicleInfo"
            };

            SqlDataReader rdrVehicle = null;

            try
            {
                // The following three codes identify the first
                // selected row from the vehicle grid control 
                // listed
                int[] selRows = gvVehicleList.GetSelectedRows();
                DataRowView selRow = (DataRowView)gvVehicleList.GetRow(selRows[0]);
                vehicleID = selRow["VehicleID"].ToString();

                cmdloadVehicle.Parameters.Add(new SqlParameter("@VID", vehicleID));

                rdrVehicle = cmdloadVehicle.ExecuteReader();

                if( pcbVehicle.Image != null )
                    pcbVehicle.Image.Dispose();
                if (pcbDriver.Image != null)
                    pcbDriver.Image.Dispose();

                if (rdrVehicle.Read())
                {
                    int Tanker = int.Parse(rdrVehicle["TankerCapacity"].ToString());

                    int first = (int)(0.2 * Tanker);
                    int second = (int)(0.4 * Tanker);

                    //lscTanker..MaxValue = Tanker;

                    //lscTanker.Ranges[0].EndValue = Tanker;
                    //lscTanker.Ranges[0].StartValue = second+1;

                    //lscTanker.Ranges[1].StartValue = first+1;
                    //lscTanker.Ranges[1].EndValue = second;

                    //lscTanker.Ranges[2].StartValue = 0;
                    //lscTanker.Ranges[2].EndValue = first;

                    //lscTanker.Value = int.Parse(rdrVehicle["StartGageReading"].ToString());
                    //Load Vehicle Image if this any

                    if (rdrVehicle["VImage"] != DBNull.Value)
                        pcbVehicle.Image = Image.FromFile(Tasks.getByteImage((byte[])rdrVehicle["VImage"], "png"));
                    DataTable drDriver = Tasks.getVehicleDriver(vehicleID);

                    // Check if the vehicle is assigned a driver
                    if (drDriver != null)
                    {
                        // here driver is assigned already but
                        // check wether the driver has photo
                        if (drDriver.Rows.Count > 0)
                        {
                            DataRow drTemp = drDriver.Rows[0];
                            if (drTemp["DImage"] != DBNull.Value)
                                pcbDriver.Image = Image.FromFile(Tasks.getByteImage((byte[])drTemp["DImage"], "jpg"));
                        }
                        else
                            pcbDriver.Image = null;
                    }
                    else
                        pcbDriver.Image = null;


                    loadVehicleLubricant(vehicleID);
                    loadVehicleTire(vehicleID);
                    //TODO:
                    //  Chart and Gauges load goes here

                    //loadVehicleSummerizedChart();
                }
                else
                {
                    pcbVehicle.Image = null;
                    pcbDriver.Image = null;
                }
            }
            catch (Exception Ex) { MessageBox.Show(Ex.Message); }
            finally
            {
                if (rdrVehicle != null) rdrVehicle.Close();
                cmdloadVehicle.Dispose();
            }
        }
    }
}
