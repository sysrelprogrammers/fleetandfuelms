﻿using Microsoft.Win32;
using System;
using System.Threading;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace FleetFuelMS
{
    public partial class ConnectionConfig : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public ConnectionConfig()
        {
            InitializeComponent();
        }

        private void sbnTest_Click(object sender, EventArgs e)
        {
            string ConString = string.Format("Data Source={0}; Initial Catalog = {1}; Persist Security Info = True; User ID = {2}; Password={3}",
                               cbeDatabaseServer.Text, "FFMS", txtUserName.Text,txtPassword.Text);

            SqlConnection conTest = new SqlConnection(ConString);
           
            try
            {
                conTest.Open();
                // Here the connection is successful 
                // proceed with success
                MessageBox.Show(GloVars.MSG_CONNECTION_TEST_SUCCESS);
            }
            catch(Exception Ex)
            {
                MessageBox.Show(GloVars.MSG_CONNECTION_TEST_FAIL);
            }
            finally
            {
                conTest.Close();
            }
        }

        private void sbnConnect_Click(object sender, EventArgs e)
        {
            string ConString = string.Format("Data Source={0}; Initial Catalog = {1}; Persist Security Info = True; User ID = {2}; Password={3}",
                               cbeDatabaseServer.Text, "FFMS", txtUserName.Text, txtPassword.Text);

            SqlConnection conTest = new SqlConnection(ConString);

            try
            {
                conTest.Open();

                // Here the connection is successful 
                // proceed with success

                RegistryKey key = Registry.LocalMachine.CreateSubKey("SOFTWARE\\FFMS\\Connection");
                key.SetValue("Server", cbeDatabaseServer.Text);
                key.SetValue("Database", txtDatabase.Text);
                key.SetValue("user", txtUserName.Text);
                key.SetValue("password", txtPassword.Text);

                MessageBox.Show(GloVars.MSG_CONNECTION_TEST_SUCCESS);
            }
            catch (Exception Ex)
            {
                MessageBox.Show(GloVars.MSG_CONNECTION_TEST_FAIL);
            }
            finally
            {
                conTest.Close();
            }
        }

        private DataTable loadSqlServer()
        {
            return Tasks.getSqlServerInstance();
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {

        }

        private void ConnectionConfig_Load(object sender, EventArgs e)
        {
        }

        private void ConnectionConfig_Shown(object sender, EventArgs e)
        {
            Task<DataTable> task = new Task<DataTable>(obj => {
                return loadSqlServer();

            }, 400);
            task.Start();

            DataTable result = task.Result;

            foreach (DataRow dr in result.Rows)
            {
                cbeDatabaseServer.Properties.Items.Add(string.Format("{0}\\{1}", dr[0], dr[1]));
            }

        }
    }
}
