﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;

namespace FleetFuelMS
{
    public partial class UserLogin : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public UserLogin()
        {
            InitializeComponent();
        }

        private void sbnCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void sbnLogin_Click(object sender, EventArgs e)
        {
            //SqlConnection conUser = new SqlConnection()
            //{
            //   ConnectionString = "Data Source=jeremaion\\sqlexpress;Initial Catalog=FFMS;Integrated Security=True",
            //};

            SqlCommand cmdUser = new SqlCommand() {
                Connection = GloVars.Conn,//conUser,
                CommandType = CommandType.StoredProcedure,
                CommandText = "getUserByUserName",
                CommandTimeout = 30
            };

            SqlDataReader rdrUser = null;

            try {
                //conUser.Open();

                cmdUser.Parameters.Add(new SqlParameter("@UName", txtUserName.Text));

                rdrUser = cmdUser.ExecuteReader();

                if( rdrUser.Read() )
                {
                    if ( Tasks.verifyPassword(txtPassword.Text, rdrUser["Password"].ToString()) )
                    {
                        //Now the password is confirmed so proceed
                        GloVars.loggedUser = new User();

                        GloVars.loggedUser.UserID = rdrUser["UserId"].ToString();
                        GloVars.loggedUser.UserName = rdrUser["UserName"].ToString();
                        rdrUser.Close();
                        GloVars.loggedUser.loginTime = Tasks.getCurrentTime();

                        GloVars.MDIPARENT = new FleetFuelMSForm();
                        GloVars.MDIPARENT.Show();

                        this.Hide();
                        return;
                    }
                }

                // Now the information provided is invalid 
                // so proceed with failer and focus on 
                // the text password.
                MessageBox.Show(GloVars.MSG_INVALID_LOGIN_INFO);
                txtPassword.Focus();
                txtPassword.SelectAll(); 
            }
            catch (SqlException Ex) { MessageBox.Show(Ex.Message);}
            finally
            {
                if ( rdrUser != null)
                    rdrUser.Close();
                cmdUser.Dispose();
                //conUser.Close();
            }
        }

        private void UserLogin_Load(object sender, EventArgs e)
        {
            string[] conInfo = Tasks.getDatabaseLoginInfo();
            string strCon = string.Format("Data Source={0}; Initial Catalog={1}; Persist Security Info=True; User ID={2};Password={3}",
                                         conInfo[0], conInfo[1], conInfo[2], conInfo[3]);

            GloVars.Conn = new SqlConnection();
            GloVars.Conn.ConnectionString = strCon;
            GloVars.Conn.Open();
        }

        private bool checkConnectedPrevious()
        {
            return (false);
        }

        private void connect()
        {
            string[] conInfo = Tasks.getDatabaseLoginInfo();
            string strCon = string.Format("Data Source={0}; Initial Catalog={1}; Persist Security Info=True; User ID={2};Password={3}",
                                         conInfo[0], conInfo[1], conInfo[2], conInfo[3]);

            GloVars.Conn = new SqlConnection();
            GloVars.Conn.ConnectionString = strCon;
            GloVars.Conn.Open();
        }

        private string getLongDateTime()
        {
            DateTime dtNow = DateTime.Now;
            return string.Format("{0} {1}", dtNow.ToLongDateString(), dtNow.ToLongTimeString()); 
        }

        private void tmrLogin_Tick(object sender, EventArgs e)
        {
            labelControl4.Text = getLongDateTime();
        }

        private void btnConfigure_Click(object sender, EventArgs e)
        {
            (new ConnectionConfig()).ShowDialog();
        }
    }
}