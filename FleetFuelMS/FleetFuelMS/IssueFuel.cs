﻿using System;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Drawing;

namespace FleetFuelMS
{
    public partial class IssueFuel : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public IssueFuel()
        {
            InitializeComponent();
        }

        private void IssueFuel_Load(object sender, EventArgs e)
        {
            loadDrivers();
        }

        private void loadDrivers()
        {
            SqlCommand cmdloadD = new SqlCommand()
            {
                Connection = GloVars.Conn,
                CommandType = System.Data.CommandType.StoredProcedure,
                CommandText = "getDriverInfo"
            };

            SqlDataReader rdrLoadDriver = null;

            try
            {
                rdrLoadDriver = cmdloadD.ExecuteReader();

                DataTable dtDriver = new DataTable();
                
                dtDriver.Columns.Add("FullName");


                while (rdrLoadDriver.Read())
                {
                    DataRow drVehicle = dtDriver.NewRow();
                    string strFullName = string.Format("{0} {1} {2}", rdrLoadDriver["First Name"], rdrLoadDriver["Middle Name"], rdrLoadDriver["Last Name"]);
                    drVehicle["FullName"] = strFullName;

                    dtDriver.Rows.Add(drVehicle);
                }

                trlDrivers.DataSource = dtDriver;

            }
            catch (SqlException Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                if (rdrLoadDriver != null) rdrLoadDriver.Close();
                cmdloadD.Dispose();
            }
        }

        private void trlDrivers_CustomDrawNodeCell(object sender, DevExpress.XtraTreeList.CustomDrawNodeCellEventArgs e)
        {
            TreeList tree = sender as TreeList;

            if (e.Node == tree.FocusedNode)
                e.Graphics.FillRectangle(SystemBrushes.Window, e.Bounds);
            Rectangle rect = new Rectangle(e.EditViewInfo.ContentRect,
                            e.EditViewInfo.ContentRect.Top,
                            Convert.ToInt32(e.Graphics.MeasureString(e.Node.GetValue("FullName"),(new Font(),)),
                            Convert.ToInt32(e.Graphics.MeasureString(Height)));
        }
    }
}
