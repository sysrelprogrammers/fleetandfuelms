﻿using System;
using Microsoft.Win32;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

namespace FleetFuelMS
{

    class Tasks
    { 
        //
        //
        public static SqlConnection getNewConnection(string strConnection)
        {
            SqlConnection Con = null;
            try {
                Con = new SqlConnection(strConnection);
                Con.Open();
            }
            catch(Exception Ex) { }
            finally { }
            return Con;
        }
        //This function generates list of available SQL Server instances
        //across the network and return a datatable
        public static DataTable getSqlServerInstance()
        {
            return SqlDataSourceEnumerator.Instance.GetDataSources();
        }

        // This function generated hashed password
        // and return the result
        public static string encryptPassword(string password)
        {
            string strSalt = BCrypt.Net.BCrypt.GenerateSalt(12);

            string hashed = BCrypt.Net.BCrypt.HashPassword(password, strSalt);
            
            return hashed;
        }

        // This function generated hashed password
        // and return the result
        public static bool verifyPassword(string password, string hashed)
        {
            return BCrypt.Net.BCrypt.Verify(password, hashed);
        }

        public static string GetRandomSalt()
        {
            return BCrypt.Net.BCrypt.GenerateSalt(30);
        }

        // This procedure gets new code for a bunch of tables
        // tableName , columnName, fieldSize stands for 
        // ( Table, Column ) names, the size of integral parts
        // in the expected id(code) patters
        public static string getNewCode(String tableName, int fieldSize)
        {
            SqlCommand cmdGetCode = new SqlCommand();

            cmdGetCode.Connection = GloVars.Conn;
            cmdGetCode.CommandType = CommandType.StoredProcedure;
            cmdGetCode.CommandText = "getNewCode";

            cmdGetCode.Parameters.Add(new SqlParameter("@TName", tableName));

            try
            {
                SqlDataReader rdrGetCode = cmdGetCode.ExecuteReader();

                string newCode = ""; // new code to be generated

                if (rdrGetCode.Read())
                {
                    string currentCode = (int.Parse( rdrGetCode["LargeCode"].ToString()) + 1).ToString();

                    for (int i = 1; i <= fieldSize - (currentCode.Length); i++)
                        newCode += "0";
                    newCode = rdrGetCode["Prefix"].ToString() + "-" + newCode + currentCode;
                }

                rdrGetCode.Close();
                cmdGetCode.Dispose();

                return newCode;
            }
            catch (SqlException Ex)
            {
                return "";
            }
            finally {
                
            }
        }

        //This procedure take an image path and convert the located
        //Image into byte array
        public static byte[] getImageByte(String ImagePath)
        {

            if (ImagePath != "")
            {
                FileStream fs;
                fs = new FileStream(ImagePath, FileMode.Open, FileAccess.Read);
                //a byte array to read the image
                byte[] picbyte = new byte[fs.Length];
                fs.Read(picbyte, 0, System.Convert.ToInt32(fs.Length));
                fs.Close();

                return picbyte;
            }
            else
                return null;
        }

        //This procedure take an bytearray and converts it to image
        //of the specified image format as fileFormat
        public static string getByteImage(byte[] imageByte, string fileFormat)
        {
            string filePath = String.Format("{0}FFMS\\", Path.GetTempPath());

            if (!Directory.Exists(filePath))   Directory.CreateDirectory(filePath);

            //filePath = String.Format("{0}{1}.{2}", filePath, Path.GetRandomFileName(),fileFormat);
            filePath = String.Format("{0}temp.{1}", filePath, fileFormat);

            if (imageByte != null)
            {
                FileStream fs;
                fs = new FileStream(filePath, FileMode.Create);

                //a byte array to read the image
                byte[] picbyte = (byte[]) imageByte;
                fs.Write(picbyte, 0, picbyte.Length);
                fs.Close();
            }

            return filePath;
        }

        public static string getFuelID(string FuelName)
        {
            SqlCommand cmdFuelId = new SqlCommand()
            {
                Connection = GloVars.Conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "getFuelIDbyName"
            };

            cmdFuelId.Parameters.Add(new SqlParameter("@FName", FuelName));

            SqlDataReader rdrFuelId = cmdFuelId.ExecuteReader();

            string temp = "";

            if (rdrFuelId.Read())
                temp = rdrFuelId["FuelID"].ToString();

            rdrFuelId.Close();
            cmdFuelId.Dispose();
            return temp;
        }

        public static string getVehicleMakeID(string Make, string Model)
        {
            SqlCommand cmdVehicleMake = new SqlCommand()
            {
                Connection = GloVars.Conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "getVehicleMakeID"
            };

            cmdVehicleMake.Parameters.Add(new SqlParameter("@Make", Make));
            cmdVehicleMake.Parameters.Add(new SqlParameter("@Model", Model));

            SqlDataReader rdrVehicleMake = cmdVehicleMake.ExecuteReader();

            string temp = "";

            if (rdrVehicleMake.Read())
                temp = rdrVehicleMake["MakeID"].ToString();

            rdrVehicleMake.Close();
            cmdVehicleMake.Dispose();
            return temp;
        }

        public static DataTable getVehicleDriver(string vehicleID)
        {
            DataTable dtReturn = new DataTable("Driver");
            dtReturn.Columns.Add("VehicleID");
            dtReturn.Columns.Add("First Name");
            dtReturn.Columns.Add("Middle Name");
            dtReturn.Columns.Add("Last Name");
            dtReturn.Columns.Add("Gender");
            dtReturn.Columns.Add("Birth Date");
            dtReturn.Columns.Add("Phone");
            dtReturn.Columns.Add("License");
            dtReturn.Columns.Add("Vehicle");
            dtReturn.Columns.Add("DImage", typeof(byte[]));
            dtReturn.Columns.Add("RegDate");
            dtReturn.Columns.Add("RUser");

            SqlCommand cmdVehicleDriver = new SqlCommand()
            {
                Connection = Tasks.getNewConnection(GloVars.Conn.ConnectionString ),
                CommandType = CommandType.StoredProcedure,
                CommandText = "getVehicleDriver"
            };
            SqlDataReader rdrVehicleDriver = null;
            try
            {
                cmdVehicleDriver.Parameters.Add(new SqlParameter("@VID", vehicleID));

                rdrVehicleDriver = cmdVehicleDriver.ExecuteReader();

                if (rdrVehicleDriver.Read())
                {
                    DataRow drReturn = dtReturn.Rows.Add();
                    drReturn[0] = rdrVehicleDriver["DriverID"];
                    drReturn[1] = rdrVehicleDriver["FirstName"];
                    drReturn[2] = rdrVehicleDriver["MiddleName"];
                    drReturn[3] = rdrVehicleDriver["LastName"];
                    drReturn[4] = rdrVehicleDriver["Gender"];
                    drReturn[5] = rdrVehicleDriver["BirthDate"];
                    drReturn[6] = rdrVehicleDriver["Phone"];
                    drReturn[7] = rdrVehicleDriver["License"];
                    drReturn[8] = rdrVehicleDriver["Vehicle"];
                    drReturn[9] = (byte []) rdrVehicleDriver["DImage"];
                    drReturn[10] = rdrVehicleDriver["RegDate"];
                    drReturn[11] = rdrVehicleDriver["RUser"];
                }
            }
            catch(Exception Ex) { MessageBox.Show(Ex.Message); }
            finally {
                if (rdrVehicleDriver != null) rdrVehicleDriver.Close();
                cmdVehicleDriver.Dispose();
            }
                
            return dtReturn;
        }

        public static string getVehicleTypeID(string VType)
        {
            SqlCommand cmdVehicleType = new SqlCommand()
            {
                Connection = GloVars.Conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "getVehicleTypeIDbyType"
            };

            cmdVehicleType.Parameters.Add(new SqlParameter("@TName", VType));

            SqlDataReader rdrVehicleType = cmdVehicleType.ExecuteReader();

            string temp = "";

            if (rdrVehicleType.Read())
                temp = rdrVehicleType["TypeID"].ToString();

            rdrVehicleType.Close();
            cmdVehicleType.Dispose();
            return temp;
        }

        public static string getCountryCode(string countryName)
        {
            SqlCommand cmdCountry = new SqlCommand()
            {
                Connection = GloVars.Conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "getCountryNameByID"
            };

            cmdCountry.Parameters.Add(new SqlParameter("@CountryName", countryName));

            SqlDataReader rdrCountry = cmdCountry.ExecuteReader();

            string temp = "";

            if (rdrCountry.Read())
                temp = rdrCountry["CountryCode"].ToString();

            rdrCountry.Close();
            cmdCountry.Dispose();
            return temp;

        }
        
        public static void updateCodeLog(string Table, string LastCode)
        {
            SqlCommand cmdReg = new SqlCommand()
            {
                Connection = GloVars.Conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "updateCodeLog"
            };

            cmdReg.Parameters.Add(new SqlParameter("@TableName", Table));
            cmdReg.Parameters.Add(new SqlParameter("@LargeCode", int.Parse(LastCode.Substring(3))));

            cmdReg.ExecuteNonQuery();
        }

        public static DateTime getCurrentTime()
        {
            DateTime dtValue = new DateTime();

            SqlCommand cmdTime = new SqlCommand()
            {
                Connection = GloVars.Conn,
                CommandType = CommandType.StoredProcedure,
                CommandText = "getCurrentTime"
            };

            SqlDataReader rdrTime = null;

            try
            {
                rdrTime = cmdTime.ExecuteReader();

                if (rdrTime.Read())
                {
                    dtValue = (DateTime)rdrTime["CurrentTime"];
                }

            }
            catch (Exception Ex) {
                MessageBox.Show(Ex.Message);
            }
            finally {
                if( rdrTime != null)
                    rdrTime.Close();
                cmdTime.Dispose();
            }

            return dtValue;
        }


        //This function shall return list of strings
        //to be used for login info frm registry
        public static string[] getDatabaseLoginInfo()
        {
            string[] strInfo = new string[4];

            try
            {
                RegistryKey regKey;
                regKey = (RegistryKey)Registry.LocalMachine.OpenSubKey("SOFTWARE\\FFMS\\Connection");

                strInfo[0] = regKey.GetValue("server").ToString();
                strInfo[1] = regKey.GetValue("Database").ToString();
                strInfo[2] = regKey.GetValue("user").ToString();
                strInfo[3] = regKey.GetValue("password").ToString();
            }
            catch (Exception Ex)
            {

            }
            finally { }

            return strInfo;
        }
    }
}
