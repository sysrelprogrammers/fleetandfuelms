﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace FleetFuelMS
{
    class User
    {
        public string UserID { set; get; }
        public string UserName { set; get; }
        public DateTime loginTime { set; get; }
        public DateTime logoutTime { set; get; }
    }

    class GloVars
    {
        public static string MSG_ = "";

        // Registration Messages
        public static string MSG_DRIVER_REGISTERED = "Driver Registeration Successful!";
        public static string MSG_GUARANTOR_REGISTERED = "Guarantor Registration Successful!";
        public static string MSG_TIRE_REGISTERED = "Tire Registration Successful!";
        public static string MSG_LUBRICANT_REGISTERED = "Lubricant Registeration Successful!";
        public static string MSG_VEHICLE_LUBRICANT_REGISTERED = "Vehicle Lubricant Registeration Successful!";
        public static string MSG_VEHICLE_TIRE_REGISTERED = "Vehicle Tire Registeration Successful!";

        //Invalid Input Messages
        public static string MSG_INVALID_LOGIN_INFO = "Invalid login Info. Please try again!";

        //Connection Messages
        public static string MSG_CONNECTION_TEST_SUCCESS = "Test Successful";
        public static string MSG_CONNECTION_TEST_FAIL = "Invalid Connection Parameters. Or make sure physical connection available";

        public static FleetFuelMSForm MDIPARENT;
        public static SqlConnection Conn;
        public static User loggedUser = null;

        public static string[] listOfTireBrands = { "Dunlop","Addis Tire","Adereneza","Advan","Aeolus","AKA","Alliance","Apollo","Austone","Autogrip","Avon","Barez","Belshina","Bridgestone",
                               "Casumina","CEAT","Chengshan","Cobra","Cooper","Dayton","Dean","Dena Tire","Definity","Dextero","Diamond","Dick Cepek","DMACK","DRC","Eldorado","Fireforce",
                               "Firestone","Fortune","Fullrun","Fullway","Fuzion","Greatwall","Giti Tire","Goldstone","GT Radial","Hercules","Hualin","Ironman","Lassa","Mastercraft",
                               "Mentor","Mickey Thompson","Mohawk","Nature","Primewell","Roadmaster","Roadpro","Runway","Seiberling","Supercat","Starfire","Versatyre","Yokohama" };
    }
}
 