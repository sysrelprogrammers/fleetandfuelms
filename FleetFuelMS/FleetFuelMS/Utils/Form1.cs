﻿using System;
using System.IO;
using Microsoft.VisualBasic.FileIO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FleetFuelMS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }
        private void updatedata(string code, string country, string fileName)
        {
            //use filestream object to read the image
            //read to the full length of image to a byte array.
            //add this byte as an oracle parameter and insert it into database.

            try
            {
                //proceed only when the image has a valid path
                //
                OpenFileDialog ofdImage = new OpenFileDialog();

                String imagename = fileName;

                FileStream fs;
                fs = new FileStream(@imagename, FileMode.Open, FileAccess.Read);
                //a byte array to read the image
                byte[] picbyte = new byte[fs.Length];
                fs.Read(picbyte, 0, System.Convert.ToInt32(fs.Length));
                fs.Close();
                //open the database using odp.net and insert the data
                string connstr = "Data Source=jeremaion\\sqlexpress;Initial Catalog=FFMS;Integrated Security=True";
                SqlConnection conn = new SqlConnection(connstr);
                conn.Open();
                string query;
                query = string.Format("insert into COUNTRY(countrycode, country, cimage) values('{0}','{1}', @pic)", code, country);

                SqlParameter picparameter = new SqlParameter()
                {
                    SqlDbType = SqlDbType.Image,
                    ParameterName = "pic",
                    Value = picbyte
                };

                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.Add(picparameter);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                conn.Close();
                conn.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //const string filePath = "D:\\_PROJECTS\\FFMS Development Center\\Pictures\\flags-mini_2\\";

            //updatedata("cd", "", filePath + "cd.png" );
            //updatedata("ks", "", filePath + "ks.png");
            //updatedata("me", "", filePath + "me.png");
            //updatedata("rs", "", filePath + "rs.png");
            //updatedata("tl", "", filePath + "tl.png");

            TextFieldParser parser = new TextFieldParser(@"C:\Users\jerem\Desktop\country.csv");
            parser.TextFieldType = FieldType.Delimited;
            parser.SetDelimiters(",");
            int i = 0;
            string[] FullText = new string[197];
            while (!parser.EndOfData)
            {
                //Processing row
                string[] fields = parser.ReadFields();
                FullText[i] = "(";
                for (int j = 0; j < fields.Length; j++)
                {
                    if (j != 2)
                        FullText[i] += string.Format("'{0}',", fields[j]);
                    else
                        FullText[i] += string.Format("'{0}'", fields[j]);
                }
                FullText[i] += "),";
                i++;
            }
            saveSqlFile(FullText);
            parser.Close();
        }

        private void saveSqlFile(string[] createText)
        {
            string path = @"C:\Users\jerem\Desktop\MyTest.sql";

            // This text is added only once to the file.
            if (!File.Exists(path))
            {
                // Create a file to write to.
                File.WriteAllLines(path, createText, Encoding.UTF8);
            }

            // This text is always added, making the file longer over time
            // if it is not deleted.
            //File.AppendAllText(path, createText, Encoding.UTF8);
        }
    }
}
