﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace FleetFuelMS
{
    public partial class FleetFuelMSForm : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public FleetFuelMSForm()
        {
            InitializeComponent();
        }
               
        private void FleetFuelMSForm_Load(object sender, EventArgs e)
        {
            Form1 frm = new Form1();
            frm.MdiParent = this;
            frm.Show();
        }

        private void barMdiChildrenListItem1_ListItemClick(object sender, DevExpress.XtraBars.ListItemClickEventArgs e)
        {

        }

        private void bbiRegisterVehicle_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RegisterVehicle frm = new RegisterVehicle()
            {
                MdiParent = this,
                Text = "Register Vehicle",
                WindowState = FormWindowState.Maximized
            };

            frm.Show();
        }

        private void bbiRegisterDriver_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RegisterDriver frm = new RegisterDriver()
            {
                MdiParent = this,
                Text = "Register Driver",
                WindowState = FormWindowState.Maximized
            };

            frm.Show();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RegisterLubricant frm = new RegisterLubricant()
            {
                MdiParent = this,
                Text = "Register Lubricant",
                WindowState = FormWindowState.Maximized
            };

            frm.Show();
        }

        private void bbiRegisterTire_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RegisterTire frm = new RegisterTire()
            {
                MdiParent = this,
                Text = "Register Tire",
                WindowState = FormWindowState.Maximized
            };

            frm.Show();
        }
        private void bbiVehicleItems_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            VehicleDetail frm = new VehicleDetail()
            {
                MdiParent = this,
                Text = "Vehicle Items (Lubricant, Tire)",
                WindowState = FormWindowState.Maximized
            };

            frm.Show();
        }

        private void FleetFuelMSForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void bbiIssueFuel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            IssueFuel frm = new IssueFuel()
            {
                MdiParent = this,
                Text = "Issue Fuel",
                WindowState = FormWindowState.Maximized
            };

            frm.Show();
        }
    }
}
